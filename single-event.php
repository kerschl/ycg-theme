<?php
/* Template for displaying single events */
get_header();
while (have_posts()) : the_post();
	get_template_part('template-parts/events/event-single');
endwhile;
get_footer();
