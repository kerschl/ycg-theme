<?php
/* Template for displaying single galleries */
get_header();
while (have_posts()) : the_post();
	get_template_part('template-parts/gallery/gallery-single');
endwhile;
get_footer();
