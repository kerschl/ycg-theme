<?php
/* Template for displaying the front page */
get_header();
// Slider
$Slider = new WP_Query(array(
	'nopaging' => true,
	'post_type' => 'slide'
));
if ($Slider->have_posts()) {
?>
	<div id="FrontPageCarousel" class="container-md p-0 carousel slide" data-bs-ride="carousel">
		<div class="carousel-indicators mb-0 text-center d-none d-md-block">
			<?php
			// indicators
			$slide_count = 0;
			while ($Slider->have_posts()) {
				$Slider->the_post();
				get_template_part('template-parts/slider/carousel-indicator', "name", array('slide_count' => $slide_count));
				$slide_count++;
			} ?>
		</div>
		<div class="carousel-inner">
			<?php
			// slider items
			$slide_count = 0;
			while ($Slider->have_posts()) {
				$Slider->the_post();
				get_template_part('template-parts/slider/carousel-item', "name", array('slide_count' => $slide_count));
				$slide_count++;
			} ?>
		</div>
		<?php
		get_template_part('template-parts/slider/carousel-control');
		?>
	</div>
<?php } ?>
<article>
	<?php
	// Query Text Content of homepage and display
	the_post();
	the_content();
	?>
</article>
<aside>
	<div class="row">
		<h2 class="col-12 text-center py-3"><a class="link-dark text-decoration-none" href="/aktuelles">Aktuelles</a></h2>
		<?php
		// Get the last three posts
		$homepagePosts = new WP_Query(array(
			'posts_per_page' => 3,
			'post_type' => 'post'
		));
		if ($homepagePosts->have_posts()) {
			while ($homepagePosts->have_posts()) {
				$homepagePosts->the_post();
				get_template_part('template-parts/posts/post-item');
			}
		} else {
			get_template_part('template-parts/posts/post-no-posts');
		}
		?>
	</div>
	<div class="row">
		<div class="col-12 col-lg-6">
			<h2 class="text-center py-3">
				<a class="link-dark text-decoration-none" href="/veranstaltungen">Veranstaltungen</a>
			</h2>
			<?php
			// Get the most recent events
			$today = date('Ymd');
			$homepageEvents = new WP_Query(array(
				'posts_per_page' => 3,
				'post_type' => 'event',
				'meta_key' => 'veranstaltungsdatum',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_query' => array(
					array(
						'key' => 'veranstaltungsdatum',
						'compare' => '>=',
						'value' => $today,
						'type' => 'numeric'
					)
				)
			));
			if ($homepageEvents->have_posts()) {
				while ($homepageEvents->have_posts()) {
					$homepageEvents->the_post();
					get_template_part('template-parts/events/event-item');
				}
			} else {
				get_template_part('template-parts/events/event-no-events');
			}
			?>
		</div>
		<div class="col-12 col-lg-6">
			<h2 class="text-center py-3">
				<a class="link-dark text-decoration-none" href="/galerie">Galerie</a>
			</h2>
			<?php
			// Get the last three galleries
			$homepageGalleries = new WP_Query(array(
				'posts_per_page' => 6,
				'post_type' => 'gallery'
			));
			if ($homepageGalleries->have_posts()) {
				echo '<div class="list-group mb-3">';
				while ($homepageGalleries->have_posts()) {
					$homepageGalleries->the_post();
					get_template_part('template-parts/gallery/gallery-item');
				}
				echo '</div>';
			} else {
				get_template_part('template-parts/gallery/gallery-no-galleries');
			}
			?>
		</div>
	</div>
</aside>
<?php get_footer();
