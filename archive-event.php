<?php
/* Template for displaying events archive */
get_header();
?>
<article>
	<?php
	$events_page = get_page_by_path("veranstaltungen");
	$post = get_post($events_page->ID);
	echo "<h1>" . apply_filters('the_title', $post->post_title) . "</h1>";
	echo apply_filters('the_content', $post->post_content);
	?>
</article>
<div id="archive">
	<?php
	$first_day_of_the_year = date('Y') . '0101';
	$event_query_settings = array(
		// right now shows all since the first day of the year
		'posts_per_page' => -1,
		'post_type' => 'event',
		'meta_key' => 'veranstaltungsdatum',
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key' => 'veranstaltungsdatum',
				'compare' => '>=',
				'value' => $first_day_of_the_year,
				'type' => 'numeric'
			)
		)
	);

	$events = new WP_Query($event_query_settings);
	if ($events->have_posts()) {
		$copy_events = clone $events;
		$events->the_post();

		$event_type_obj = get_field_object('veranstaltungskategorie');
		$event_type_choices = $event_type_obj['choices'];
	?>
		<div class="row">
			<div class="col-lg-4 col-sm-12 mb-3">
				<button id="upcoming_event_btn" class="btn btn-primary w-100">Auch Vergangene anzeigen</button>
			</div>
			<div class="col-lg-4 col-sm-12 mb-3">
				<button id="extern_event_btn" class="btn btn-warning w-100">Externe Veranstaltungen ausblenden</button>
			</div>
			<div class="col-lg-4 col-sm-12 mb-3">
				<select class="form-select" id="event-category-selector" aria-label="Default select example">
					<option selected="" value="all">Alle Veranstaltungskategorien</option>
					<?php
					foreach ($event_type_choices as $key => $value) {
						echo '<option value="' . esc_html($key) . '">' . esc_html($event_type_choices[$key]) . '</option>';
					}
					?>
				</select>
			</div>
		</div>
		<?php
		wp_reset_query();

		echo '<div id="event-list">';
		while ($copy_events->have_posts()) {
			$copy_events->the_post();
			get_template_part('template-parts/events/event-item');
		}
		echo '</div>';
		//echo paginate_links();
		echo '<p id="no-event-banner" class="text-center pb-3">Keine Veranstaltungen gefunden</p>';
		?>
		<script>
			class upcoming_events_cntrl {
				// 1. create/initiate object
				constructor() {
					this.pastEventToggleButton = document.getElementById("upcoming_event_btn");
					this.eventSelector = document.getElementById("event-category-selector");
					this.pastEventToggleState = true; // false = all events - true = only upcoming events
					this.eventCategory = "all";
					this.eventsList = document.getElementById("event-list");
					this.externEventToggleButton = document.getElementById("extern_event_btn");
					this.externEventToggleState = false;
					this.noEventFlag = true;
					this.noEventBanner = document.getElementById("no-event-banner");
					this.noEventBanner.classList.add("d-none");
					//console.log(this.eventsList.children);
					this.events();
				}

				// 2. events
				events() {
					this.pastEventToggleButton.addEventListener("click", () =>
						this.filterUpcomingEvents()
					);
					this.eventSelector.addEventListener("change", () =>
						this.filterElementsByCategory()
					);
					this.externEventToggleButton.addEventListener("click", () =>
						this.filterExternEvents()
					);
				}

				// 3. methods
				filterUpcomingEvents() {
					if (this.pastEventToggleState === false) {
						this.pastEventToggleButton.innerHTML = "Auch Vergangene anzeigen";
						this.pastEventToggleButton.className = "btn btn-primary w-100";
						this.pastEventToggleState = true;
					} else {
						this.pastEventToggleButton.innerHTML = "Nur Bevorstehende anzeigen";
						this.pastEventToggleButton.className = "btn btn-secondary w-100";
						this.pastEventToggleState = false;
					}

					this.filterEvents();
				}

				filterExternEvents() {
					if (this.externEventToggleState === false) {
						this.externEventToggleButton.innerHTML =
							"Externe Veranstaltungen einblenden";
						this.externEventToggleButton.className = "btn btn-secondary w-100";
						this.externEventToggleState = true;
					} else {
						this.externEventToggleButton.innerHTML =
							"Externe Veranstaltungen ausblenden";
						this.externEventToggleButton.className = "btn btn-warning w-100";
						this.externEventToggleState = false;
					}

					this.filterEvents();
				}

				filterElementsByCategory() {
					this.eventCategory = this.eventSelector.value;

					this.filterEvents();
				}

				filterEvents() {
					this.noEventFlag = true;
					for (var i = 0; i < this.eventsList.children.length; i++) {
						this.filterState = this.eventCategory == "all";
						this.filterState =
							this.filterState ||
							this.eventsList.children[i].classList.contains(
								this.eventCategory
							);
						this.filterState =
							this.filterState &&
							!(
								this.pastEventToggleState == true &&
								this.eventsList.children[i].classList.contains("past-event")
							);
						this.filterState =
							this.filterState &&
							!(
								this.externEventToggleState == true &&
								this.eventsList.children[i].classList.contains("extern")
							);

						if (this.filterState) {
							this.eventsList.children[i].classList.remove("d-none");
							this.noEventFlag = false;
						} else {
							this.eventsList.children[i].classList.add("d-none");
						}
					}

					if (this.noEventFlag) {
						this.noEventBanner.classList.remove("d-none");
					} else {
						this.noEventBanner.classList.add("d-none");
					}

				}
			}

			new upcoming_events_cntrl();
		</script>
	<?php
	} else {
		get_template_part('template-parts/events/event-no-events');
	}
	?>
</div>
<?php get_footer(); ?>
