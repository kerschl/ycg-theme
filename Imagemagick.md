# Formating images

Required packages are `imagemagick` and `jpegoptim`.

Type hirachie by filesize `raw > png > jpeg/jpg > .webp > .avif > svg/vector`

## Format PNG to JPEG

```bash
mogrify -format jpg *.png
```

`-format jpg` image format type

`*.png` is the file selection (everyting that ends with .png)

## Resize Images

To resize multible images use a for loop:

```bash
for var in *.jpg; do convert $var -resize "1920>x1920>" $var; done
```

`-resize "1920>x1920>"` Resizes to a maximum width and height of 1920 which is full HD

Images that are already smaller skip this step.

## Optimizing file size

#### JPEG

```bash
jpegoptim -m 92 *.jpg
```

`-m 92` Quality rate (92% is a commonly used value)

#### PNG

```bash
optipng -o 1 *.png
```

`-o [0-2]` Sets the loss rate (0: almost none - 2: some)

## Rename Pictures

```bash
name=name-here; ls | cat -n | while read n f; do EXT="${f##*.}"; mv $f $name`printf "-%03d."$EXT $n`; done
```

Var $name stores the filename. To overwrite file extension use:

```bash
name=name-here; ls | cat -n | while read n f; do mv $f $name`printf "-%03d.jpg" $n`; done
```

## See EXIF-Data

EXIF-Data:

```bash
identify -verbose image.jpg | grep "exif:"
```

All image data:

```bash
identify -verbose image.jpg
```

## Remove EXIF-Data

```bash
mogrify -strip *.jpg
```

`-strip` Removes EXIF-Data

Removing the EXIF-Data also removes the saved image rotation.

## Rotate Images

First sort the images to be oriented 90, 180 or 270 degrees in folders.
Then use a loop to correct them. `mogrify` rotates to the right.

```bash
mkdir 90 180 270
$ cd 90 && for var in *.jpg; do mogrify -rotate 90 $var; done && cd ..
$ cd 180 && for var in *.jpg; do mogrify -rotate 180 $var; done && cd ..
$ cd 270 && for var in *.jpg; do mogrify -rotate 270 $var; done && cd ..
```

## Congratulations

Now you have very clean and privacy conform images. Well made!
