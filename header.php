<?php /* The header added to each view */ ?>
<!DOCTYPE html>
<html lang="de">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= is_front_page() ? get_bloginfo('name') : wp_title('') ?></title>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/build/styles.css">
	<?php wp_head() ?>
</head>

<body <?php body_class(); ?>>
	<header id="header">
		<nav id="nav" class="navbar navbar-expand-lg navbar-dark bg-primary">
			<div class="container">
				<a class="navbar-brand" href="<?php bloginfo('wpurl'); ?>">
					<img id="logo" class="logo" alt="Yachtclub-Gollenshausen" height="60" src="<?php bloginfo('template_directory'); ?>/assets/logo/logo.svg">
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<?php if (has_nav_menu('main-menu')) {
						wp_nav_menu([
							'theme_location'  => 'main-menu',
							'menu'            => 'main-menu',
							// div
							'container'       => false,
							// ul
							'menu_class'      => 'navbar-nav ms-md-auto',
							'menu_id'         => 'main-menu',
							'walker' => new ycg_main_menu_walker()
							// declared in functions.php
							// https://developer.wordpress.org/reference/functions/wp_nav_menu/
						]);
					} ?>
					<?php get_template_part('template-parts/buttons/logout-button');					?>
				</div>
			</div>
		</nav>
	</header>
	<main id="main">
