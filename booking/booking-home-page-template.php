<?php
/* 
Template Name: Buchungsuebersicht
*/
require_once(__DIR__ . '/settings/BookingItemRulesAccordion.php');
require_once(__DIR__ . '/home-menu/UserBookingsDisplay.php');
require_once(__DIR__ . '/home-menu/ItemsDisplay.php');
require_once(__DIR__ . "/booking/BookingExceptions.php");

get_header();

if (is_user_logged_in()) {
?>
	<div id="booking">
		<h1>Buchungsportal für Vereinsboote</h1>
		<h3>Willkommen <?= $current_user->display_name ?></h3>
		<h4>Boote und Zubehör</h4>
		<?php
		$items_display = new ItemsDisplay();
		$items_display->show_items();

		try {
			if (isset($_GET["jahr"])) {
				$year = intval($_GET["jahr"]);
				if ($year == 0) {
					throw new StandardBookingException("Ungültige Anfrage", "Anfrage nicht möglich");
				}
			} else {
				$now = current_datetime();
				$year = intval($now->format("Y"));
			}
			$bookings_display = new UserBookingsDisplay($year, get_current_user_id());
			$bookings_display->show();
		} catch (StandardBookingException $e) {
			$e->show_booking_error();
		}

		echo '<h4>Buchungsregeln</h4>';
		global $wpdb;
		$rules = new BookingItemRulesAccordion($wpdb);
		$rules->show_booking_rules();

		echo '<h4>Benutzerkonto</h4>' .
			'<a class="btn btn-primary me-3" href="/' . USER_ACCOUNT_URL . '">Benutzerkonto verwalten</a>' .
			'<a class="btn btn-warning" href="' . wp_logout_url($redirect = '/' . BOOKING_HOME_URL) . '">Ausloggen</a>';

		the_content();

		?>
	</div>
<?php
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
