<?php
/* 
Template Name: Buchungsabrechnung
*/
include __DIR__ . '/billing/billing.php';

get_header();

if (is_user_logged_in()) {
	if (current_user_can('read_private_pages')) {
		$now = current_datetime();
		$year = $now->format('Y');
?>
		<div id="booking">
			<h1>Buchungsabrechnung für <?= $year ?></h1>
			<p>Achtung: Es sind alle Buchung für das entsprechende Jahr enthalten. Auch zukünftige!</p>
			<?php
			$booking_users = Billing::get_users_with_booking_in_year($year);

			if (count($booking_users) > 0) {
				foreach ($booking_users as $user) {
					$bill = new Billing($user->user_id, $year);
					$bill->show_bookings();
				}
			} else {
				echo '<p>Keine Nutzer mit Buchungen für dieses Jahr gefunden</p>';
			}
			?>
		</div>
<?php
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
