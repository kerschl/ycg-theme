<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

function insert_booking_item($item_tag, $item_type, $item_name, $img_url)
{
	global $wpdb;

	$results = $wpdb->get_results("SELECT * FROM " . BOOKING_ITEMS_TABLE . " WHERE item_tag = '" . $item_tag . "'");

	if (count($results) == 0) {
		$wpdb->insert(
			BOOKING_ITEMS_TABLE,
			array(
				'item_tag' => $item_tag,
				'item_type' => $item_type,
				'item_name' => $item_name,
				'img_url' => $img_url
			)
		);
	}
}

if (is_user_logged_in()) {
	if (current_user_can('manage_options')) {

		insert_booking_item('sb20', BOOKING_ITEM_TYPE_BOAT, 'Palito', '/wp-content/themes/ycg-theme/assets/booking/item_logos/sb20_class_logo.jpg');
		insert_booking_item('dyas', BOOKING_ITEM_TYPE_BOAT, 'Thetis', '/wp-content/themes/ycg-theme/assets/booking/item_logos/dyas_class_logo.jpg');
		insert_booking_item('emotor', BOOKING_ITEM_TYPE_GEAR, 'E-Motor', '/wp-content/themes/ycg-theme/assets/booking/item_logos/torqeedo_logo.jpg');
		insert_booking_item('opti', BOOKING_ITEM_TYPE_BOAT, 'Flipper', '/wp-content/themes/ycg-theme/assets/booking/item_logos/optimist_class_logo.jpg');
		insert_booking_item('420-0', BOOKING_ITEM_TYPE_BOAT, '420er', '/wp-content/themes/ycg-theme/assets/booking/item_logos/420_class_logo.jpg');
		insert_booking_item('420-1', BOOKING_ITEM_TYPE_BOAT, '420er', '/wp-content/themes/ycg-theme/assets/booking/item_logos/420_class_logo.jpg');

		header('Location: ' . $_POST['redirect_to']);
	}
} else {
	echo '<p>Access denied</p>';
}
