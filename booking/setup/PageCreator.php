<?php
require_once(__DIR__ . "/../../../../../wp-load.php");

class PageCreator
{
	public function __construct()
	{
		$this->pages = array();
	}

	public function register_page($page_title, $page_slug, $page_template, $page_content = "")
	{
		$page = array(
			"page_title" => $page_title,
			"page_slug" => $page_slug,
			"page_template" => $page_template,
			"page_content" => $page_content
		);

		array_push($this->pages, $page);
	}

	public function all_pages_exist_already()
	{
		$page_count = 0;

		foreach ($this->pages as $page) {
			if ($this->page_with_slug_exists($page['page_slug'])) {
				$page_count = $page_count + 1;
			}
		}

		if (count($this->pages) == $page_count) {
			return true;
		} else {
			return false;
		}
	}

	public function create_pages()
	{
		foreach ($this->pages as $page) {
			wp_insert_post(array(
				'post_title' => $page['page_title'], //The title of your post.
				'post_name' => $page['page_slug'],
				'page_template' => $page['page_template'], //Sets the template for the page.
				'post_content' => $page['page_content'], //The full text of the post.
				'post_type' => 'page', //Sometimes you want to post a page.
				'post_status' => 'publish', //Set the status of the new post.
				'comment_status' => 'closed', // 'closed' means no comments.
			));
		}
	}

	private function page_with_slug_exists($slug = '')
	{
		if (!$slug) {
			return false;
		}
		$post_object = get_page_by_path($slug, OBJECT, 'page');

		if (!$post_object) {
			return false;
		}
		return true;
	}
}
