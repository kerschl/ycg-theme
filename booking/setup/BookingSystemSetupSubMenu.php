<?php

/**
 * Menu to create the DB and the Pages required for the booking system
 *
 * @author Christoph
 */
class BookingSystemSetupSubMenu
{

	/**
	 * Autoload method
	 * @return void
	 */
	public function __construct()
	{
		add_action('admin_menu', array(&$this, 'register_sub_menu'));
	}

	/**
	 * Register submenu
	 * @return void
	 */
	public function register_sub_menu()
	{
		add_submenu_page(
			'buchungen', // parent page slug
			'Buchungsportal aufsetzen',
			'Setup',
			'manage_options',
			'buchungsportal_aufsetzen',
			array(&$this, 'create_booking_pages'),
			10 // menu position
		);
	}

	/**
	 * Render submenu
	 * @return void
	 */
	public function create_booking_pages()
	{
?>
		<div class="wrap">
			<h1><?= get_admin_page_title() ?></h1>
			<form method="post" action="<?= get_template_directory_uri(); ?>/booking/setup/setup-pages.php">
				<input type="hidden" value="<?= esc_attr($_SERVER['REQUEST_URI']); ?>" name="redirect_to"><br>
				<input class="button button-primary" type="submit" value="Seiten für Buchungsportal anlegen">
			</form>
			<form method="post" action="<?= get_template_directory_uri(); ?>/booking/setup/setup-db.php">
				<input type="hidden" value="<?= esc_attr($_SERVER['REQUEST_URI']); ?>" name="redirect_to"><br>
				<input class="button button-primary" type="submit" value="Datenbank für Buchungsportal anlegen">
			</form>
			<form method="post" action="<?= get_template_directory_uri(); ?>/booking/setup/setup-user-roles.php">
				<input type="hidden" value="<?= esc_attr($_SERVER['REQUEST_URI']); ?>" name="redirect_to"><br>
				<input class="button button-primary" type="submit" value="Benutzerrollen für Buchungsportal anlegen">
			</form>
			<form method="post" action="<?= get_template_directory_uri(); ?>/booking/setup/setup-ycg-items.php">
				<input type="hidden" value="<?= esc_attr($_SERVER['REQUEST_URI']); ?>" name="redirect_to"><br>
				<input class="button button-primary" type="submit" value="YCG Boote und Zubehör anlegen">
			</form>
		</div>
<?php
	}
}
