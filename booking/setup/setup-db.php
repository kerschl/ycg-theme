<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

if (is_user_logged_in()) {
	if (current_user_can('manage_options')) {
		global $wpdb;

		// Create db table for boat bookings
		$boat_booking_table_name = BOOKINGS_TABLE;
		if ($wpdb->get_var("SHOW TABLES LIKE '$boat_booking_table_name'") != $boat_booking_table_name) {
			//table not in database. Create new table
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $boat_booking_table_name (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				user_id bigint(20) NOT NULL,
				boat text NULL,
				booking_created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				booking_date date DEFAULT '0000-00-00' NOT NULL,
				booking_start_time time DEFAULT '00:00:00' NOT NULL,
				booking_end_time time DEFAULT '00:00:00' NOT NULL,
				booking_type text NULL,
				booking_cost decimal(15,2) NOT NULL,
				booking_comment text NULL,
				logbook_created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				logbook_crew text NULL,
				logbook_time_takeover text NULL,
				logbook_time_return text NULL,
				logbook_defects_on_arrival text NULL,
				logbook_weather text NULL,
				logbook_damage_report text NULL,
				logbook_etc text NULL,
				UNIQUE KEY id (id)
			) $charset_collate;";
			dbDelta($sql);
		}

		// Create db table for booking items
		$item_table_name = BOOKING_ITEMS_TABLE;
		if ($wpdb->get_var("SHOW TABLES LIKE '$item_table_name'") != $item_table_name) {
			//table not in database. Create new table
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $item_table_name (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				item_type text NULL,
				item_tag text NULL,
				item_name text NULL,
				item_description text NULL,
				booking_enabled BIT NOT NULL DEFAULT 0,
				reason_booking_disabled text NULL,
				max_number_bookings_ahead INT NOT NULL DEFAULT 3,
				double_booking_on_weekend BIT NOT NULL DEFAULT 0,
				cancel_deadline INT NOT NULL DEFAULT 6,
				no_rules_apply_deadline INT NOT NULL DEFAULT 6,
				booking_price_youth decimal(15,2) NOT NULL,
				booking_price_adult decimal(15,2) NOT NULL,
				img_url text NULL,
				UNIQUE KEY id (id)
			) $charset_collate;";
			dbDelta($sql);
		}

		// Create db table for Maintenance notes
		$mn_table_name = MAINTENANCE_NOTES_TABLE;
		if ($wpdb->get_var("SHOW TABLES LIKE '$mn_table_name'") != $mn_table_name) {
			//table not in database. Create new table
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $mn_table_name (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				user_id bigint(20) NOT NULL,
				item_tag text NULL,
				created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				note text NULL,
				UNIQUE KEY id (id)
			) $charset_collate;";
			dbDelta($sql);
		}
		header('Location: ' . $_POST['redirect_to']);
	}
} else {
	echo '<p>Access denied</p>';
}
