<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(__DIR__ . "/PageCreator.php");

if (is_user_logged_in()) {
	if (current_user_can('manage_options')) {

		$BookingPageCreator = new PageCreator;
		$BookingPageCreator->register_page("Buchungsportal - Startseite", BOOKING_HOME_URL, "booking/booking-home-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Buchungkalender", BOOKING_CALENDAR_URL, "booking/booking-calendar-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Logbuch", LOGBOOK_URL, "booking/logbook-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Logbucheintrag", LOGBOOK_ENTRY_URL, "booking/logbook-create-entry-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Buchung anlegen", CREATE_BOOKING_URL, "booking/booking-create-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Buchung stornieren", CANCEL_BOOKING_URL, "booking/booking-delete-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Wartungsnotiz", MAINTENANCE_NOTE_URL, "booking/maintenance_note-create-entry-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Benutzerkonto", USER_ACCOUNT_URL, "booking/user-settings-page-template.php");
		$BookingPageCreator->register_page("Buchungsportal - Abrechnung", BILLING_URL, "booking/billing-page-template.php");

		if (!$BookingPageCreator->all_pages_exist_already()) {
			$BookingPageCreator->create_pages();
			header('Location: ' . $_POST['redirect_to']);
		} else {
			echo 'All or some of the pages required for the booking system exist already. Please delte them and try again.';
		}
	}
} else {
	echo '<p>Access denied</p>';
}
