<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(__DIR__ . "/PageCreator.php");

if (is_user_logged_in()) {
	if (current_user_can('manage_options')) {
		add_role('customer', 'Kunde', array());
		header('Location: ' . $_POST['redirect_to']);
	}
} else {
	echo '<p>Access denied</p>';
}
