<?php
/* 
Template Name: Logbucheintrag
*/
require_once(__DIR__ . "/logbook/LogbookEntryDialog.php");
require_once(__DIR__ . "/booking/BookingExceptions.php");

get_header();

if (is_user_logged_in()) {
	try {
		if (isset($_GET["id"])) {
			$lb = LogbookEntryDialog::get_logbook_entry_by_id(intval($_GET["id"]));
			$lb->check_if_user_is_allowed_to_edit_entry(get_current_user_id());
			$lb->show_dialog();
		} elseif (isset($_POST["booking_id"])) {
			$lb = LogbookEntryDialog::get_logbook_entry_by_id($_POST["booking_id"]);
			$lb->check_if_user_is_allowed_to_edit_entry(get_current_user_id());

			if ($lb->check_if_all_post_args_are_set()) {
				$lb->load_entries_from_POST();
				$lb->update();
				$lb->show_update_confirmation();
			} else {
				throw new StandardBookingException("Nicht alle benötigten Felder ausgefüllt", "Logbucheintrag kann nicht erstellt werden");
			}
		} else {
			throw new StandardBookingException("Ungültige Anfrage", "Logbucheintrag kann nicht erstellt werden");
		}
	} catch (StandardBookingException $e) {
		$e->show_booking_error();
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
