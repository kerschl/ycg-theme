<?php

/**
 * @property	integer	$id
 * @property	integer	$user_id
 * @property	string	$item_type
 * @property	string	$boat
 * @property	DateTime $booking_created
 * @property	DateTime $booking_date
 * @property	Time	$booking_start_time
 * @property	Time	$booking_end_time
 * @property	String $booking_type
 * @property	float	$booking_cost
 * @property	string	$booking_comment
 * @property	DateTime	$logbook_created
 * @property	string	$logbook_crew
 * @property	string	$logbook_time_takeover
 * @property	string	$logbook_time_return
 * @property	string	$logbook_defects_on_arrival
 * @property	string	$logbook_weather
 * @property	string	$logbook_damage_report
 * @property	string	$logbook_etc
 */
class Booking
{
	public function __construct($row = null)
	{
		if (isset($row)) {
			$this->id = intval($row->id);
			$this->user_id = intval($row->user_id);
			$this->item_type = ($row->item_type == BOOKING_ITEM_TYPE_BOAT) ? BOOKING_ITEM_TYPE_BOAT : BOOKING_ITEM_TYPE_GEAR;
			$this->boat = $row->boat;
			$this->booking_created = new DateTime($row->booking_created, wp_timezone());
			$this->booking_date = new DateTime($row->booking_date, wp_timezone());
			$this->booking_start_time = new DateTime($row->booking_start_time, wp_timezone());
			$this->booking_end_time = new DateTime($row->booking_end_time, wp_timezone());
			$this->booking_type = $row->booking_type;
			$this->booking_cost = floatval($row->booking_cost);
			$this->booking_comment = $row->booking_comment;
			$this->logbook_created = new DateTime($row->logbook_created, wp_timezone());
			$this->logbook_crew = $row->logbook_crew;
			$this->logbook_time_takeover = $row->logbook_time_takeover;
			$this->logbook_time_return = $row->logbook_time_return;
			$this->logbook_defects_on_arrival = $row->logbook_defects_on_arrival;
			$this->logbook_weather = $row->logbook_weather;
			$this->logbook_damage_report = $row->logbook_damage_report;
			$this->logbook_etc = $row->logbook_etc;
		} else {
			$this->id = null;
			$this->user_id = null;
			$this->item_type = null;
			$this->boat = null;
			$this->booking_created = null;
			$this->booking_date = null;
			$this->booking_start_time = null;
			$this->booking_end_time = null;
			$this->booking_type = null;
			$this->booking_cost = null;
			$this->booking_comment = null;
			$this->logbook_created = null;
			$this->logbook_crew = null;
			$this->logbook_time_takeover = null;
			$this->logbook_time_return = null;
			$this->logbook_defects_on_arrival = null;
			$this->logbook_weather = null;
			$this->logbook_damage_report = null;
			$this->logbook_etc = null;
		}
	}

	public static function get_booking_by_id(int $id)
	{
		return self::query_booking(
			"SELECT `" . BOOKINGS_TABLE . "`.*, `" . BOOKING_ITEMS_TABLE . "`.item_type " .
				"FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
				"WHERE `" . BOOKINGS_TABLE . "`.id = " . $id
		);
	}

	public static function get_booking_by_tag_and_date(String $tag, DateTime $date)
	{
		return self::query_booking(
			"SELECT `" . BOOKINGS_TABLE . "`.*, `" . BOOKING_ITEMS_TABLE . "`.item_type " .
				"FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
				"WHERE `" . BOOKINGS_TABLE . "`.booking_date = '" . $date->format('Y-m-d') . "' AND `" . BOOKINGS_TABLE . "`.boat = '" . $tag . "'"
		);
	}

	public static function get_future_bookings_by_tag_and_user_id(String $tag, int $user_id)
	{
		$date = current_datetime();
		return self::query_booking(
			"SELECT `" . BOOKINGS_TABLE . "`.*, `" . BOOKING_ITEMS_TABLE . "`.item_type " .
				"FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
				"WHERE `" . BOOKINGS_TABLE . "`.booking_date >= '" . $date->format('Y-m-d') . "' AND `" . BOOKINGS_TABLE . "`.boat = '" . $tag . "' AND `" . BOOKINGS_TABLE . "`.user_id = " . $user_id .
				" ORDER BY `" . BOOKINGS_TABLE . "`.booking_date DESC"
		);
	}

	public static function get_past_bookings_of_user(int $user_id)
	{
		$date = current_datetime();
		return self::query_booking(
			"SELECT `" . BOOKINGS_TABLE . "`.*, `" . BOOKING_ITEMS_TABLE . "`.item_type " .
				"FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
				"WHERE `" . BOOKINGS_TABLE . "`.booking_date < '" . $date->format('Y-m-d') . "' AND `" . BOOKINGS_TABLE . "`.user_id = '" . $user_id . "' ORDER BY `" . BOOKINGS_TABLE . "`.booking_date"
		);
	}

	public static function get_booking_by_tag_and_date_and_user_id(String $tag, DateTime $date, int $user_id)
	{
		return self::query_booking(
			"SELECT `" . BOOKINGS_TABLE . "`.*, `" . BOOKING_ITEMS_TABLE . "`.item_type " .
				"FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
				"WHERE `" . BOOKINGS_TABLE . "`.booking_date = '" . $date->format('Y-m-d') . "' AND `" . BOOKINGS_TABLE . "`.boat = '" . $tag . "' AND `" . BOOKINGS_TABLE . "`.user_id = '" . $user_id . "'"
		);
	}

	public static function get_bookings_of_user_by_year(int $user_id, int $year)
	{
		return self::query_booking(
			"SELECT `" . BOOKINGS_TABLE . "`.*, `" . BOOKING_ITEMS_TABLE . "`.item_type " .
				"FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
				"WHERE YEAR(`" . BOOKINGS_TABLE . "`.booking_date) = '" . $year . "' AND `" . BOOKINGS_TABLE . "`.user_id = '" . $user_id . "' ORDER BY `" . BOOKINGS_TABLE . "`.booking_date DESC "
		);
	}

	public static function get_bookings_by_tag_and_year(string $item_tag, int $year)
	{
		return self::query_booking(
			"SELECT `" . BOOKINGS_TABLE . "`.*, `" . BOOKING_ITEMS_TABLE . "`.item_type " .
				"FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
				"WHERE YEAR(`" . BOOKINGS_TABLE . "`.booking_date) = '" . $year . "' AND `" . BOOKINGS_TABLE . "`.boat = '" . $item_tag . "' ORDER BY `" . BOOKINGS_TABLE . "`.booking_date DESC "
		);
	}

	public static function get_available_years()
	{
		global $wpdb;
		$years = $wpdb->get_results(
			"SELECT DISTINCT YEAR(`booking_date`) AS year FROM `" . BOOKINGS_TABLE . "` ORDER BY YEAR(`booking_date`) DESC"
		);

		$year_list = [];
		foreach ($years as $year) {
			array_push($year_list, intval($year->year));
		}
		return $year_list;
	}

	private static function query_booking($request)
	{
		global $wpdb;
		$bookings = $wpdb->get_results($request);

		$booking_list = [];
		foreach ($bookings as $booking) {
			array_push($booking_list, new static($booking));
		}

		return $booking_list;
	}

	public static function create_booking($user_id, $item_tag, DateTime $booking_date, $booking_start_time, $booking_end_time, $booking_type, $booking_cost, $booking_comment)
	{
		$now = current_datetime();

		global $wpdb;
		$wpdb->insert(BOOKINGS_TABLE, array(
			"user_id" => $user_id,
			"boat" => $item_tag,
			"booking_created" => $now->format("Y-m-d H:i:s"),
			"booking_date" => $booking_date->format("Y-m-d"),
			"booking_start_time" => $booking_start_time,
			"booking_end_time" => $booking_end_time,
			"booking_type" => $booking_type,
			"booking_cost" => $booking_cost,
			"booking_comment" => $booking_comment
		));
		return $wpdb->insert_id;
	}

	public static function delete_booking_by_id(int $id)
	{
		global $wpdb;
		$wpdb->delete(BOOKINGS_TABLE, array('id' => $id));
	}

	public function update()
	{
		global $wpdb;
		$wpdb->update(
			BOOKINGS_TABLE,
			array(
				'user_id' => $this->user_id,
				'boat' => $this->boat,
				'booking_created' => $this->booking_created->format("Y-m-d H:i:s"),
				'booking_date' => $this->booking_date->format("Y-m-d"),
				'booking_type' => $this->booking_type,
				'booking_cost' => $this->booking_cost,
				'booking_comment' => $this->booking_comment,
				'logbook_created' => $this->logbook_created->format("Y-m-d H:i:s"),
				'logbook_crew' => $this->logbook_crew,
				'logbook_time_takeover' => $this->logbook_time_takeover,
				'logbook_time_return' => $this->logbook_time_return,
				'logbook_defects_on_arrival' => $this->logbook_defects_on_arrival,
				'logbook_weather' => $this->logbook_weather,
				'logbook_damage_report' => $this->logbook_damage_report,
				'logbook_etc' => $this->logbook_etc
			),
			array('id' => $this->id)
		);
	}
}
