<?php
require_once(__DIR__ . "/../user/BookingUser.php");
require_once(__DIR__ . "/Booking.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");
/**
 * custom exception class for general Booking errors
 */
class StandardBookingException extends Exception
{
	public function __construct($message, $heading = "Buchung nicht möglich", $code = 0, Throwable $previous = null)
	{
		// make sure everything is assigned properly
		parent::__construct($message, $code, $previous);
		$this->heading = $heading;
	}

	private $heading;

	public function show_booking_error()
	{
		echo '<div class="text-center" id="booking">' .
			'<h1>' . $this->heading . '</h1>' .
			'<p>' . $this->message . '</p>';
		'</div>';
	}
}

/**
 * custom exception class for already booked dates
 * @property Booking	$booking
 * @property BookingUser	$user
 * @property BookingItemModel $booking_item
 */
class DateAlreadyBookedException extends Exception
{
	public function __construct(Booking $booking, $code = 0, Throwable $previous = null)
	{
		$this->booking = $booking;
		$this->user = new BookingUser($booking->user_id);
		$this->booking_item = new BookingItemModel();
		$this->booking_item->load_by_item_tag($this->booking->boat);

		// make sure everything is assigned properly
		parent::__construct("Datum bereits gebucht", $code, $previous);
	}

	public function show_booking_error()
	{
		echo '<div class="text-center" id="booking">' .
			'<h1>Datum bereits gebucht</h1>' .
			'<p>Boot/Zubehör: ' . $this->booking_item->item_name . '</p>' .
			'<p>Datum: ' . $this->booking->booking_date->format('d.m.Y') . '</p>' .
			'<p>Gebucht von ' . $this->user->display_name . '</p>' .
			'<p>Telefonnummer: ' . $this->user->get_phone_number() . '</p>' .
			'<p>Anmerkung: ' . nl2br($this->booking->booking_comment) . '</p>' .
			'</div>';
	}
}

/**
 * custom exception class for exceeding the maximum number of bookings
 * @property Booking	$bookings
 * @property BookingItemModel $booking_item
 * @property DateTime	$no_rules_apply_time
 */
class MaxNumberOfFutureBookingsReachedException extends Exception
{
	public function __construct(array $bookings, DateTime $no_rules_apply_time, $code = 0, Throwable $previous = null)
	{
		$this->bookings = $bookings;
		$this->booking_item = new BookingItemModel();
		$this->booking_item->load_by_item_tag($this->bookings[0]->boat);
		$this->no_rules_apply_time = $no_rules_apply_time;

		// make sure everything is assigned properly
		parent::__construct("Datum bereits gebucht", $code, $previous);
	}

	public function show_booking_error()
	{
		echo '<div class="text-center" id="booking">' .
			'<h1>Maximale Anzahl an Buchungen erreicht</h1>' .
			'<p>Du kannst maximal  <b>' . $this->booking_item->max_number_bookings_ahead . '</b> Buchungen im voraus für ' . $this->booking_item->item_name . ' tätigen. ' .
			'Wenn ein Tag abgesegelt ist, kann ein neuer gebucht werden.</p>' .
			'<p>An folgenden Tagen hast du bereits eine Buchung für ' . $this->booking_item->item_name . ' angelegt: </p>';

		foreach ($this->bookings as $booking) {
			echo '<p><b>' . $booking->booking_date->format('d.m.Y') . '</b></p>';
		}

		echo '<p>Diese Beschränkung ist ab dem ' . $this->no_rules_apply_time->format('d.m.Y') . ' um ' . $this->no_rules_apply_time->format('G:i') . ' Uhr für dieses Datum aufgehoben</p>' .
			'</div>';
	}
}

/**
 * custom exception class for uncomplete logbooks
 * @property array $bookings
 */
class LogbooksNotCompleteException extends Exception
{
	public function __construct(array $bookings, $code = 0, Throwable $previous = null)
	{
		$this->bookings = $bookings;

		// make sure everything is assigned properly
		parent::__construct("Logbucheinträge unvollständig", $code, $previous);
	}

	public function show_booking_error()
	{
		echo '<div class="text-center" id="booking">' .
			'<h1>Logbuch unvollständig</h1>' .
			'<p>Du musst zuerst alle Logbucheinträge vergangener Buchungen vervollständigen, bevor du eine neue Buchung anlegen kannst. ' .
			'Für folgende Buchungen sind die Logbucheinträge nicht vollständig:</p>';

		foreach ($this->bookings as $booking) {
			$booking_item = new BookingItemModel();
			$booking_item->load_by_item_tag($booking->boat);
			$edit_url = LOGBOOK_ENTRY_URL . '?' . http_build_query(array('id' => $booking->id));
			echo '<div><a href="' . $edit_url . '">' . $booking_item->item_name . ' am ' . $booking->booking_date->format('d.m.Y') . '</a></div>';
		}
		echo '</div>';
	}
}
