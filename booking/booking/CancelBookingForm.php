<?php
require_once(__DIR__ . "/../settings/BookingItemModel.php");
require_once(__DIR__ . "/Booking.php");
require_once(__DIR__ . "/BookingExceptions.php");

/**
 * @property Booking	$booking
 * @property DateTime	$cancel_deadline_datetime
 */

class CancelBookingForm extends BookingItemModel
{
	public function __construct(int $booking_id)
	{
		$this->booking = Booking::get_booking_by_id($booking_id);

		if (count($this->booking) != 1) {
			throw new StandardBookingException("Diese Buchung existiert nicht");
		} else {
			$this->booking = $this->booking[0];
			$this->load_by_item_tag($this->booking->boat);

			$this->cancel_deadline_datetime = clone $this->booking->booking_date;
			$this->cancel_deadline_datetime->setTime(0, 0);
			$this->cancel_deadline_datetime = $this->cancel_deadline_datetime->sub(new DateInterval("PT" . $this->cancel_deadline . "H"));
		}
	}

	public function user_is_allowed_to_delte_booking()
	{
		if ($this->booking->user_id != get_current_user_id()) {
			throw new StandardBookingException("Diese Buchung ist nicht von dir");
		}

		$now = current_datetime();
		if ($this->cancel_deadline_datetime < $now) {
			throw new StandardBookingException("Die Stornierungsfrist für diese Buchung ist abgelaufen");
		}
		return true;
	}

	public function delete_booking()
	{
		Booking::delete_booking_by_id($this->booking->id);
	}

	public function show_cancel_confirmation()
	{
		echo '<div class="text-center" id="booking">' .
			'<h1>Buchung storniert</h1>' .
			'<p>Buchung für ' . $this->item_name . ' am ' . $this->booking->booking_date->format("d.m.Y") . ' storniert</p>' .
			'<a class="btn btn-primary" href="/' . BOOKING_HOME_URL . '">Zurück zum Buchungsportal</a>' .
			'</div>';
	}

	public function show()
	{
		if ($this->booking->booking_type == BOOKING_RATE_ADULT) {
			$booking_rate = "Erwachsene";
		} elseif ($this->booking->booking_type == BOOKING_RATE_YOUTH) {
			$booking_rate = "Jugendliche";
		} else {
			$booking_rate = "Regatta";
		}
?>
		<div id="booking">
			<h1>Buchung Stornieren</h1>
			<p>Diese Buchung kann noch bis zum <?= $this->cancel_deadline_datetime->format("d.m.Y"); ?> um <? echo $this->cancel_deadline_datetime->format("G:i"); ?> Uhr storniert werden</p>
			<div class="my-3">
				<form action="<?= site_url('/' . CANCEL_BOOKING_URL); ?>" method="post">
					<input id="booking_id" type="text" name="booking_id" value="<?= $this->booking->id; ?>" hidden>
					<div class="mb-3">
						<label for="boot">Boot/Zubehör:</label>
						<input id="boot" type="text" name="boot" value="<?= $this->item_name; ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="booking_date">Buchungsdatum:</label>
						<input id="booking_date" type="text" name="booking_date" value="<?= $this->booking->booking_date->format("d.m.Y"); ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="booking_type">Buchungsart:</label>
						<input id="booking_type" type="text" name="booking_type" value="<?= ($booking_rate); ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="booking_cost">Gebühr:</label>
						<input id="booking_cost" type="text" name="booking_cost" value="<?= number_format($this->booking->booking_cost, 2); ?> €" readonly>
					</div>
					<button class="btn btn-danger" type="submit">Jetzt stornieren</button>
				</form>
			</div>
		</div>
<?php
	}
}
