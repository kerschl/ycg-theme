<?php

/**
 * @property	integer $ID
 * @property	string	$name
 * @property	string	$category
 * @property	DateTime	$date
 * @property	boolean		$extern  
 */
class Event
{
	public function __construct()
	{
		$this->ID = get_the_ID();
		$this->name = get_the_title();
		$this->category = get_field('veranstaltungskategorie')['value'];
		$this->date = new DateTime(get_field("veranstaltungsdatum"), wp_timezone());
		$this->extern = (get_field('Veranstaltungextern') == null) ? False : True;
	}

	public function is_reserved_for_regatta_bookings(string $boat_tag)
	{
		$meta_value = get_post_meta($this->ID, BOOKING_ITEM_EVENT_META_PREFIX . $boat_tag)[0];
		return ($meta_value == BOOKING_ITEM_IS_BLOCKED_FOR_EVENT) ? true : false;
	}
}
