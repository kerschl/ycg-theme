<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");
require_once(__DIR__ . "/../user/BookingUser.php");
require_once(__DIR__ . "/BookingExceptions.php");
require_once(__DIR__ . "/EventCalendar.php");

/**
 * @property	BookingUser	$user
 * @property	DateTime	$booking_date
 * @property	DateTime	$no_rules_apply_time
 */
class NewBooking extends BookingItemModel
{
	public function __construct($user_id, DateTime $booking_date, $item_tag)
	{
		parent::__construct();

		$this->user = new BookingUser($user_id);
		$this->booking_date = $booking_date;

		if (in_array($item_tag, self::get_all_booking_item_tags())) {
			$this->load_by_item_tag($item_tag);

			$this->no_rules_apply_time = clone $this->booking_date;
			$this->no_rules_apply_time->setTime(0, 0);
			$this->no_rules_apply_time = $this->no_rules_apply_time->sub(new DateInterval("PT" . $this->no_rules_apply_deadline . "H"));
		} else {
			throw new StandardBookingException('Ungültige Auswahl für Boot oder Zubehör');
		}
	}

	public function user_is_allowed_to_make_booking()
	{
		if ($this->date_is_in_the_past()) {
			throw new StandardBookingException('Das Buchungsdatum liegt in der Vergangenheit');
		}

		if (!$this->is_enabled_for_booking()) {
			throw new StandardBookingException('Buchung derzeit deaktiviert: ' . $this->reason_booking_disabled);
		}

		if (!$this->user_has_phone_number_set()) {
			throw new StandardBookingException('Deine Telefonnummer ist noch nicht hinterlegt.' .
				' Bevor du keine Nummer hinterlegt hast, kannst du keine Buchungen tätigen.' .
				' Du kannst dies hier <a class="" href="/' . USER_ACCOUNT_URL . '">hier</a> nachholen');
		}

		// Check if boat is available
		$booking = Booking::get_booking_by_tag_and_date($this->item_tag, $this->booking_date);
		if (count($booking) > 0) {
			throw new DateAlreadyBookedException($booking[0]);
		}

		if (!$this->user->has_access_to_item($this->item_tag)) {
			throw new StandardBookingException('Du hast keinen Zugriff auf dieses Boot oder Zubehör');
		}

		// Check if all old logbook entries are filled
		$bookings_with_uncomplete_logbooks = $this->get_past_bookings_with_incomplete_logbooks();
		if (count($bookings_with_uncomplete_logbooks) > 0) {
			throw new LogbooksNotCompleteException($bookings_with_uncomplete_logbooks);
		}

		// Check wether user is a trainer
		if ($this->user->get_user_role() == BOOKING_USER_ROLE_NORMAL) {

			// Check wether no rules deadline is already passed
			$now = current_datetime();
			if ($now < $this->no_rules_apply_time) {
				// Check if maximum number of bookings is not yet exceeded
				if ($this->max_number_bookings_ahead > 0) {
					$bookings = Booking::get_future_bookings_by_tag_and_user_id($this->item_tag, $this->user->ID);
					if (count($bookings) >= $this->max_number_bookings_ahead) {
						throw new MaxNumberOfFutureBookingsReachedException($bookings, $this->no_rules_apply_time);
					}
				}

				// Check if a booking for the weekend already exists
				if ($this->double_booking_on_weekend == false) {
					if ($this->if_booking_for_the_weekend_already_exists()) {
						throw new StandardBookingException("Für dieses Wochenende hast du bereits eine Buchung für " . $this->item_name . " angelegt. " .
							"Es ist nicht erlaubt " . $this->item_name . " für ein komplettes Wochenende zu buchen." .
							" Diese Beschränkung ist ab dem " . $this->no_rules_apply_time->format('d.m.Y') . " um " . $this->no_rules_apply_time->format('G:i') .
							" Uhr für dieses Datum aufgehoben.");
					}
				}
			}
		}
		return true;
	}

	public function create_booking($booking_type, $booking_comment)
	{
		if ($booking_type == BOOKING_RATE_YOUTH || $booking_type == BOOKING_RATE_ADULT) {
			$this->add_booking_to_db($booking_type, $booking_comment);
		} else {
			throw new StandardBookingException('Ungültiger Buchungstarif');
		}
	}

	private function add_booking_to_db($booking_type, $booking_comment)
	{
		if ($booking_type == BOOKING_RATE_ADULT) {
			$booking_price = $this->booking_price_adult;
		} else {
			$booking_price = $this->booking_price_youth;
		}

		Booking::create_booking(
			$this->user->ID,
			$this->item_tag,
			$this->booking_date,
			'00:00:00',
			'23:59:59',
			$booking_type,
			$booking_price,
			$booking_comment
		);
	}

	private function date_is_in_the_past()
	{
		$now = current_datetime();
		return $this->booking_date <= $now;
	}

	private function is_enabled_for_booking()
	{
		return $this->booking_enabled;
	}

	private function user_has_phone_number_set()
	{
		return $this->user->get_phone_number() != '';
	}

	private function get_past_bookings_with_incomplete_logbooks()
	{
		$bookings = Booking::get_past_bookings_of_user($this->user->ID);
		$uncomplete_logbooks = [];
		foreach ($bookings as $booking) {
			if (!$this->logbook_is_complete($booking)) {
				array_push($uncomplete_logbooks, $booking);
			}
		}
		return $uncomplete_logbooks;
	}

	private function logbook_is_complete(Booking $booking)
	{
		if ($booking->item_type == BOOKING_ITEM_TYPE_BOAT) {
			return $this->boat_logbook_is_complete($booking);
		} else {
			return $this->gear_logbook_is_complete($booking);
		}
	}

	private function boat_logbook_is_complete(Booking $booking)
	{
		if ($this->logbook_entry_is_empty($booking->logbook_crew)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_time_takeover)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_time_return)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_defects_on_arrival)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_weather)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_damage_report)) {
			return false;
		} else {
			return true;
		}
	}

	private function gear_logbook_is_complete(Booking $booking)
	{
		if ($this->logbook_entry_is_empty($booking->logbook_time_takeover)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_time_return)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_defects_on_arrival)) {
			return false;
		} elseif ($this->logbook_entry_is_empty($booking->logbook_damage_report)) {
			return false;
		} else {
			return true;
		}
	}

	private function logbook_entry_is_empty($entry)
	{
		return ($entry == null || $entry == '');
	}

	private function if_booking_for_the_weekend_already_exists()
	{
		$day_of_the_week = intval($this->booking_date->format('N'));

		// For Saturday
		if ($day_of_the_week == 6) {
			$temp_booking_date = clone $this->booking_date;
			$sunday = $temp_booking_date->add(new DateInterval("P1D"));
			$booking = Booking::get_booking_by_tag_and_date_and_user_id($this->item_tag, $sunday, $this->user->ID);
			if (count($booking) > 0) {
				return true;
			}
			// For Sunday
		} elseif ($day_of_the_week == 7) {
			$temp_booking_date = clone $this->booking_date;
			$saturday = $temp_booking_date->sub(new DateInterval("P1D"));
			$booking = Booking::get_booking_by_tag_and_date_and_user_id($this->item_tag, $saturday, $this->user->ID);
			if (count($booking) > 0) {
				return true;
			}
		} else {
			return false;
		}
	}

	public function show_booking_confirmation()
	{
		echo '<div class="text-center" id="booking">' .
			'<h1>Buchung abgeschlossen</h1>' .
			'<p>' . $this->item_name . ' gebucht für ' . $this->booking_date->format("d.m.Y") . '</p>' .
			'<a class="btn btn-primary" href="/' . BOOKING_HOME_URL . '">Zurück zum Buchungsportal</a>' .
			'</div>';
	}
}
