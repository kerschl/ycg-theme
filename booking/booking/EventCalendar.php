<?php
require_once(__DIR__ . "/Event.php");

class EventCalendar
{
	static public function get_event_for_date(DateTime $date)
	{
		$event_query_settings = array(
			'posts_per_page' => -1,
			'post_type' => 'event',
			'meta_key' => 'veranstaltungsdatum',
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'veranstaltungsdatum',
					'compare' => '==',
					'value' => $date->format("Ymd"),
					'type' => 'numeric'
				)
			)
		);

		wp_reset_query();
		$events = new WP_Query($event_query_settings);

		if ($events->have_posts()) {
			while ($events->have_posts()) {
				$events->the_post();
				return new Event();
			}
		}

		return null;
	}
}
