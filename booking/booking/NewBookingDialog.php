<?php
//require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(__DIR__ . "/NewBooking.php");

/**
 * @property	DateTime	$booking_date
 * @property	DateTime	$no_rules_apply_time
 */
class NewBookingDialog extends NewBooking
{
	public function __construct($user_id, DateTime $booking_date, $item_tag)
	{
		parent::__construct($user_id, $booking_date, $item_tag);
	}

	public function show_booking_form()
	{
		$event = EventCalendar::get_event_for_date($this->booking_date);
		$now = current_datetime();

		if ($event and $now < $this->no_rules_apply_time) {
			if ($event->is_reserved_for_regatta_bookings($this->item_tag)) {
				$this->show_booking_form_start($event->name);
				$this->show_booking_rate_selector();
				$this->show_booking_form_end();
				return;
			}
		}
		$this->show_booking_form_start();
		$this->show_booking_rate_selector();
		$this->show_booking_form_end();
	}

	private function show_booking_form_start(string $regatta_notice = null)
	{
		echo '<div id="booking">' .
			'<h1>Buchungsformular</h1>' .
			'<h3>Buchung für ' . $this->user->display_name . '</h3>';

		if ($regatta_notice) {
			echo '<p>Achtung! An diesem Datum findet folgende Veranstaltung statt: <b>' . $regatta_notice . '</b></p>';
		}
		echo '<div class="my-3">' .
			'<form action="' . site_url('/' . CREATE_BOOKING_URL) . '" method="post">';
?>
		<input type="hidden" id="item_tag" name="item_tag" value="<?= $this->item_tag; ?>">
		<input type="hidden" id="booking_date" name="booking_date" value="<?= $this->booking_date->format("Y-m-d"); ?>">
		<div class="mb-3">
			<label for="item_name">Boot:</label>
			<input id="item_name" type="text" name="item_name" value="<?= $this->item_name; ?>" readonly>
		</div>
		<div class="mb-3">
			<label for="booking_date_visible">Datum:</label>
			<input id="booking_date_visible" type="text" name="booking_date_visible" value="<?= $this->booking_date->format('d.m.Y'); ?>" readonly>
		</div>
	<?php
	}

	private function show_booking_form_end()
	{
	?>
		<div class="mb-3">
			<label for="booking_comment">Anmerkung:</label>
			<textarea id="booking_comment" type="text" name="booking_comment"></textarea>
			<div class="form-text">Falls das Boot für eine Regatta gebucht wird, bitte unter Anmerkung den Namen der Regatta eintragen.</div>
		</div>
		<button class="btn btn-primary" type="submit">Jetzt kostenpflichtig buchen</button>
		</form>
		</div>
		</div>
	<?php
	}

	public function show_booking_rate_selector()
	{
	?>
		<div class="mb-3">
			<label for="booking_type">Buchungsart:</label>
				<?php
				if ($this->user->get_booking_rate() == BOOKING_RATE_YOUTH) {
					echo '<input type="text" value="Jugendliche - ' . number_format($this->booking_price_youth, 2) . '€" readonly>';
					echo '<input type="hidden" id="booking_type" name="booking_type" value="' . BOOKING_RATE_YOUTH . '">';
				} else {
					echo '<input type="text" value="Erwachsene - ' . number_format($this->booking_price_adult, 2) . '€" readonly>';
					echo '<input type="hidden" id="booking_type" name="booking_type" value="' . BOOKING_RATE_ADULT . '">';
				}
				?>
		</div>
	<?php
	}
}
