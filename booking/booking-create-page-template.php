<?php
/* 
Template Name: Buchung-Anlegen
*/
require_once(__DIR__ . '/booking/NewBookingDialog.php');
require_once(__DIR__ . '/booking/NewBooking.php');
require_once(__DIR__ . '/booking/BookingExceptions.php');
require_once(__DIR__ . '/booking/EventCalendar.php');

require_once(__DIR__ . '/settings/BookingItemModel.php');
require_once(__DIR__ . '/user/BookingUser.php');

get_header();

if (is_user_logged_in()) {

	// Check if the date is valid
	if (checkdate(intval($_GET["monat"]), intval($_GET["tag"]), intval($_GET["jahr"]))) {
		$day = strval(intval($_GET["tag"]));
		$month = strval(intval($_GET["monat"]));
		$year = strval(intval($_GET["jahr"]));

		$booking_date = new DateTime($year . '-' . $month . '-' . $day . ' 23:59:59', wp_timezone());

		try {
			$booking = new NewBookingDialog(get_current_user_id(), $booking_date, $_GET["item"]);
			$booking->user_is_allowed_to_make_booking();
			$booking->show_booking_form();
		} catch (StandardBookingException $e) {
			$e->show_booking_error();
		} catch (DateAlreadyBookedException $e) {
			$e->show_booking_error();
		} catch (MaxNumberOfFutureBookingsReachedException $e) {
			$e->show_booking_error();
		} catch (LogbooksNotCompleteException $e) {
			$e->show_booking_error();
		}
	}

	$booking_item = $_POST['item_tag'];
	$booking_date = $_POST['booking_date'];
	$booking_type = $_POST['booking_type'];
	$booking_comment = htmlspecialchars(stripslashes($_POST['booking_comment']));

	if (isset($booking_item, $booking_date, $booking_type, $booking_comment)) {
		$booking_date = DateTime::createFromFormat("Y-m-d H:i:s", $booking_date . '23:59:59', wp_timezone());

		try {
			$booking = new NewBooking(get_current_user_id(), $booking_date, $booking_item);
			$booking->user_is_allowed_to_make_booking();
			$booking->create_booking($booking_type, $booking_comment);
			$booking->show_booking_confirmation();
		} catch (StandardBookingException $e) {
			$e->show_booking_error();
		} catch (DateAlreadyBookedException $e) {
			$e->show_booking_error();
		} catch (MaxNumberOfFutureBookingsReachedException $e) {
			$e->show_booking_error();
		} catch (LogbooksNotCompleteException $e) {
			$e->show_booking_error();
		}
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
