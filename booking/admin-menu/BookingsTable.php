<?php

/**
 * Class to display the admin menu with the table of all bookings
 *
 * @author Christoph
 */
require_once(__DIR__ . "/../booking/Booking.php");
require_once(__DIR__ . "/BookingRow.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");

class BookingsTable
{

	/**
	 * Autoload method
	 * @return void
	 */
	public function __construct()
	{
		$this->selected_year = $this->get_selected_year();
	}

	private $selected_year;

	/**
	 * Render settings menu
	 * @return void
	 */
	public function show()
	{
		$this->selected_year = $this->get_selected_year();
		echo '<div class="wrap"><h1>Buchungen</h1>';
		$this->show_filters();
		$this->show_bookings_table($this->selected_year);
		echo '</div>';
		$this->filter_script();
	}

	/**
	 * Show selector menu for filtering bookings
	 * @return void
	 */
	private function show_filters()
	{
		echo '<div class="tablenav bottom">';
		$this->year_selector();
		$this->user_selector();
		$this->boat_gear_selector();
		echo '</div>';
	}

	/**
	 * Show table with all bookings of the selected year
	 * @return void
	 */
	private function show_bookings_table(int $year)
	{
		?>
			<table class="widefat">
				<thead>
					<tr>
						<th class="row-title">Datum</th>
						<th>Name</th>
						<th>Boot/Zubehör</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$rows = BookingRow::get_bookings_by_year($year);
					foreach ($rows as $row) {
						$row->show_row();
					}
					?>
				</tbody>
			</table>
		<?php
	}

	private function year_selector()
	{
		?>
			<div class="alignleft actions">
				<label for="year-selector" class="screen-reader-text">Jahr</label>
				<select name="year" id="year-selector">
					<?php
					foreach (BookingRow::get_available_years() as $year) {
						echo '<option value="' . $this->get_year_uri($year) . '"' . (($this->selected_year == $year) ? 'selected' : '') . '>' . $year . '</option>';
					}
					?>
				</select>
			</div>
			<script type="text/javascript">
				var urlmenu = document.getElementById('year-selector');
				urlmenu.onchange = function() {
					window.location = this.options[this.selectedIndex].value;
				};
			</script>
		<?php
	}

	private function user_selector()
	{
		$users = get_users();
		?>
			<div class="alignleft actions">
				<label class="screen-reader-text" for="user-selector">Name</label>
				<select name="user-selector" id="user-selector">
					<option value="all" selected>Alle Benutzer</option>
					<?php
					foreach ($users as $user) {
						echo '<option value="user_id_' . $user->ID . '">' . $user->display_name . '</option>';
					}
					?>
				</select>
			</div>
		<?php
	}

	private function boat_gear_selector()
	{
		$booking_items = BookingItemModel::get_all_booking_items();
		?>
			<div class="alignleft actions">
				<label class="screen-reader-text" for="item-selector">Name</label>
				<select name="item-selector" id="item-selector">
					<option value="all" selected>Alle Boote/Zubehör</option>
					<?php
					foreach ($booking_items as $item) {
						echo '<option value="' . $item->item_tag . '">' . $item->item_name . '</option>';
					}
					?>
				</select>
			</div>
		<?php
	}

	private function get_selected_year()
	{
		$available_years = BookingRow::get_available_years();
		if (isset($_GET["jahr"])) {
			$selected_year = intval($_GET["jahr"]);
			if (in_array($selected_year, $available_years)) {
				return $selected_year;
			}
		}
		$now = current_datetime();
		$current_year = intval($now->format("Y"));
		if (in_array($current_year, $available_years)) {
			return $current_year;
		} else {
			return $available_years[0];
		}
	}

	private function get_year_uri(int $year)
	{
		$query = $_GET;

		$query['jahr'] = $year;
		unset($query["id"]);

		// new link
		$query_result = http_build_query($query);
		return $_SERVER['PHP_SELF'] . '?' . $query_result;
	}

	private function filter_script()
	{
		?>
			<script>
				<?php include(__DIR__ . '/bookings_filter.js'); ?>
			</script>
	<?php
	}
}
