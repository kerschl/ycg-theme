<?php
require_once(__DIR__ . "/../settings/BookingItemModel.php");
require_once(__DIR__ . "/BookingCreator.php");
/**
 * Menu to create the DB and the Pages required for the booking system
 *
 * @author Christoph
 */

class CustomBookingCreatorMenu
{

	/**
	 * Autoload method
	 * @return void
	 */
	public function __construct()
	{
		add_action('admin_menu', array(&$this, 'register_sub_menu'));
	}

	private $error_msg = null;

	/**
	 * Register submenu
	 * @return void
	 */
	public function register_sub_menu()
	{
		add_submenu_page(
			'buchungen', // parent page slug
			'Buchung erstellen',
			'Erstellen',
			'manage_options',
			'buchung_erstellen',
			array(&$this, 'show_menu'),
			2 // menu position
		);
	}

	public function show_menu()
	{
		try {
			if (BookingCreator::all_POST_args_are_set()) {
				$new_booking = BookingCreator::create_from_POST();
				$new_booking->create();
				add_action('admin_notices', array(&$this, 'show_create_notification'));
			}
		} catch (Exception $e) {
			add_action('admin_notices', array(&$this, 'show_error_notification'));
			$this->error_msg = $e->getMessage();
		}

		$this->show_form();

		if ($this->error_msg) {
			do_action('admin_notices', $this->error_msg);
		} else {
			do_action('admin_notices');
		}
	}

	/**
	 * Show error notification
	 */
	public function show_error_notification(string $error_msg)
	{
?>
		<div class="notice notice-error is-dismissible">
			<p>Fehler: <b><?= $error_msg ?></b></p>
		</div>
	<?php
	}

	/**
	 * Show create notification
	 */
	public function show_create_notification(string $error_msg)
	{
	?>
		<div class="notice notice-success is-dismissible">
			<p>Buchung erstellt</p>
		</div>
	<?php
	}

	public function show_form()
	{
	?>
		<div class="wrap">
			<form method="post" action="<?= esc_attr($_SERVER['REQUEST_URI']); ?>">
				<h1>Buchung erstellen</h1>
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="user_id">Name</label></th>
							<td><? $this->user_select_dialog() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="item_tag">Boot/Zubehör</label></th>
							<td><? $this->item_select_dialog() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_date">Buchungsdatum</label></th>
							<td><input type="date" id="booking_date" name="booking_date"></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_type">Buchungsart</label></th>
							<td><? $this->booking_type_selector() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_cost">Preis</label></th>
							<td><input name="booking_cost" type="number" step="0.01" min="0" id="booking_cost" value="0.00"></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_comment">Buchungsanmerkung</label></th>
							<td><textarea id="booking_comment" name="booking_comment" cols="40" rows="2"></textarea><br></td>
						</tr>
					</tbody>
				</table><br>
				<input class="button button-primary" type="submit" value="Buchung erstellen">
			</form>
		</div>
	<?php
	}

	private function user_select_dialog()
	{
		$users = get_users();

	?>
		<select name="user_id" id="user_id_select">
			<option value="">Benutzer auswählen</option>
			<?php

			foreach ($users as $user) {
				echo '<option value="' . $user->ID . '">' . $user->display_name . '</option>';
			}

			echo '</select>';
		}

		private function item_select_dialog()
		{
			$items = BookingItemModel::get_all_booking_items();

			?>
			<select name="item_tag" id="item_tag_select">
				<option value="">Boot/Zubehör wählen</option>
				<?php

				foreach ($items as $item) {
					echo '<option value="' . $item->item_tag . '">' . $item->item_name . '</option>';
				}

				echo '</select>';
			}

			private function booking_type_selector()
			{
				$booking_types = array(
					BOOKING_RATE_YOUTH => 'Jugendliche',
					BOOKING_RATE_ADULT => 'Erwachsene'
				);
				?>
				<select name="booking_type" id="booking_type_select">
			<?php

				foreach ($booking_types as $key => $description) {
					echo '<option value="' . $key . '">' . $description . '</option>';
				}

				echo '</select>';
			}
		}
