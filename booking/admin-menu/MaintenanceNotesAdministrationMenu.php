<?php

/**
 * Menu to create the DB and the Pages required for the booking system
 *
 * @author Christoph
 */
require_once(__DIR__ . "/MaintenanceNotesTable.php");
require_once(__DIR__ . "/MaintenanceNoteMenu.php");
require_once(__DIR__ . "/MaintenanceNoteUpdater.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");

class MaintenanceNotesAdministrationMenu
{

	/**
	 * Autoload method
	 * @return void
	 */
	public function __construct()
	{
		add_action('admin_menu', array(&$this, 'register_menu'));
	}

	private $error_msg = null;

	/**
	 * Register menu
	 * @return void
	 */
	public function register_menu()
	{
		add_submenu_page(
			'buchungen', // parent page slug
			'Wartungsnotizen verwalten',
			'Wartungsnotizen',
			'manage_options',
			'wartungsnotizen',
			array(&$this, 'show_menu'),
			1 // menu position
		);
	}

	/**
	 * Render settings menu
	 * @return void
	 */
	public function show_menu()
	{
		$this->bookings_menu_css();

		if (MaintenanceNoteUpdater::all_POST_args_are_set_for_admin_update()) {
			try {
				$mn = MaintenanceNoteUpdater::get_maintenance_note_by_id(intval($_POST['id']));

				if (isset($_POST["update"])) {
					$mn->load_from_POST();
					$mn->update();
					add_action('admin_notices', array(&$this, 'show_update_notification'));
					$menu = MaintenanceNoteMenu::get_maintenance_note_by_id($mn->ID);
					$menu->show();
				} else if (isset($_POST["delte"])) {
					MaintenanceNote::delete_by_id($mn->ID);
					add_action('admin_notices', array(&$this, 'show_delete_notification'));
					$table = new MaintenanceNotesTable();
					$table->show();
				} else {
					throw new Exception("Unglültige Anfrage");
				}
			} catch (Exception $e) {
				add_action('admin_notices', array(&$this, 'show_error_notification'));
				$this->error_msg = $e->getMessage();
			}
		} else {
			if (isset($_GET["id"])) {
				try {
					$menu = MaintenanceNoteMenu::get_maintenance_note_by_id(intval($_GET["id"]));
					$menu->show();
				} catch (StandardBookingException $e) {
					add_action('admin_notices', array(&$this, 'show_invalid_id_notification'));
					$table = new MaintenanceNotesTable();
					$table->show();
				}
			} else {
				$table = new MaintenanceNotesTable();
				$table->show();
			}
		}

		if ($this->error_msg) {
			do_action('admin_notices', $this->error_msg);
		} else {
			do_action('admin_notices');
		}
	}

	/**
	 * Show error notification
	 */
	public function show_error_notification(string $error_msg)
	{
?>
		<div class="notice notice-error is-dismissible">
			<p>Aktualisierung fehlgeschlagen: <b><?= $error_msg ?></b></p>
		</div>
	<?php
	}

	/**
	 * Show update notification
	 */
	public function show_update_notification()
	{
	?>
		<div class="notice notice-success is-dismissible">
			<p>Wartungsnotiz aktualisiert</p>
		</div>
	<?php
	}

	/**
	 * Show delete notification
	 */
	public function show_delete_notification()
	{
	?>
		<div class="notice notice-success is-dismissible">
			<p>Wartungsnotiz gelöscht</p>
		</div>
	<?php
	}

	/**
	 * Show update notification
	 */
	public function show_invalid_id_notification()
	{
	?>
		<div class="notice notice-error is-dismissible">
			<p>Ungültige Wartungsnotiz-ID</p>
		</div>
	<?php
	}

	/**
	 * Enque CSS for menu
	 * @return void
	 */
	private function bookings_menu_css()
	{
	?>
		<style>
			<?php include(__DIR__ . '/bookings_menu.css'); ?>
		</style>
<?php
	}
}
