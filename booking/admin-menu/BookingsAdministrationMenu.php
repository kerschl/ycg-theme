<?php

/**
 * Menu to create the DB and the Pages required for the booking system
 *
 * @author Christoph
 */
require_once(__DIR__ . "/BookingsTable.php");
require_once(__DIR__ . "/BookingMenu.php");
require_once(__DIR__ . "/BookingUpdater.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");

class BookingsAdministrationMenu
{

	/**
	 * Autoload method
	 * @return void
	 */
	public function __construct()
	{
		add_action('admin_menu', array(&$this, 'register_menu'));
	}

	private $error_msg = null;

	/**
	 * Register menu
	 * @return void
	 */
	public function register_menu()
	{
		add_menu_page(
			'Buchungen verwalten',
			'Buchungen',
			'manage_options',
			'buchungen',
			array(&$this, 'show_menu'),
			'data:image/svg+xml;base64,' . base64_encode(file_get_contents(__DIR__ . "/../../assets/booking/menu_logos/sail-boat.svg")),
			50
		);
	}

	/**
	 * Render settings menu
	 * @return void
	 */
	public function show_menu()
	{
		$this->bookings_menu_css();

		if (BookingUpdater::all_POST_args_are_set_for_admin_update()) {
			try {
				$booking = BookingUpdater::get_booking_by_id(intval($_POST['id']));

				if (count($booking) != 1) {
					throw new Exception("Ungültige Buchungs-ID");
				}
				$booking = $booking[0];

				if (isset($_POST["update"])) {
					$booking->load_from_POST();
					$booking->update();
					add_action('admin_notices', array(&$this, 'show_update_notification'));
					$menu = BookingMenu::get_booking_by_id($booking->id);
					$menu[0]->show();
				} else if (isset($_POST["delte"])) {
					Booking::delete_booking_by_id($booking->id);
					add_action('admin_notices', array(&$this, 'show_delete_notification'));
					$table = new BookingsTable();
					$table->show();
				} else {
					throw new Exception("Unglültige Anfrage");
				}
			} catch (Exception $e) {
				add_action('admin_notices', array(&$this, 'show_error_notification'));
				$this->error_msg = $e->getMessage();
			}
		} else {
			if (isset($_GET["id"])) {
				$menu = BookingMenu::get_booking_by_id(intval($_GET["id"]));
				if (count($menu) != 1) {
					add_action('admin_notices', array(&$this, 'show_invalid_id_notification'));
					$table = new BookingsTable();
					$table->show();
				} else {
					$menu[0]->show();
				}
			} else {
				$table = new BookingsTable();
				$table->show();
			}
		}

		if ($this->error_msg) {
			do_action('admin_notices', $this->error_msg);
		} else {
			do_action('admin_notices');
		}
	}

	/**
	 * Show error notification
	 */
	public function show_error_notification(string $error_msg)
	{
?>
		<div class="notice notice-error is-dismissible">
			<p>Aktualisierung fehlgeschlagen: <b><?= $error_msg ?></b></p>
		</div>
	<?php
	}

	/**
	 * Show update notification
	 */
	public function show_update_notification(string $error_msg)
	{
	?>
		<div class="notice notice-success is-dismissible">
			<p>Buchung aktualisiert</p>
		</div>
	<?php
	}

	/**
	 * Show delete notification
	 */
	public function show_delete_notification(string $error_msg)
	{
	?>
		<div class="notice notice-success is-dismissible">
			<p>Buchung gelöscht</p>
		</div>
	<?php
	}

	/**
	 * Show update notification
	 */
	public function show_invalid_id_notification(string $error_msg)
	{
	?>
		<div class="notice notice-error is-dismissible">
			<p>Ungültige Buchungs-ID</p>
		</div>
	<?php
	}

	/**
	 * Enque CSS for menu
	 * @return void
	 */
	private function bookings_menu_css()
	{
	?>
		<style>
			<?php include(__DIR__ . '/bookings_menu.css'); ?>
		</style>
<?php
	}
}
