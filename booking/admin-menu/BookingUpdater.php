<?php
require_once(__DIR__ . "/../booking/Booking.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");


class BookingUpdater extends Booking
{
	public static function all_POST_args_are_set_for_admin_update()
	{
		return isset(
			$_POST['id'],
			$_POST['user_id'],
			$_POST['item_tag'],
			$_POST['booking_date'],
			$_POST['booking_type'],
			$_POST['booking_cost'],
			$_POST['booking_comment'],
			$_POST['logbook_crew'],
			$_POST['logbook_time_takeover'],
			$_POST['logbook_time_return'],
			$_POST['logbook_defects_on_arrival'],
			$_POST['logbook_weather'],
			$_POST['logbook_damage_report'],
			$_POST['logbook_etc'],
		);
	}

	public function load_from_POST()
	{
		$this->user_id = $this->get_user_id_from_POST();
		$this->boat = $this->get_item_tag_from_POST();
		$this->booking_date = $this->get_booking_date_from_POST();
		$this->booking_type = $this->get_booking_type_from_POST();
		$this->booking_cost = $this->get_booking_cost_from_POST();
		$this->booking_comment = htmlspecialchars(stripslashes($_POST['booking_comment']));
		$this->logbook_crew = htmlspecialchars(stripslashes($_POST['logbook_crew']));
		$this->logbook_time_takeover = htmlspecialchars(stripslashes($_POST['logbook_time_takeover']));
		$this->logbook_time_return = htmlspecialchars(stripslashes($_POST['logbook_time_return']));
		$this->logbook_defects_on_arrival = htmlspecialchars(stripslashes($_POST['logbook_defects_on_arrival']));
		$this->logbook_weather = htmlspecialchars(stripslashes($_POST['logbook_weather']));
		$this->logbook_damage_report = htmlspecialchars(stripslashes($_POST['logbook_damage_report']));
		$this->logbook_etc = htmlspecialchars(stripslashes($_POST['logbook_etc']));
	}

	private function get_user_id_from_POST()
	{
		$users = get_users();
		$user_ids = [];

		foreach ($users as $user) {
			array_push($user_ids, $user->ID);
		}

		$user_id = intval($_POST['user_id']);
		if (!in_array($user_id, $user_ids)) {
			throw new Exception("Der ausgewählte Nutzer existiert nicht");
		}
		return $user_id;
	}

	private function get_item_tag_from_POST()
	{
		if (!in_array($_POST["item_tag"], BookingItemModel::get_all_booking_item_tags())) {
			throw new Exception("Ungültiges Boot oder Zubehör ausgewählt");
		}
		return $_POST["item_tag"];
	}

	private function get_booking_date_from_POST()
	{
		$new_booking_date = new DateTime($_POST["booking_date"], wp_timezone());
		if ($new_booking_date != $this->booking_date) {
			if (count(self::get_booking_by_tag_and_date($_POST["item_tag"], $new_booking_date)) > 0) {
				throw new Exception("Für das neu gewählte Buchungsdatum existiert bereits eine andere Buchung");
			}
		}
		return $new_booking_date;
	}

	private function get_booking_type_from_POST()
	{
		$booking_types = [BOOKING_RATE_ADULT, BOOKING_RATE_YOUTH];
		if (!in_array($_POST['booking_type'], $booking_types)) {
			throw new Exception("Ungültige Buchungsart ausgewählt");
		}
		return $_POST['booking_type'];
	}

	private function get_booking_cost_from_POST()
	{
		$cost = floatval($_POST['booking_cost']);
		if ($cost < 0) {
			throw new Exception("Negative Preise sind nicht zulässig");
		}
		return $cost;
	}
}
