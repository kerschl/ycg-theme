<?php
require_once(__DIR__ . "/../settings/BookingItemModel.php");
require_once(__DIR__ . "/../booking/Booking.php");
require_once(__DIR__ . "/BookingCreator.php");
/**
 * Menu to see stats of the bookings 
 *
 * @author Christoph
 */

class BookingStatsAdminMenu
{

	/**
	 * Autoload method
	 * @return void
	 */
	public function __construct()
	{
		add_action('admin_menu', array(&$this, 'register_sub_menu'));
		$this->selected_year = $this->get_selected_year();
	}

	private $selected_year;

	/**
	 * Register submenu
	 * @return void
	 */
	public function register_sub_menu()
	{
		add_submenu_page(
			'buchungen', // parent page slug
			'Buchungsstatistiken',
			'Statistiken',
			'manage_options',
			'booking_stats',
			array(&$this, 'show_menu'),
			4 // menu position
		);
	}

	private function show_booking_item_stats(BookingItemModel $booking_item)
	{
	?>
		<h3><strong><?= $booking_item->item_name ?></strong></h3>
		<table class="widefat" style="table-layout: fixed">
			<?= $this->show_booking_item_stat_row('Anzahl Buchungen', $this->get_booking_count_by_tag_and_year($booking_item->item_tag, $this->selected_year)) ?>
			<?= $this->show_booking_item_stat_row('Einnahmen', number_format($this->get_earnings_by_tag_and_year($booking_item->item_tag, $this->selected_year),2) . ' €') ?>
			<?= $this->show_booking_item_stat_row('Skipper', $this->get_number_of_skippers_by_tag_and_year($booking_item->item_tag, $this->selected_year)) ?>
		</table>
	<?php
	}

	private function show_booking_item_stat_row(string $description, $value){
	?>
		<tr>
			<td class="row-title"><label for="tablecell"><?php esc_attr_e($description, 'WpAdminStyle'); ?></label></td>
			<td><?php esc_attr_e($value , 'WpAdminStyle' ); ?></td>
		</tr>
	<?php
	}

	private function get_booking_count_by_tag_and_year($item_tag, $year){
		return count(Booking::get_bookings_by_tag_and_year($item_tag, $year));
	}

	private function get_earnings_by_tag_and_year($item_tag, $year){
		$bookings = Booking::get_bookings_by_tag_and_year($item_tag, $year);
		$earnings = 0.0;
		foreach ($bookings as $booking){
			$earnings += $booking->booking_cost;
		}
		return $earnings;
	}

	private function get_number_of_skippers_by_tag_and_year($item_tag, $year){
		$bookings = Booking::get_bookings_by_tag_and_year($item_tag, $year);
		$skippers = [];
		foreach ($bookings as $booking){
			array_push($skippers, $booking->user_id);
		}
		return count(array_unique($skippers));
	}

	public function show_menu(){
	?>
		<div class="wrap">
			<h1>Statistiken</h1>
			<?php
			$this->year_selector();
			foreach(BookingItemModel::get_all_booking_items() as $booking_item){
				$this->show_booking_item_stats($booking_item);
			}
			?>
		</div>
	<?php
	}

	private function year_selector()
	{
		?>
			<div>
				<label for="year-selector" class="screen-reader-text">Jahr</label>
				<select name="year" id="year-selector">
					<?php
					foreach (Booking::get_available_years() as $year) {
						echo '<option value="' . $this->get_year_uri($year) . '"' . (($this->selected_year == $year) ? 'selected' : '') . '>' . $year . '</option>';
					}
					?>
				</select>
			</div>
			<script type="text/javascript">
				var urlmenu = document.getElementById('year-selector');
				urlmenu.onchange = function() {window.location = this.options[this.selectedIndex].value;};
			</script>
		<?php
	}

	private function get_year_uri(int $year)
	{
		$query = $_GET;
		$query['jahr'] = $year;
		$query_result = http_build_query($query);

		return $_SERVER['PHP_SELF'] . '?' . $query_result;
	}

	private function get_selected_year()
	{
		$available_years = Booking::get_available_years();
		if (isset($_GET["jahr"])) {
			$selected_year = intval($_GET["jahr"]);
			if (in_array($selected_year, $available_years)) {
				return $selected_year;
			}
		}
		$now = current_datetime();
		$current_year = intval($now->format("Y"));
		if (in_array($current_year, $available_years)){
			return $current_year;
		} else {
			return $available_years[0];
		}
	}

}
