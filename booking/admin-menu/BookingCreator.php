<?php
require_once(__DIR__ . "/../booking/Booking.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");


class BookingCreator extends Booking
{
	public static function all_POST_args_are_set()
	{
		return isset(
			$_POST['user_id'],
			$_POST['item_tag'],
			$_POST['booking_date'],
			$_POST['booking_type'],
			$_POST['booking_cost'],
			$_POST['booking_comment']
		);
	}

	static function create_from_POST()
	{
		$new_booking = new static;
		$new_booking->user_id = self::get_user_id_from_POST();
		$new_booking->boat = self::get_item_tag_from_POST();
		$new_booking->booking_date = self::get_booking_date_from_POST();
		$new_booking->booking_type = self::get_booking_type_from_POST();
		$new_booking->booking_cost = self::get_booking_cost_from_POST();
		$new_booking->booking_comment = sanitize_textarea_field($_POST['booking_comment']);

		return $new_booking;
	}

	public function create()
	{
		self::create_booking(
			$this->user_id,
			$this->boat,
			$this->booking_date,
			"00:00:00",
			"23:59:59",
			$this->booking_type,
			$this->booking_cost,
			$this->booking_comment
		);
	}

	static function get_user_id_from_POST()
	{
		$users = get_users();
		$user_ids = [];

		foreach ($users as $user) {
			array_push($user_ids, $user->ID);
		}

		$user_id = intval($_POST['user_id']);
		if (!in_array($user_id, $user_ids)) {
			throw new Exception("Der ausgewählte Nutzer existiert nicht");
		}
		return $user_id;
	}

	static function get_item_tag_from_POST()
	{
		if (!in_array($_POST["item_tag"], BookingItemModel::get_all_booking_item_tags())) {
			throw new Exception("Ungültiges Boot oder Zubehör ausgewählt");
		}
		return $_POST["item_tag"];
	}

	static function get_booking_date_from_POST()
	{
		$new_booking_date = new DateTime($_POST["booking_date"], wp_timezone());
		if (count(self::get_booking_by_tag_and_date($_POST["item_tag"], $new_booking_date)) > 0) {
			throw new Exception("Für das gewählte Buchungsdatum existiert bereits eine andere Buchung");
		}
		return $new_booking_date;
	}

	static function get_booking_type_from_POST()
	{
		$booking_types = [BOOKING_RATE_ADULT, BOOKING_RATE_YOUTH];
		if (!in_array($_POST['booking_type'], $booking_types)) {
			throw new Exception("Ungültige Buchungsart ausgewählt");
		}
		return $_POST['booking_type'];
	}

	static function get_booking_cost_from_POST()
	{
		$cost = floatval($_POST['booking_cost']);
		if ($cost < 0) {
			throw new Exception("Negative Preise sind nicht zulässig");
		}
		return $cost;
	}
}
