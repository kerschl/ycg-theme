<?php

/**
 * Class to display the admin menu with the table of all bookings
 *
 * @author Christoph
 */
require_once(__DIR__ . "/../booking/Booking.php");
require_once(__DIR__ . "/BookingRow.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");

class BookingMenu extends Booking
{
	public function show()
	{
?>
		<div class="wrap">
			<h1>Buchung bearbeiten</h1>
			<form method="post" action="<?= esc_attr($_SERVER['REQUEST_URI']); ?>">
				<input type="hidden" value="<?= $this->id ?>" name="id">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="user_id">Name</label></th>
							<td><? $this->user_select_dialog() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="item_tag">Boot/Zubehör</label></th>
							<td><? $this->item_select_dialog() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_date">Buchungsdatum</label></th>
							<td><input type="date" id="booking_date" name="booking_date" value="<?= $this->booking_date->format("Y-m-d") ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_type">Buchungsart</label></th>
							<td><? $this->booking_type_selector() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_cost">Preis</label></th>
							<td><input name="booking_cost" type="number" step="0.01" min="0" id="booking_cost" value="<?= $this->booking_cost ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_comment">Buchungsanmerkung</label></th>
							<td><textarea id="booking_comment" name="booking_comment" cols="40" rows="2"><?= $this->booking_comment ?></textarea><br></td>
						</tr>
						<tr>
							<th scope="row"><label for="logbook_crew">Mitseglerinnen und -segler</label></th>
							<td><textarea id="logbook_crew" name="logbook_crew" cols="40" rows="5"><?= $this->logbook_crew ?></textarea><br></td>
						</tr>
						<tr>
							<th scope="row"><label for="logbook_time_takeover">Uhrzeit der Übernahme</label></th>
							<td><textarea id="logbook_time_takeover" name="logbook_time_takeover" cols="40" rows="2"><?= $this->logbook_time_takeover ?></textarea><br></td>
						</tr>
						<tr>
							<th scope="row"><label for="logbook_time_return">Uhrzeit der Rückkehr</label></th>
							<td><textarea id="logbook_time_return" name="logbook_time_return" cols="40" rows="2"><?= $this->logbook_time_return ?></textarea><br></td>
						</tr>
						<tr>
							<th scope="row"><label for="logbook_defects_on_arrival">Mängel bei Übernahme</label></th>
							<td><textarea id="logbook_defects_on_arrival" name="logbook_defects_on_arrival" cols="40" rows="5"><?= $this->logbook_defects_on_arrival ?></textarea><br></td>
						</tr>
						<tr>
							<th scope="row"><label for="logbook_weather">Wetter</label></th>
							<td><textarea id="logbook_weather" name="logbook_weather" cols="40" rows="5"><?= $this->logbook_weather ?></textarea><br></td>
						</tr>
						<tr>
							<th scope="row"><label for="logbook_damage_report">Entstandene Schäden</label></th>
							<td><textarea id="logbook_damage_report" name="logbook_damage_report" cols="40" rows="5"><?= $this->logbook_damage_report ?></textarea><br></td>
						</tr>
						<tr>
							<th scope="row"><label for="logbook_etc">Sonstiges</label></th>
							<td><textarea id="logbook_etc" name="logbook_etc" cols="40" rows="5"><?= $this->logbook_etc ?></textarea><br></td>
						</tr>
					</tbody>
				</table><br>
				<button class="btn button-primary" type="submit" name="update">Aktualisieren</button>
				<button class="btn button-primary" type="submit" name="delte" style="background: #DC3232; border: 1px solid #DC3232;" onclick="return confirm('Bist du sicher, dass du die Buchung löschen möchtest? Dies kann nicht mehr rückgängig gemacht werden.')">Buchung Löschen</button>
			</form>
		</div>
<?php
	}

	private function booking_type_selector()
	{
		$booking_types = array(
			BOOKING_RATE_YOUTH => 'Jugendliche',
			BOOKING_RATE_ADULT => 'Erwachsene'
		);
		echo '<select name="booking_type" id="booking_type_select">';
		foreach ($booking_types as $key => $description) {
			$selected = ($this->booking_type == $key) ? "selected" : "";
			echo '<option value="' . $key . '"' . $selected . '>' . $description . '</option>';
		}
		echo '</select>';
	}

	private function user_select_dialog()
	{
		$users = get_users();
		echo '<select name="user_id" id="user_id_select">';
		foreach ($users as $user) {
			$selected = ($this->user_id == $user->ID) ? "selected" : "";
			echo '<option value="' . $user->ID . '"' . $selected . '>' . $user->display_name . '</option>';
		}
		echo '</select>';
	}

	private function item_select_dialog()
	{
		$items = BookingItemModel::get_all_booking_items();
		echo '<select name="item_tag" id="item_tag_select">';
		foreach ($items as $item) {
			$selected = ($this->boat == $item->item_tag) ? "selected" : "";
			echo '<option value="' . $item->item_tag . '"' . $selected . '>' . $item->item_name . '</option>';
		}
		echo '</select>';
	}
}
