class bookings_filter {
	constructor() {
		this.userSelector = document.getElementById("user-selector");
		this.itemSelector = document.getElementById("item-selector");
		this.bookings = document.getElementsByClassName("booking");
		this.events();
	}

	events() {
		this.userSelector.addEventListener("change", () =>
			this.filterBookings()
		);
		this.itemSelector.addEventListener("change", () =>
			this.filterBookings()
		);
	}

	filterBookings() {
		if (
			this.userSelector.value == "all" &&
			this.itemSelector.value == "all"
		) {
			this.show_all_bookings();
		} else if (this.userSelector.value == "all") {
			for (var i = 0; i < this.bookings.length; ++i) {
				if (
					this.bookings[i].classList.contains(this.itemSelector.value)
				) {
					this.bookings[i].style.display = "";
				} else {
					this.bookings[i].style.display = "none";
				}
			}
		} else if (this.itemSelector.value == "all") {
			for (var i = 0; i < this.bookings.length; ++i) {
				if (
					this.bookings[i].classList.contains(this.userSelector.value)
				) {
					this.bookings[i].style.display = "";
				} else {
					this.bookings[i].style.display = "none";
				}
			}
		} else {
			for (var i = 0; i < this.bookings.length; ++i) {
				if (
					this.bookings[i].classList.contains(
						this.userSelector.value
					) &&
					this.bookings[i].classList.contains(this.itemSelector.value)
				) {
					this.bookings[i].style.display = "";
				} else {
					this.bookings[i].style.display = "none";
				}
			}
		}
	}

	show_all_bookings() {
		for (var i = 0; i < this.bookings.length; ++i) {
			this.bookings[i].style.display = "";
		}
	}
}

new bookings_filter();
