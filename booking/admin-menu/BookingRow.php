<?php

/**
 * @property	integer	$ID
 * @property	integer	$user_id
 * @property	string $user_display_name
 * @property	string $item_tag
 * @property	string	$item_name
 * @property	string	$item_type
 * @property	DateTime $booking_date
 * @property	DateTime	$logbook_created
 * @property	string	$logbook_crew
 * @property	string	$logbook_time_takeover
 * @property	string	$logbook_time_return
 * @property	string	$logbook_defects_on_arrival
 * @property	string	$logbook_weather
 * @property	string	$logbook_damage_report
 * @property	string	$logbook_etc
 */
class BookingRow
{
	private function __construct($row)
	{
		$this->ID = intval($row->id);
		$this->user_id = intval($row->user_id);
		$this->user_display_name = (get_user_by('ID', $this->user_id))->display_name;
		$this->item_tag = $row->item_tag;
		$this->item_name = $row->item_name;
		$this->item_type = ($row->item_type == BOOKING_ITEM_TYPE_BOAT) ? BOOKING_ITEM_TYPE_BOAT : BOOKING_ITEM_TYPE_GEAR;
		$this->booking_date = new DateTime($row->booking_date, wp_timezone());
	}

	public static function get_bookings_by_year(int $year)
	{
		$query =
			"SELECT " .
			"`" . BOOKINGS_TABLE . "`.id, " .
			"`" . BOOKINGS_TABLE . "`.user_id, " .
			"`" . BOOKING_ITEMS_TABLE . "`.item_tag, " .
			"`" . BOOKING_ITEMS_TABLE . "`.item_name, " .
			"`" . BOOKING_ITEMS_TABLE . "`.item_type, " .
			"`" . BOOKINGS_TABLE . "`.booking_date " .
			" FROM `" . BOOKINGS_TABLE . "` INNER JOIN `" . BOOKING_ITEMS_TABLE . "` " .
			" ON `" . BOOKINGS_TABLE . "`.boat=`" . BOOKING_ITEMS_TABLE . "`.item_tag " .
			"WHERE YEAR(`" . BOOKINGS_TABLE . "`.booking_date) = " . $year .
			" ORDER BY `" . BOOKINGS_TABLE . "`.booking_date DESC";

		global $wpdb;
		$bookings = $wpdb->get_results($query);

		$entries = [];

		foreach ($bookings as $booking) {
			array_push($entries, new static($booking));
		}
		return $entries;
	}

	public static function get_available_years()
	{
		global $wpdb;
		$years = $wpdb->get_results(
			"SELECT DISTINCT YEAR(`booking_date`) AS year FROM `" . BOOKINGS_TABLE . "` ORDER BY YEAR(`booking_date`) DESC"
		);

		$year_list = [];
		foreach ($years as $year) {
			array_push($year_list, intval($year->year));
		}
		return $year_list;
	}

	private function get_edit_uri(int $id)
	{
		$query = $_GET;

		unset($query["jahr"]);
		$query['id'] = $id;

		$query_result = http_build_query($query);
		return $_SERVER['PHP_SELF'] . '?' . $query_result;
	}

	public function show_row()
	{
?>
		<tr class="booking <?= 'user_id_' . $this->user_id ?> <?= $this->item_tag ?>" onclick="window.location='<?= $this->get_edit_uri($this->ID) ?>';">
			<td class="row-title"><label for="tablecell"><?php esc_attr_e($this->booking_date->format("d.m.Y"), 'WpAdminStyle'); ?> </label></td>
			<td><?php esc_attr_e($this->user_display_name, 'WpAdminStyle'); ?> </td>
			<td><?php esc_attr_e($this->item_name, 'WpAdminStyle'); ?></td>
		</tr>
<?php
	}
}
