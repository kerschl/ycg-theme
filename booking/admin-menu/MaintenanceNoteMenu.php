<?php

/**
 * Class to display the admin menu with the table of all bookings
 *
 * @author Christoph
 */
require_once(__DIR__ . "/../logbook/MaintenanceNote.php");
require_once(__DIR__ . "/MaintenanceNoteRow.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");

class MaintenanceNoteMenu extends MaintenanceNote
{
	public function show()
	{
?>
		<div class="wrap">
			<h1>Wartungsnotiz bearbeiten</h1>
			<form method="post" action="<?= esc_attr($_SERVER['REQUEST_URI']); ?>">
				<input type="hidden" value="<?= $this->ID ?>" name="id">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="user_id">Autor</label></th>
							<td><? $this->user_select_dialog() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="item_tag">Boot/Zubehör</label></th>
							<td><? $this->item_select_dialog() ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="created">Erstellt</label></th>
							<td><input type="datetime-local" id="created" name="created" value="<?= $this->created->format("Y-m-d H:i:s") ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="note">Notiz</label></th>
							<td><textarea id="note" name="note" cols="40" rows="5"><?= $this->note ?></textarea></td>
						</tr>
					</tbody>
				</table><br>
				<button class="btn button-primary" type="submit" name="update">Aktualisieren</button>
				<button class="btn button-primary" type="submit" name="delte" style="background: #DC3232; border: 1px solid #DC3232;" onclick="return confirm('Bist du sicher, dass du die Wartungsnotiz löschen möchtest? Dies kann nicht mehr rückgängig gemacht werden.')">Notiz Löschen</button>
			</form>
		</div>
<?php
	}

	private function user_select_dialog()
	{
		$users = get_users();
		echo '<select name="user_id" id="user_id_select">';
		foreach ($users as $user) {
			$selected = ($this->user_id == $user->ID) ? "selected" : "";
			echo '<option value="' . $user->ID . '"' . $selected . '>' . $user->display_name . '</option>';
		}
		echo '</select>';
	}

	private function item_select_dialog()
	{
		$items = BookingItemModel::get_all_booking_items();
		echo '<select name="item_tag" id="item_tag_select">';
		foreach ($items as $item) {
			$selected = ($this->item_tag == $item->item_tag) ? "selected" : "";
			echo '<option value="' . $item->item_tag . '"' . $selected . '>' . $item->item_name . '</option>';
		}
		echo '</select>';
	}
}
