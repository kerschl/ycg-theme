<?php
require_once(__DIR__ . "/../logbook/MaintenanceNote.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");


class MaintenanceNoteUpdater extends MaintenanceNote
{
	public static function all_POST_args_are_set_for_admin_update()
	{
		return isset(
			$_POST['id'],
			$_POST['user_id'],
			$_POST['item_tag'],
			$_POST['created'],
			$_POST['note']
		);
	}

	public function load_from_POST()
	{
		$this->user_id = $this->get_user_id_from_POST();
		$this->item_tag = $this->get_item_tag_from_POST();
		$this->created = $this->get_date_from_POST("created");
		$this->updated = $this->get_date_from_POST("updated");
		$this->note = htmlspecialchars(stripslashes($_POST["note"]));
	}

	private function get_user_id_from_POST()
	{
		$users = get_users();
		$user_ids = [];

		foreach ($users as $user) {
			array_push($user_ids, $user->ID);
		}

		$user_id = intval($_POST['user_id']);
		if (!in_array($user_id, $user_ids)) {
			throw new Exception("Der ausgewählte Nutzer existiert nicht");
		}
		return $user_id;
	}

	private function get_item_tag_from_POST()
	{
		if (!in_array($_POST["item_tag"], BookingItemModel::get_all_booking_item_tags())) {
			throw new Exception("Ungültiges Boot oder Zubehör ausgewählt");
		}
		return $_POST["item_tag"];
	}

	private function get_date_from_POST($post_key)
	{
		$new_booking_date = new DateTime($_POST[$post_key], wp_timezone());
		return $new_booking_date;
	}
}
