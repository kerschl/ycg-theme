<?php
/* 
Template Name: Logbuch
*/
require_once(__DIR__ . "/logbook/Logbook.php");
require_once(__DIR__ . "/booking/BookingExceptions.php");

get_header();

if (is_user_logged_in()) {
	try {
		if (isset($_GET["item"])) {
			if (isset($_GET["jahr"])) {
				$year = intval($_GET["jahr"]);
				if ($year == 0) {
					throw new StandardBookingException("Ungültige Anfrage");
				}
			} else {
				$now = current_datetime();
				$year = intval($now->format("Y"));
			}
			$lb = new Logbook($_GET["item"], $year);
			$lb->show();
		} else {
			throw new StandardBookingException("Ungültige Anfrage");
		}
	} catch (StandardBookingException $e) {
		$e->show_booking_error();
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
