<?php
/* 
Template Name: Benutzerkonto
*/
require_once(__DIR__ . "/user/UserPhoneForm.php");
require_once(__DIR__ . "/booking/BookingExceptions.php");
require_once(__DIR__ . "/user/UserPasswordForm.php");

get_header();

if (is_user_logged_in()) {
	$phone_number = $_POST["phone_number"];
	$new_password = $_POST["new_password"];

	if (isset($phone_number)) {
		try {
			$phone_form = new UserPhoneForm(get_current_user_id());
			if (!$phone_form->phone_number_is_valid($phone_number)) {
				throw new StandardBookingException("Ungültige Telefonnummer. Telefonnummer bitte im Format +49 170 123456 angeben");
			}
			$phone_form->set_phone_number($phone_number);
			$phone_form->show_update_confirmation();
		} catch (StandardBookingException $e) {
			$e->show_booking_error();
		}
	} elseif (isset($new_password)) {
		try {
			$password_form = new UserPasswordForm(get_current_user_id());
			if (!$password_form->password_is_valid($new_password)) {
				throw new StandardBookingException("Ungültiges Passwort. Das Passwort muss mindestens 8 Zeichen, einen Großbuchstaben, einen Kleinbuchstaben, eine Ziffer und ein Sonderzeichen enthalten.");
			}
			$password_form->set_password($new_password);
			$password_form->show_update_confirmation();
		} catch (StandardBookingException $e) {
			$e->show_booking_error();
		}
	} else {
		echo '<div id="booking">' .
			'<h1>Benutzerkontoverwaltung</h1>';

		$phone_form = new UserPhoneForm(get_current_user_id());
		$phone_form->show_input_form();

		$password_form = new UserPasswordForm(get_current_user_id());
		$password_form->show_input_form();

		echo '</div>';
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
