<?php
class Billing
{
	public function __construct($user_id, $year)
	{
		$this->user_id = $user_id;
		$this->year = $year;
	}

	/********************* PROPERTY ********************/
	private $user_id = null;
	private $year = null;
	/********************* PUBLIC **********************/

	public static function get_users_with_booking_in_year($year)
	{
		global $wpdb;

		return $wpdb->get_results("SELECT DISTINCT `user_id` FROM `" . BOOKINGS_TABLE . "` WHERE `booking_date` >= '" . $year . "-01-01' AND `booking_date` <= '" . $year . "-12-31'");
	}

	private function get_bookings_by_user_id($user_id)
	{
		global $wpdb;
		return $wpdb->get_results("SELECT * FROM `" . BOOKINGS_TABLE . "` WHERE `user_id` = ' " . $user_id . "'  AND `booking_date` > '" . $this->year . "-01-01' ORDER BY `booking_date`");
	}

	private function show_single_booking($booking)
	{
		include dirname(__DIR__, 1) . '/booking-config.php';

		$booking_date = new DateTime($booking->booking_date, wp_timezone());
		$booking_created_date = new DateTime($booking->booking_created, wp_timezone());

		echo '<div class="mb-3">' .
			'<div class="d-flex text-decoration-none border rounded overflow-hidden">' .
			'<div class="p-3 me-auto text-black d-flex align-items-start flex-column">' .
			'<h4 class="mb-2">' . $booking_date->format("d.m.Y") . '</h4>' .
			'<p class="mb-0">Boot: ' . $booking->boat . '</p>' .
			'<p class="mb-0">Tarif: ' . $booking_rate_translation[$booking->booking_type] . '</p>' .
			'<p class="mb-0">Gebühr: ' . $booking->booking_cost . '€</p>' .
			'<p class="mb-0">Anmerkung: ' . $booking->booking_comment . '</p>' .
			'</div>' .
			'</div>' .
			'</div>';
	}

	public function show_bookings()
	{
		$bookings = $this->get_bookings_by_user_id($this->user_id);

		$this->show_billing_header();

		if (count($bookings) > 0) {
			foreach ($bookings as $booking) {
				$this->show_single_booking($booking);
			}
		} else {
			echo '<p>Keine Buchungen für diesen Benutzer vorhanden</p>';
		}

		$this->show_billing_footer();
	}

	private function show_billing_header()
	{
		$user_name = (get_user_by('ID', $this->user_id))->display_name;

		echo '<h4>Buchungen für: ' . $user_name . '</h4>';
	}

	private function show_billing_footer()
	{
		global $wpdb;
		$total_cost = $wpdb->get_results("SELECT SUM(`booking_cost`) AS `cost` FROM `" . BOOKINGS_TABLE . "` WHERE `booking_date` >= '" . $this->year . "-01-01' AND `booking_date` <= '" . $this->year . "-12-31' AND `user_id` = '" . $this->user_id . "'");
		echo '<h5>Gesamtgebühr für ' . $this->year . ': ' . $total_cost[0]->cost . '€</h5><hr>';
	}
}
