<?php
/* 
Template Name: Buchungskalender
*/

require_once(__DIR__ . "/calendar/BookingCalendar.php");
require_once(__DIR__ . "/settings/BookingItemModel.php");
require_once(__DIR__ . "/booking/BookingExceptions.php");

get_header();

if (is_user_logged_in()) {
	$booking_item_tag = $_GET["item"];

	try {
		// Check if selected boat is valid option
		if (in_array($booking_item_tag, BookingItemModel::get_all_booking_item_tags())) {

			$booking_item = new BookingItemModel();
			$booking_item->load_by_item_tag($booking_item_tag);

			$calendar = new BookingCalendar($booking_item);

			echo '<div id="booking">' .
				'<h1>Buchungskalender ' . $booking_item->item_name . '</h1>';

			$calendar->show();

			echo '</div>';
		} else {
			throw new StandardBookingException("Ungültige Auswahl an Boot oder Zubehör");
		}
	} catch (StandardBookingException $e) {
		$e->show_booking_error();
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
