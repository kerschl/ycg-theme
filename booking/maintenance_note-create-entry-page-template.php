<?php
/* 
Template Name: Wartungsnotiz
*/
require_once(__DIR__ . "/logbook/MaintenanceNote.php");
require_once(__DIR__ . "/logbook/MaintenanceNoteDialog.php");
require_once(__DIR__ . "/settings/BookingItemModel.php");
require_once(__DIR__ . "/booking/BookingExceptions.php");

get_header();

if (is_user_logged_in()) {

	try {
		if (isset($_GET["id"])) {
			$mn = MaintenanceNoteDialog::get_maintenance_note_by_id(intval($_GET["id"]));

			if ($mn->user_id == get_current_user_id()) {
				$mn->show_form(true, true);
			} else {
				throw new StandardBookingException("Diese Wartungsnotiz ist nicht von dir", "Wartungsnotiz kann nicht angezeigt werden");
			}
		} elseif (isset($_GET["item"])) {
			$item = $_GET["item"];

			// Check if item exists
			$booking_items = BookingItemModel::get_all_booking_item_tags();

			if (in_array($item, $booking_items)) {
				$booking_item_model = new BookingItemModel();
				$booking_item_model->load_by_item_tag($item);

				$mn = new MaintenanceNoteDialog();
				$mn->user_id = get_current_user_id();
				$mn->user_display_name = (wp_get_current_user())->display_name;
				$mn->item_tag = $item;
				$mn->item_name = $booking_item_model->item_name;
				$mn->show_form();
			} else {
				throw new StandardBookingException("Ungültige Auswahl an Boot oder Zubehör", "Wartungsnotiz kann nicht angelegt werden");
			}
		} elseif (isset($_POST["id"], $_POST["note"])) {
			$mn = MaintenanceNoteDialog::get_maintenance_note_by_id(intval($_POST["id"]));

			$new_note = htmlspecialchars(stripslashes($_POST["note"]));

			if ($mn->user_id == get_current_user_id()) {

				if (isset($_POST["delete"])) {
					MaintenanceNote::delete_by_id($mn->ID);
					$mn->show_confirmation("Wartungsnotiz gelöscht", "Die Wartungsnotiz wurde aus dem Logbuch entfernt");
				} elseif (strlen($new_note) > 1) {
					$mn->note = $new_note;
					$mn->update();
					$mn->show_confirmation("Wartungsnotiz aktualisiert", "Die Wartungsnotiz wurde erfolgreich aktualisiert");
				} else {
					throw new StandardBookingException("Die Wartungsnotiz muss einen Text beinhalten", "Wartungsnotiz konnte nicht erstellt werden");
				}
			} else {
				throw new StandardBookingException("Diese Wartungsnotiz ist nicht von dir", "Wartungsnotiz kann nicht angezeigt werden");
			}
		} elseif (isset($_POST["item_tag"], $_POST["note"])) {
			// check if item tag exits
			$booking_items = BookingItemModel::get_all_booking_item_tags();

			$item = $_POST["item_tag"];
			$note = htmlspecialchars(stripslashes($_POST["note"]));

			if (in_array($item, $booking_items)) {
				if (!empty($note)) {
					$booking_item_model = new BookingItemModel();
					$booking_item_model->load_by_item_tag($item);

					$mn = new MaintenanceNoteDialog();
					$mn->user_id = get_current_user_id();
					$mn->item_tag = $item;
					$mn->note = $note;
					$mn->create();
					$mn->show_confirmation("Wartungsnotiz angelegt", "Die Wartungsnotiz ist ab jetzt im Logbuch sichtbar");
				} else {
					throw new StandardBookingException("Die Wartungsnotiz muss einen Text beinhalten", "Wartungsnotiz konnte nicht erstellt werden");
				}
			} else {
				throw new StandardBookingException("Ungültige Auswahl an Boot oder Zubehör", "Wartungsnotiz konnte nicht erstellt werden");
			}
		} else {
			throw new StandardBookingException("Ungültige Anfrage", "Wartungsnotiz konnte nicht erstellt werden");
		}
	} catch (StandardBookingException $e) {
		$e->show_booking_error();
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
