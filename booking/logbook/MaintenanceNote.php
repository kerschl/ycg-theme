<?php
require_once(__DIR__ . "/../booking/BookingExceptions.php");
/**
 * @property	integer	$ID
 * @property	integer	$user_id
 * @property	string $user_display_name
 * @property	string	$item_tag
 * @property	string	$item_name
 * @property	DateTime	$created
 * @property	DateTime	$updated
 * @property	string	$note
 */
class MaintenanceNote
{
	public function __construct()
	{
		$this->ID = null;
		$this->user_id = null;
		$this->user_display_name = null;
		$this->item_tag = null;
		$this->item_name = null;
		$this->created = null;
		$this->updated = null;
		$this->note = null;
	}

	public function update()
	{
		$now = current_datetime();

		global $wpdb;
		$wpdb->update(
			MAINTENANCE_NOTES_TABLE,
			array(
				"user_id" => $this->user_id,
				"item_tag" => $this->item_tag,
				"note" => $this->note,
				"created" => $this->created->format("Y-m-d H:i:s"),
				"updated" => $now->format("Y-m-d H:i:s")
			),
			array('ID' => $this->ID)
		);
	}

	public function create()
	{
		$now = current_datetime();

		global $wpdb;
		$wpdb->insert(MAINTENANCE_NOTES_TABLE, array(
			"user_id" => $this->user_id,
			"item_tag" => $this->item_tag,
			"created" => $now->format("Y-m-d H:i:s"),
			"updated" => $now->format("Y-m-d H:i:s"),
			"note" => $this->note
		));
	}

	public static function get_maintenance_note_by_id(int $ID)
	{
		$query =
			"SELECT " .
			MAINTENANCE_NOTES_TABLE . ".id, " .
			MAINTENANCE_NOTES_TABLE . ".user_id, " .
			MAINTENANCE_NOTES_TABLE . ".item_tag, " .
			BOOKING_ITEMS_TABLE . ".item_name, " .
			MAINTENANCE_NOTES_TABLE . ".created, " .
			MAINTENANCE_NOTES_TABLE . ".updated, " .
			MAINTENANCE_NOTES_TABLE . ".note " .
			" FROM " . MAINTENANCE_NOTES_TABLE . " INNER JOIN " . BOOKING_ITEMS_TABLE . " " .
			" ON " . MAINTENANCE_NOTES_TABLE . ".item_tag=" . BOOKING_ITEMS_TABLE . ".item_tag " .
			"WHERE " . MAINTENANCE_NOTES_TABLE . ".id = " . $ID;

		global $wpdb;
		$logbook_entry = $wpdb->get_results($query);

		if (count($logbook_entry) == 1) {
			$entry = $logbook_entry[0];

			$mn = new static();
			$mn->ID = intval($entry->id);
			$mn->user_id = intval($entry->user_id);
			$mn->user_display_name = (get_user_by('ID', $mn->user_id))->display_name;
			$mn->item_tag = $entry->item_tag;
			$mn->item_name = $entry->item_name;
			$mn->created = new DateTime($entry->created, wp_timezone());
			$mn->updated = new DateTime($entry->updated, wp_timezone());
			$mn->note = $entry->note;

			return $mn;
		} else {
			throw new StandardBookingException("Ungültige Wartungsnotiz ausgewählt");
		}
	}

	public static function get_by_item_tag(string $item_tag, int $year)
	{
		$now = current_datetime();

		$query =
			"SELECT " .
			MAINTENANCE_NOTES_TABLE . ".id, " .
			MAINTENANCE_NOTES_TABLE . ".user_id, " .
			MAINTENANCE_NOTES_TABLE . ".item_tag, " .
			BOOKING_ITEMS_TABLE . ".item_name, " .
			MAINTENANCE_NOTES_TABLE . ".created, " .
			MAINTENANCE_NOTES_TABLE . ".updated, " .
			MAINTENANCE_NOTES_TABLE . ".note " .
			" FROM " . MAINTENANCE_NOTES_TABLE . " INNER JOIN " . BOOKING_ITEMS_TABLE . " " .
			" ON " . MAINTENANCE_NOTES_TABLE . ".item_tag=" . BOOKING_ITEMS_TABLE . ".item_tag " .
			"WHERE YEAR(" . MAINTENANCE_NOTES_TABLE . ".created) = " . $year . " AND " .
			MAINTENANCE_NOTES_TABLE . ".item_tag = '" . $item_tag . "' AND " .
			MAINTENANCE_NOTES_TABLE . ".created <= '" . $now->format("Y-m-d H:i:s") . "' " .
			"ORDER BY " . MAINTENANCE_NOTES_TABLE . ".created DESC";

		global $wpdb;
		$mn_db_entires = $wpdb->get_results($query);

		$mns = [];

		if (count($mn_db_entires) > 0) {
			foreach ($mn_db_entires as $entry) {
				$mn = new static();
				$mn->ID = intval($entry->id);
				$mn->user_id = intval($entry->user_id);
				$mn->user_display_name = (get_user_by('ID', $mn->user_id))->display_name;
				$mn->item_tag = $entry->item_tag;
				$mn->item_name = $entry->item_name;
				$mn->created = new DateTime($entry->created, wp_timezone());
				$mn->updated = new DateTime($entry->updated, wp_timezone());
				$mn->note = $entry->note;

				array_push($mns, $mn);
			}
		}

		return $mns;
	}

	public static function delete_by_id(int $id)
	{
		global $wpdb;
		$wpdb->delete(MAINTENANCE_NOTES_TABLE, array('id' => $id));
	}
}
