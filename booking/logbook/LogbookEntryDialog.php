<?php
require_once(__DIR__ . "/LogbookEntry.php");
require_once(__DIR__ . "/../booking/BookingExceptions.php");

class LogbookEntryDialog extends LogbookEntry
{
	public function check_if_user_is_allowed_to_edit_entry(int $current_user_id)
	{
		if ($current_user_id != $this->user_id) {
			throw new StandardBookingException("Diese Buchung ist nicht von dir", "Eintrag kann nicht angezeigt werden");
		}
	}

	public function check_if_all_post_args_are_set()
	{
		$logbook_crew = $_POST['logbook_crew'];
		$logbook_time_takeover = $_POST['logbook_time_takeover'];
		$logbook_time_return = $_POST['logbook_time_return'];
		$logbook_defects_on_arrival = $_POST['logbook_defects_on_arrival'];
		$logbook_weather = $_POST['logbook_weather'];
		$logbook_damage_report = $_POST['logbook_damage_report'];
		$logbook_etc = $_POST['logbook_etc'];

		if ($this->item_type == BOOKING_ITEM_TYPE_BOAT) {
			if (
				!empty($logbook_crew) and
				!empty($logbook_time_takeover) and
				!empty($logbook_time_return) and
				!empty($logbook_defects_on_arrival) and
				!empty($logbook_weather) and
				!empty($logbook_damage_report) and
				isset($logbook_etc)
			) {
				return true;
			}
		} else {
			if (
				!empty($logbook_time_takeover) and
				!empty($logbook_time_return) and
				!empty($logbook_defects_on_arrival) and
				!empty($logbook_damage_report) and
				isset($logbook_etc)
			) {
				return true;
			}
		}
		return false;
	}

	public function load_entries_from_POST()
	{
		if ($this->item_type == BOOKING_ITEM_TYPE_BOAT) {
			$this->logbook_crew = htmlspecialchars(stripslashes($_POST['logbook_crew']));
			$this->logbook_time_takeover = htmlspecialchars(stripslashes($_POST['logbook_time_takeover']));
			$this->logbook_time_return = htmlspecialchars(stripslashes($_POST['logbook_time_return']));
			$this->logbook_defects_on_arrival = htmlspecialchars(stripslashes($_POST['logbook_defects_on_arrival']));
			$this->logbook_weather = htmlspecialchars(stripslashes($_POST['logbook_weather']));
			$this->logbook_damage_report = htmlspecialchars(stripslashes($_POST['logbook_damage_report']));
			$this->logbook_etc = htmlspecialchars(stripslashes($_POST['logbook_etc']));
		} else {
			$this->logbook_time_takeover = htmlspecialchars(stripslashes($_POST['logbook_time_takeover']));
			$this->logbook_time_return = htmlspecialchars(stripslashes($_POST['logbook_time_return']));
			$this->logbook_defects_on_arrival = htmlspecialchars(stripslashes($_POST['logbook_defects_on_arrival']));
			$this->logbook_damage_report = htmlspecialchars(stripslashes($_POST['logbook_damage_report']));
			$this->logbook_etc = htmlspecialchars(stripslashes($_POST['logbook_etc']));
		}
		$this->logbook_created = current_datetime();
	}

	public function show_update_confirmation()
	{
		echo '<div class="text-center" id="booking">' .
			'<h1>Logbucheintrag gespeichert</h1>' .
			'<p>Für ' . $this->item_name . ' am ' . $this->booking_date->format("d.m.Y") . '</p>' .
			'<a class="btn btn-primary" href="/' . BOOKING_HOME_URL . '">Zurück zum Buchungsportal</a>' .
			'</div>';
	}

	public function show_dialog()
	{
		if ($this->item_type == BOOKING_ITEM_TYPE_BOAT) {
			$this->show_boat_dialog();
		} else {
			$this->show_gear_dialog();
		}
		$this->check_entries_script();
	}

	private function show_gear_dialog()
	{
?>
		<div id="booking">
			<h1>Logbucheintrag</h1>
			<div class="my-3">
				<form action="<?= site_url('/' . LOGBOOK_ENTRY_URL); ?>" method="post" class="needs-validation" novalidate>
					<input id="booking_id" type="text" name="booking_id" value="<?= $this->ID; ?>" hidden>
					<input id="item_type" type="text" name="item_type" value="<?= $this->item_type; ?>" hidden>
					<div class="mb-3">
						<label for="item_name">Zubehör:</label>
						<input id="item_name" type="text" name="item_name" value="<?= $this->item_name; ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="logbook_skipper">Entleiher/Entleiherin:</label>
						<input id="logbook_skipper" type="text" name="logbook_skipper" value="<?= $this->user_display_name; ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="booking_date">Buchungsdatum:</label>
						<input id="booking_date" type="text" name="booking_date" value="<?= $this->booking_date->format('d.m.Y'); ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="logbook_time_takeover">Uhrzeit der Übernahme:</label>
						<input id="logbook_time_takeover" type="text" name="logbook_time_takeover" value="<?= $this->logbook_time_takeover; ?>" required>
						<div class="invalid-feedback">
							Bitte gib die Uhrzeit ein, wann du das Zubehör übernommen hast.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_time_return">Uhrzeit der Rückgabe:</label>
						<input id="logbook_time_return" type="text" name="logbook_time_return" value="<?= $this->logbook_time_return; ?>" required>
						<div class="invalid-feedback">
							Bitte gib die Uhrzeit ein, wann du das Zubehör zurück gebracht hast.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_defects_on_arrival">Mängel bei Übernahme:</label>
						<textarea id="logbook_defects_on_arrival" type="text" name="logbook_defects_on_arrival" required><?= $this->logbook_defects_on_arrival; ?></textarea>
						<div class="invalid-feedback">
							Falls es keine Mängel gab, schreibe bitte „keine“ in das Feld.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_damage_report">Entstandene Schäden:</label>
						<textarea id="logbook_damage_report" type="text" name="logbook_damage_report" required><?= $this->logbook_damage_report; ?></textarea>
						<div class="invalid-feedback">
							Falls keine Schäden entstanden sind, schreibe bitte „keine“ in das Feld.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_etc">Sonstiges:</label>
						<textarea id="logbook_etc" type="text" name="logbook_etc"><?= $this->logbook_etc; ?></textarea>
					</div>
					<button class="btn btn-primary" type="submit">Eintrag speichern</button>
				</form>
			</div>
		</div>
	<?php
	}

	private function check_entries_script()
	{
	?>
		<script>
			// Disabling form submissions if there are invalid fields
			(function() {
				'use strict'

				// Fetch all the forms we want to apply custom Bootstrap validation styles to
				var forms = document.querySelectorAll('.needs-validation')

				// Loop over them and prevent submission
				Array.prototype.slice.call(forms)
					.forEach(function(form) {
						form.addEventListener('submit', function(event) {
							if (!form.checkValidity()) {
								event.preventDefault()
								event.stopPropagation()
							}

							form.classList.add('was-validated')
						}, false)
					})
			})()
		</script>
	<?php
	}

	private function show_boat_dialog()
	{
	?>
		<div id="booking">
			<h1>Logbucheintrag</h1>
			<div class="my-3">
				<form action="<?= site_url('/' . LOGBOOK_ENTRY_URL); ?>" method="post" class="needs-validation" novalidate>
					<input id="booking_id" type="text" name="booking_id" value="<?= $this->ID; ?>" hidden>
					<input id="item_type" type="text" name="item_type" value="<?= $this->item_type; ?>" hidden>
					<div class="mb-3">
						<label for="item_name">Boot:</label>
						<input id="item_name" type="text" name="item_name" value="<?= $this->item_name; ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="logbook_skipper">Entleiher/Entleiherin:</label>
						<input id="logbook_skipper" type="text" name="logbook_skipper" value="<?= $this->user_display_name; ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="booking_date">Buchungsdatum:</label>
						<input id="booking_date" type="text" name="booking_date" value="<?= $this->booking_date->format('d.m.Y'); ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="logbook_crew">Mitseglerinnen und -segler:</label>
						<textarea id="logbook_crew" type="text" name="logbook_crew" required><?= $this->logbook_crew; ?></textarea>
						<div class="invalid-feedback">
							Bitte gib die Mitsegler an
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_time_takeover">Uhrzeit der Bootübernahme:</label>
						<input id="logbook_time_takeover" type="text" name="logbook_time_takeover" value="<?= $this->logbook_time_takeover; ?>" required>
						<div class="invalid-feedback">
							Bitte gib die Uhrzeit ein, wann du das Boot übernommen hast.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_time_return">Uhrzeit der Rückkehr:</label>
						<input id="logbook_time_return" type="text" name="logbook_time_return" value="<?= $this->logbook_time_return; ?>" required>
						<div class="invalid-feedback">
							Bitte gib die Uhrzeit ein, wann du zurückgekommen bist.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_defects_on_arrival">Mängel bei Übernahme:</label>
						<textarea id="logbook_defects_on_arrival" type="text" name="logbook_defects_on_arrival" required><?= $this->logbook_defects_on_arrival; ?></textarea>
						<div class="invalid-feedback">
							Falls es keine Mängel gab, schreibe bitte „keine“ in das Feld.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_weather">Wetter:</label>
						<textarea id="logbook_weather" type="text" name="logbook_weather" required><?= $this->logbook_weather; ?></textarea>
						<div class="invalid-feedback">
							Bitte beschreibe kurz, wie das Wetter während des Segelns war.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_damage_report">Entstandene Schäden:</label>
						<textarea id="logbook_damage_report" type="text" name="logbook_damage_report" required><?= $this->logbook_damage_report; ?></textarea>
						<div class="invalid-feedback">
							Falls keine Schäden entstanden sind, schreibe bitte „keine“ in das Feld.
						</div>
					</div>
					<div class="mb-3">
						<label for="logbook_etc">Sonstiges:</label>
						<textarea id="logbook_etc" type="text" name="logbook_etc"><?= $this->logbook_etc; ?></textarea>
					</div>
					<button class="btn btn-primary" type="submit">Eintrag speichern</button>
				</form>
			</div>
		</div>
<?php
	}
}
