<?php

require_once(__DIR__ . "/../settings/BookingItemModel.php");
require_once(__DIR__ . "/LogbookEntry.php");
require_once(__DIR__ . "/MaintenanceNoteDialog.php");
require_once(__DIR__ . "/../booking/BookingExceptions.php");

/**
 * @property	object	$db
 * @property	int	$selected_year
 * @property	array $available_years
 */
class Logbook extends BookingItemModel
{
	public function __construct(string $item_tag, int $year)
	{
		global $wpdb;
		$this->db = $wpdb;
		parent::__construct();

		$this->selected_year = $year;

		if (in_array($item_tag, self::get_all_booking_item_tags())) {
			$this->load_by_item_tag($item_tag);
		} else {
			throw new StandardBookingException('Ungültige Auswahl für Boot oder Zubehör', 'Logbuch nicht verfügbar');
		}

		if ($this->selected_year > intval(date("Y"))) {
			throw new StandardBookingException("Das ausgewählte Jahr liegt in der Zukunft", 'Logbuch nicht verfügbar');
		}

		$this->available_years = $this->get_available_years($this->item_tag);
		$current_year = intval(date("Y"));
		if (!in_array($current_year, $this->available_years)) {
			array_push($this->available_years, $current_year);
			rsort($this->available_years);
		}

		if (!in_array($this->selected_year, $this->available_years)) {
			throw new StandardBookingException("Für das gewählte Jahr wurden keine Einträge angelegt", 'Logbuch nicht verfügbar');
		}
	}

	private function get_available_years(string $item_tag)
	{
		$now = current_datetime();

		$years = $this->db->get_results(
			"SELECT DISTINCT YEAR(booking_date) AS year FROM " . BOOKINGS_TABLE .
				" WHERE " .
				BOOKINGS_TABLE . ".boat='" . $item_tag . "' AND " .
				BOOKINGS_TABLE . ".booking_date<='" . $now->format("Y-m-d") . "'" .
				"ORDER BY YEAR(booking_date) DESC"
		);

		$year_list = [];
		foreach ($years as $year) {
			array_push($year_list, intval($year->year));
		}
		return $year_list;
	}

	public function show()
	{
		$lb_entries = LogbookEntry::get_logbook_entries_by_tag_and_year($this->item_tag, $this->selected_year);
		$mn_entries = MaintenanceNoteDialog::get_by_item_tag($this->item_tag, $this->selected_year);

		echo '<div id="archive">' .
			'<h2>Logbuch für ' . $this->item_name . ' - ' . $this->selected_year . '</h2>';
		$this->show_create_mn_button();
		$this->show_year_selector();

		// When no entries are available
		if (count($mn_entries) == 0 and count($lb_entries) == 0) {
			$this->show_no_entries_available();
		} else {
			while (count($mn_entries) > 0 or count($lb_entries) > 0) {
				if (count($mn_entries) > 0 and count($lb_entries) > 0) {
					if ($mn_entries[0]->created > $lb_entries[0]->booking_date) {
						$mn_entries[0]->show();
						array_shift($mn_entries);
					} else {
						$lb_entries[0]->show();
						array_shift($lb_entries);
					}
				} else {
					if (count($mn_entries) > 0) {
						$mn_entries[0]->show();
						array_shift($mn_entries);
					} else {
						$lb_entries[0]->show();
						array_shift($lb_entries);
					}
				}
			}
		}
		echo '</div>';
	}

	private function show_year_selector()
	{
		echo '<div class="mb-3">' .
			'<select class="form-select mb-3" name="year_selector" id="year_selector">' .

			'<option selected>' . $this->selected_year . '</option>';

		foreach ($this->available_years as $year) {
			if ($year != $this->selected_year) {
				echo '<option value="/' . LOGBOOK_URL . '/?item=' . $this->item_tag . '&jahr=' . $year . '">' . $year . '</option>';
			}
		}
		echo '</select>' .
			'</div>';
?>
		<script type="text/javascript">
			var urlmenu = document.getElementById('year_selector');
			urlmenu.onchange = function() {
				window.location = this.options[this.selectedIndex].value;
			};
		</script>
<?php
	}

	private function show_no_entries_available()
	{
		echo '<div class="text-center">' .
			'<p>Für ' . $this->selected_year . ' existieren noch keine Einträge</p>' .
			'</div>';
	}

	private function show_create_mn_button()
	{
		echo '<div class="mb-3 text-center">' .
			'<a class="btn btn-primary" href="/' . MAINTENANCE_NOTE_URL . '?item=' . $this->item_tag . '">Neue Wartungsnotiz anlegen</a>' .
			'</div>';
	}
}
