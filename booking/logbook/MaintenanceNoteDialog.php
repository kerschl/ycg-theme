<?php
require_once(__DIR__ . "/MaintenanceNote.php");

class MaintenanceNoteDialog extends MaintenanceNote
{
	public function show_confirmation(string $heading, string $content)
	{
?>
		<div class="text-center" id="booking">
			<h1><?= $heading ?></h1>
			<p><?= $content ?></p>
			<a class="btn btn-primary" href="/<?= LOGBOOK_URL ?> ?item=<?= $this->item_tag ?>">Zurück zum Logbuch</a>
		</div>
	<?php
	}

	public function show()
	{
	?>
		<div class="mb-3 d-flex flex-row text-decoration-none border rounded overflow-hidden">
			<div class="m-3 text-black flex-fill">
				<h4 class="mb-2"><?= $this->created->format("d.m.Y") ?> - Wartungsnotiz</h4>
				<h6 class="mb-0">Von:</h6>
				<p class=""><?= $this->user_display_name ?></p>
				<h6 class="mb-0">Notiz:</h6>
				<p class="mb-0"><?= nl2br($this->note) ?></p>
			</div>
			<?php
			if (get_current_user_id() == $this->user_id) {
				echo '<div class="m-3">' .
					'<a class="btn btn-primary" href="/' . MAINTENANCE_NOTE_URL . '?id=' . $this->ID . '">' . $this->edit_icon() . '</a>' .
					'</div>';
			}
			?>
		</div>
	<?php
	}

	private function edit_icon()
	{
		return '
		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
			<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
			<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
	  	</svg>';
	}

	public function show_form(bool $show_with_id = false, bool $delete_button = false)
	{
	?>
		<div id="booking">
			<h1>Wartungsnotiz</h1>
			<div class="my-3">
				<form action="<?= site_url('/' . MAINTENANCE_NOTE_URL); ?>" method="post" class="needs-validation" novalidate>
					<?php
					if ($show_with_id) {
						echo '<input id="id" type="text" name="id" value="' . $this->ID . '" hidden>';
					} else {
						echo '<input id="item_tag" type="text" name="item_tag" value="' . $this->item_tag . '" hidden>';
					}
					?>
					<input id="user_id" type="text" name="user_id" value="<?= $this->user_id; ?>" hidden>
					<div class="mb-3">
						<label for="item_name">Für Boot/Zubehör:</label>
						<input id="item_name" type="text" name="item_name" value="<?= $this->item_name; ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="user_display_name">Von:</label>
						<input id="user_display_name" type="text" name="user_display_name" value="<?= $this->user_display_name; ?>" readonly>
					</div>
					<div class="mb-3">
						<label for="note">Notiz:</label>
						<textarea id="note" type="text" name="note" required><?= $this->note; ?></textarea>
						<div class="invalid-feedback">
							Bitte gib eine Notiz an
						</div>
					</div>
					<button class="btn btn-primary" type="submit" name="update">Eintrag speichern</button>
					<?php
					if ($show_with_id) {
						echo '<button class="btn btn-danger" type="submit" name="delete" onclick="return confirm(\'Bist du sicher, dass du die Wartungsnotiz löschen möchtest?\');">Eintrag löschen</button>';
					}
					?>
				</form>
			</div>
		</div>
		<script>
			// Disabling form submissions if there are invalid fields
			(function() {
				'use strict'

				// Fetch all the forms we want to apply custom Bootstrap validation styles to
				var forms = document.querySelectorAll('.needs-validation')

				// Loop over them and prevent submission
				Array.prototype.slice.call(forms)
					.forEach(function(form) {
						form.addEventListener('submit', function(event) {
							if (!form.checkValidity()) {
								event.preventDefault()
								event.stopPropagation()
							}

							form.classList.add('was-validated')
						}, false)
					})
			})()
		</script>
<?php
	}
}
