<?php
require_once(__DIR__ . "/../booking/BookingExceptions.php");
/**
 * @property	integer	$ID
 * @property	integer	$user_id
 * @property	string $user_display_name
 * @property	string	$item_name
 * @property	string	$item_type
 * @property	DateTime $booking_date
 * @property	DateTime	$logbook_created
 * @property	string	$logbook_crew
 * @property	string	$logbook_time_takeover
 * @property	string	$logbook_time_return
 * @property	string	$logbook_defects_on_arrival
 * @property	string	$logbook_weather
 * @property	string	$logbook_damage_report
 * @property	string	$logbook_etc
 */
class LogbookEntry
{
	private function __construct($row)
	{
		$this->ID = intval($row->id);
		$this->user_id = intval($row->user_id);
		$this->user_display_name = (get_user_by('ID', $this->user_id))->display_name;
		$this->item_name = $row->item_name;
		$this->item_type = ($row->item_type == BOOKING_ITEM_TYPE_BOAT) ? BOOKING_ITEM_TYPE_BOAT : BOOKING_ITEM_TYPE_GEAR;
		$this->booking_date = new DateTime($row->booking_date, wp_timezone());
		$this->logbook_created = new DateTime($row->logbook_created, wp_timezone());
		$this->logbook_crew = $row->logbook_crew;
		$this->logbook_time_takeover = $row->logbook_time_takeover;
		$this->logbook_time_return = $row->logbook_time_return;
		$this->logbook_defects_on_arrival = $row->logbook_defects_on_arrival;
		$this->logbook_weather = $row->logbook_weather;
		$this->logbook_damage_report = $row->logbook_damage_report;
		$this->logbook_etc = $row->logbook_etc;
	}

	public function update()
	{
		global $wpdb;
		$wpdb->update(
			BOOKINGS_TABLE,
			array(
				'logbook_created' => $this->logbook_created->format("Y-m-d H:i:s"),
				'logbook_crew' => $this->logbook_crew,
				'logbook_time_takeover' => $this->logbook_time_takeover,
				'logbook_time_return' => $this->logbook_time_return,
				'logbook_defects_on_arrival' => $this->logbook_defects_on_arrival,
				'logbook_damage_report' => $this->logbook_damage_report,
				'logbook_weather' => $this->logbook_weather,
				'logbook_etc' => $this->logbook_etc
			),
			array('ID' => $this->ID)
		);
	}

	public static function get_logbook_entry_by_id(int $ID)
	{
		$query =
			"SELECT " .
			BOOKINGS_TABLE . ".id, " .
			BOOKINGS_TABLE . ".user_id, " .
			BOOKING_ITEMS_TABLE . ".item_name, " .
			BOOKING_ITEMS_TABLE . ".item_type, " .
			BOOKINGS_TABLE . ".booking_date, " .
			BOOKINGS_TABLE . ".logbook_created, " .
			BOOKINGS_TABLE . ".logbook_crew, " .
			BOOKINGS_TABLE . ".logbook_time_takeover, " .
			BOOKINGS_TABLE . ".logbook_time_return, " .
			BOOKINGS_TABLE . ".logbook_defects_on_arrival, " .
			BOOKINGS_TABLE . ".logbook_weather, " .
			BOOKINGS_TABLE . ".logbook_damage_report, " .
			BOOKINGS_TABLE . ".logbook_etc " .
			" FROM " . BOOKINGS_TABLE . " INNER JOIN " . BOOKING_ITEMS_TABLE . " " .
			" ON " . BOOKINGS_TABLE . ".boat=" . BOOKING_ITEMS_TABLE . ".item_tag " .
			"WHERE " . BOOKINGS_TABLE . ".id = " . $ID;

		global $wpdb;
		$logbook_entry = $wpdb->get_results($query);

		if (count($logbook_entry) == 1) {
			return new static($logbook_entry[0]);
		} else {
			throw new StandardBookingException("Ungültiger Logbucheintrag ausgewählt", "Eintrag kann nicht angezeigt werden");
		}
	}

	public static function get_logbook_entries_by_tag_and_year(string $item_tag, int $year)
	{
		$now = current_datetime();

		$query =
			"SELECT " .
			BOOKINGS_TABLE . ".id, " .
			BOOKINGS_TABLE . ".user_id, " .
			BOOKING_ITEMS_TABLE . ".item_name, " .
			BOOKING_ITEMS_TABLE . ".item_type, " .
			BOOKINGS_TABLE . ".booking_date, " .
			BOOKINGS_TABLE . ".logbook_created, " .
			BOOKINGS_TABLE . ".logbook_crew, " .
			BOOKINGS_TABLE . ".logbook_time_takeover, " .
			BOOKINGS_TABLE . ".logbook_time_return, " .
			BOOKINGS_TABLE . ".logbook_defects_on_arrival, " .
			BOOKINGS_TABLE . ".logbook_weather, " .
			BOOKINGS_TABLE . ".logbook_damage_report, " .
			BOOKINGS_TABLE . ".logbook_etc " .
			" FROM " . BOOKINGS_TABLE . " INNER JOIN " . BOOKING_ITEMS_TABLE . " " .
			" ON " . BOOKINGS_TABLE . ".boat=" . BOOKING_ITEMS_TABLE . ".item_tag " .
			"WHERE YEAR(" . BOOKINGS_TABLE . ".booking_date) = " . $year . " AND " .
			BOOKINGS_TABLE . ".boat = '" . $item_tag . "' AND " .
			BOOKINGS_TABLE . ".booking_date <= '" . $now->format("Y-m-d") . "' " .
			"ORDER BY " . BOOKINGS_TABLE . ".booking_date DESC";

		global $wpdb;
		$logbook_enties = $wpdb->get_results($query);

		$entries = [];

		foreach ($logbook_enties as $entry) {
			array_push($entries, new static($entry));
		}
		return $entries;
	}

	public function show()
	{
		if ($this->item_type == BOOKING_ITEM_TYPE_BOAT) {
			$this->show_boat_logbook_entry();
		} else {
			$this->show_gear_logbook_entry();
		}
	}

	private function show_boat_logbook_entry()
	{
?>
		<div class="mb-3 d-flex flex-row text-decoration-none border rounded overflow-hidden">
			<div class="m-3 text-black flex-fill">
				<h4 class="mb-2"><?= $this->booking_date->format("d.m.Y") ?></h4>
				<h6 class="mb-0">Entleiher/Entleiherin:</h6>
				<p class=""><?= $this->user_display_name ?></p>
				<h6 class="mb-0">Mitsegler/Mitseglerinnen:</h6>
				<p class=""><?= nl2br($this->logbook_crew) ?></p>
				<h6 class="mb-0">Uhrzeit der Übernahme:</h6>
				<p class=""><?= $this->logbook_time_takeover ?></p>
				<h6 class="mb-0">Uhrzeit der Rückkehr:</h6>
				<p class=""><?= $this->logbook_time_return ?></p>
				<h6 class="mb-0">Mängel bei Übernahme:</h6>
				<p class=""><?= nl2br($this->logbook_defects_on_arrival) ?></p>
				<h6 class="mb-0">Wetter:</h6>
				<p class=""><?= nl2br($this->logbook_weather) ?></p>
				<h6 class="mb-0">Enstandene Schäden:</h6>
				<p class=""><?= nl2br($this->logbook_damage_report) ?></p>
				<h6 class="mb-0">Sonstiges:</h6>
				<p class=""><?= nl2br($this->logbook_etc) ?></p>
			</div>
			<?php
			if (get_current_user_id() == $this->user_id) {
				echo '<div class="m-3">';
				echo '<a class="btn btn-primary" href="/' . LOGBOOK_ENTRY_URL . '?id=' . $this->ID . '">' . $this->edit_icon() . '</a>';
				echo '</div>';
			}
			?>
		</div>
	<?php
	}

	private function show_gear_logbook_entry()
	{
	?>
		<div class="mb-3 d-flex flex-row text-decoration-none border rounded overflow-hidden">
			<div class="m-3 text-black flex-fill">
				<h4 class="mb-2"><?= $this->booking_date->format("d.m.Y") ?></h4>
				<h6 class="mb-0">Entleiher/Entleiherin:</h6>
				<p class=""><?= $this->user_display_name ?></p>
				<h6 class="mb-0">Uhrzeit der Übernahme:</h6>
				<p class=""><?= $this->logbook_time_takeover ?></p>
				<h6 class="mb-0">Uhrzeit der Rückkehr:</h6>
				<p class=""><?= $this->logbook_time_return ?></p>
				<h6 class="mb-0">Mängel bei Übernahme:</h6>
				<p class=""><?= nl2br($this->logbook_defects_on_arrival) ?></p>
				<h6 class="mb-0">Enstandene Schäden:</h6>
				<p class=""><?= nl2br($this->logbook_damage_report) ?></p>
				<h6 class="mb-0">Sonstiges:</h6>
				<p class=""><?= nl2br($this->logbook_etc) ?></p>
			</div>
			<?php
			if (get_current_user_id() == $this->user_id) {
				echo '<div class="m-3">';
				echo '<a class="btn btn-primary" href="/' . LOGBOOK_ENTRY_URL . '?id=' . $this->ID . '">' . $this->edit_icon() . '</a>';
				echo '</div>';
			}
			?>
		</div>
<?php
	}

	private function edit_icon()
	{
		return '
		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
			<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
			<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
	  	</svg>';
	}
}
