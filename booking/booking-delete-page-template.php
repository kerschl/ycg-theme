<?php
/* 
Template Name: Buchung-Stornieren
*/
require_once(__DIR__ . "/booking/CancelBookingForm.php");

get_header();

if (is_user_logged_in()) {
	$booking_id = $_POST['booking_id'];

	// Check if booking needs to be deleted
	if (isset($booking_id)) {
		try {
			$form = new CancelBookingForm($booking_id);
			$form->user_is_allowed_to_delte_booking();
			$form->delete_booking();
			$form->show_cancel_confirmation();
		} catch (StandardBookingException $e){
			$e->show_booking_error();
		}
	} else {
		$booking_id = intval($_GET["id"]);
		try {
			$form = new CancelBookingForm($booking_id);
			$form->user_is_allowed_to_delte_booking();
			$form->show();
		} catch (StandardBookingException $e) {
			$e->show_booking_error();
		}
	}
} else {
	get_template_part('template-parts/booking/login-form');
}

get_footer();
