<?php
require_once(__DIR__ . "/../settings/BookingItemModel.php");
/**
 * Class for displaying the booking items in the home menu
 */
class ItemsDisplay
{
	public $items = null;

	public function __construct()
	{
		$this->items = BookingItemModel::get_all_booking_items();
	}

	public function show_items()
	{
		if (count($this->items) > 0) {
			$this->show_start();
			foreach ($this->items as $item) {
				$this->show_item($item);
			}
			$this->show_end();
		} else {
			$this->show_no_item_available();
		}
	}

	private function show_start()
	{
		echo '<div class="row row-cols-1 row-cols-md-2 g-3 mb-3">';
	}

	private function show_end()
	{
		echo '</div>';
	}

	private function show_item(BookingItemModel $item)
	{
?>
		<div class="col">
			<div class="card">
				<div class="row">
					<div class="col-4 d-flex align-items-center">
						<img src="<?= $item->img_url ?>" class="card-img ms-2" alt="...">
					</div>
					<div class="col-8">
						<div class="card-body">
							<h5 class="card-title mb-1"><?= $item->item_name ?></h5>
							<p class="card-text mb-1"><?= $item->item_description ?></p>
							<a href="/<?= BOOKING_CALENDAR_URL . '?item=' . $item->item_tag ?>" class="btn btn-primary mb-1">Kalender</a>
							<a href="/<?= LOGBOOK_URL . '?item=' . $item->item_tag ?>" class="btn btn-primary mb-1">Logbuch</a>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
	}

	private function show_no_item_available()
	{
		echo '<p class="card-text">Keine Boote oder Zubehör verfügbar</p>';
	}
}
