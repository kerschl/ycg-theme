<?php

require_once(__DIR__ . "/BookingCard.php");
require_once(__DIR__ . "/../booking/BookingExceptions.php");

/**
 * @property	int $user_id
 * @property	int	$selected_year
 * @property	array $available_years
 */
class UserBookingsDisplay
{
	public function __construct(int $year, int $user_id)
	{
		$this->selected_year = $year;
		$this->user_id = $user_id;

		$this->available_years = $this->get_available_years($this->user_id);
		$current_year = intval(date("Y"));
		if (!in_array($current_year, $this->available_years)) {
			array_push($this->available_years, $current_year);
			rsort($this->available_years);
		}

		if (!in_array($this->selected_year, $this->available_years)) {
			throw new StandardBookingException("Für das gewählte Jahr hast du keine Buchungen angelegt", "Anfrage nicht möglich");
		}
	}

	private function get_available_years(string $user_id)
	{
		global $wpdb;
		$years = $wpdb->get_results(
			"SELECT DISTINCT YEAR(booking_date) AS year FROM " . BOOKINGS_TABLE .
				" WHERE " .
				BOOKINGS_TABLE . ".user_id='" . $user_id . "' " .
				"ORDER BY YEAR(booking_date) DESC"
		);

		$year_list = [];
		foreach ($years as $year) {
			array_push($year_list, intval($year->year));
		}
		return $year_list;
	}

	public function show()
	{
		$bookings = BookingCard::get_bookings_of_user_by_year($this->user_id, $this->selected_year);

		echo '<h4>Deine Buchungen für ' . $this->selected_year . '</h4>';
		$this->show_year_selector();

		if (count($bookings) == 0) {
			$this->show_no_entries_available();
		} else {
			foreach ($bookings as $booking) {
				$booking->show();
			}
		}
	}

	private function show_year_selector()
	{
		echo '<div class="mb-3">' .
			'<select class="form-select mb-3" name="year_selector" id="year_selector">' .

			'<option selected>' . $this->selected_year . '</option>';

		foreach ($this->available_years as $year) {
			if ($year != $this->selected_year) {
				echo '<option value="/' . BOOKING_HOME_URL . '/?jahr=' . $year . '">' . $year . '</option>';
			}
		}
		echo '</select>' .
			'</div>';
?>
		<script type="text/javascript">
			var urlmenu = document.getElementById('year_selector');
			urlmenu.onchange = function() {
				window.location = this.options[this.selectedIndex].value;
			};
		</script>
<?php
	}

	private function show_no_entries_available()
	{
		echo '<div class="text-center">' .
			'<p>Für ' . $this->selected_year . ' hast du noch keine Buchungen angelegt</p>' .
			'</div>';
	}
}
