<?php
require_once(__DIR__ . "/../booking/Booking.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");
/**
 * @property	BookingItemModel	$booking_item
 * @property	DateTime	$cancel_deadline_datetime
 */

class BookingCard extends Booking
{
	public function __construct($row)
	{
		parent::__construct($row);
		$this->booking_item = new BookingItemModel();
		$this->booking_item->load_by_item_tag($this->boat);

		$this->cancel_deadline_datetime = clone $this->booking_date;
		$this->cancel_deadline_datetime->setTime(0, 0);
		$this->cancel_deadline_datetime = $this->cancel_deadline_datetime->sub(new DateInterval("PT" . $this->booking_item->cancel_deadline . "H"));
	}

	private function deadline_is_exceeded()
	{
		$now = current_datetime();
		return $this->cancel_deadline_datetime < $now;
	}

	private function get_booking_rate()
	{
		if ($this->booking_type == BOOKING_RATE_ADULT) {
			return 'Erwachsene';
		} elseif ($this->booking_type == BOOKING_RATE_YOUTH) {
			return 'Jugendliche';
		} else {
			return 'Regatta';
		}
	}

	public function show()
	{
?>
		<div class="mb-3">
			<div class="d-flex text-decoration-none border rounded overflow-hidden">
				<div class="p-3 me-auto text-black d-flex align-items-start flex-column">
					<h4 class="mb-2"><?= $this->booking_item->item_name . ' - ' . $this->booking_date->format("d.m.Y"); ?></h4>
					<p class="mb-0">Tarif: <?= $this->get_booking_rate(); ?></p>
					<p class="mb-0">Gebühr: <?= number_format($this->booking_cost, 2); ?>€</p>
					<p class="mb-0">Erstellt am <?= $this->booking_created->format("d.m.Y H:i"); ?></p>
				</div>
				<div class="d-flex align-items-center">
					<p class="text-center mb-0">
						<?php
						if ($this->deadline_is_exceeded()) {
							echo '<button type="button" class="btn btn-secondary mx-3 mt-2 mb-2" disabled>Stornieren</button>';
						} else {
							echo '<a class="btn btn-danger mx-3 mt-2 mb-2" href="/' . CANCEL_BOOKING_URL . '?id=' . $this->id . '">Stornieren</a>';
						}
						?>
						<a class="btn btn-primary mx-3 mt-2 mb-2" href="<? echo LOGBOOK_ENTRY_URL ?>?id=<?= $this->id; ?>" role="button">Logbuch</a>
					</p>
				</div>
			</div>
		</div>
<?php
	}
}
