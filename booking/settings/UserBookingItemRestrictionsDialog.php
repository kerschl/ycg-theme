<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(__DIR__ . "/UserMetaDataDialog.php");
require_once(__DIR__ . "/BookingItemModel.php");
require_once(__DIR__ . "/../user/BookingUser.php");

/**
 * @property array	$booking_item_access_options
 */
class UserBookingItemRestrictionsDialog extends UserMetaDataDialog
{
	public function __construct()
	{
		$this->booking_item_access_options = array(
			"primary_option_key" => BOOKING_ITEM_ACCESS_FALSE,
			"primary_option_desc" => "keine Freigabe",
			"secondary_option_key" => BOOKING_ITEM_ACCESS_TRUE,
			"secondary_option_desc" => "Freigegeben"
		);

		add_action('show_user_profile', array(&$this, 'show_fields'));
		add_action('edit_user_profile', array(&$this, 'show_fields'));

		add_action('personal_options_update', array(&$this, 'save_fields'));
		add_action('edit_user_profile_update', array(&$this, 'save_fields'));
	}

	public function show_fields($user_id)
	{
		parent::__construct("Buchungsfreigaben", $user_id);
		$items = BookingItemModel::get_all_booking_items();

		if (count($items) > 0) {
			$this->start_dialog_table();
			foreach ($items as $item) {
				$this->select_dialog($item->item_name, $item->item_tag, $this->booking_item_access_options, $item->item_description);
			}
			$this->end_dialog_table();
		}
	}

	public function save_fields($user_id)
	{
		$items = BookingItemModel::get_all_booking_items();

		$user = new BookingUser($user_id);

		if (count($items) > 0) {
			foreach ($items as $item) {
				$user->set_item_access($item->item_tag, $_POST[$item->item_tag]);
			}
		}
	}
}
