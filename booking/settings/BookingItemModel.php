<?php

/**
 * @property Integer $id = null;
 * @property String $item_type = null;
 * @property String $item_tag = null;
 * @property String $item_name = null;
 * @property String $item_description = null;
 * @property Boolean $booking_enabled = null;
 * @property String $reason_booking_disabled = null;
 * @property Integer $max_number_bookings_ahead = null;
 * @property Boolean $double_booking_on_weekend = null;
 * @property Integer $cancel_deadline = null;
 * @property Integer $no_rules_apply_deadline = null;
 * @property Float $booking_price_youth = null;
 * @property Float $booking_price_adult = null;
 * @property String $img_url = null;
 */
class BookingItemModel
{
	public function __construct()
	{
		$this->id = null;
		$this->item_type = null;
		$this->item_tag = null;

		$this->item_name = null;
		$this->item_description = null;

		$this->booking_enabled = null;
		$this->reason_booking_disabled = null;

		$this->max_number_bookings_ahead = null;
		$this->double_booking_on_weekend = null;

		$this->cancel_deadline = null;
		$this->no_rules_apply_deadline = null;

		$this->booking_price_youth = null;
		$this->booking_price_adult = null;

		$this->img_url = null;
	}

	public static function all_POST_arg_are_set()
	{
		if (
			isset($_POST['id']) and
			isset($_POST['item_tag']) and
			isset($_POST['item_name']) and
			isset($_POST['item_description']) and
			isset($_POST['reason_booking_disabled']) and
			isset($_POST['max_number_bookings_ahead']) and
			isset($_POST['cancel_deadline']) and
			isset($_POST['no_rules_apply_deadline']) and
			isset($_POST['booking_price_youth']) and
			isset($_POST['booking_price_adult']) and
			isset($_POST['img_url'])
		) {
			return true;
		} else {
			return false;
		}
	}

	public function load_from_POST()
	{
		$this->id = intval($_POST['id']);
		$this->item_tag = $_POST['item_tag'];

		$this->item_name = htmlspecialchars(stripslashes($_POST['item_name']));
		$this->item_description = htmlspecialchars(stripslashes($_POST['item_description']));

		$this->booking_enabled = ($_POST['booking_enabled'] == 'on') ? True : False;
		$this->reason_booking_disabled = htmlspecialchars(stripslashes($_POST['reason_booking_disabled']));

		$this->max_number_bookings_ahead = intval($_POST['max_number_bookings_ahead']);
		$this->double_booking_on_weekend = ($_POST['double_booking_on_weekend'] == 'on') ? True : False;

		$this->cancel_deadline = (intval($_POST['cancel_deadline']) > 0) ? intval($_POST['cancel_deadline']) : 0;
		$this->no_rules_apply_deadline = (intval($_POST['no_rules_apply_deadline']) > 0) ? intval($_POST['no_rules_apply_deadline']) : 0;

		$this->booking_price_youth = floatval($_POST['booking_price_youth']);
		$this->booking_price_adult = floatval($_POST['booking_price_adult']);

		$this->img_url = $_POST['img_url'];
	}

	public function load_by_item_tag($item_tag)
	{
		global $wpdb;
		$item = $wpdb->get_results("SELECT * FROM `" . BOOKING_ITEMS_TABLE . "` WHERE `item_tag` = '" . $item_tag . "'")[0];

		$this->id = intval($item->id);
		$this->item_type = ($item->item_type == BOOKING_ITEM_TYPE_BOAT) ? BOOKING_ITEM_TYPE_BOAT : BOOKING_ITEM_TYPE_GEAR;
		$this->item_tag = $item->item_tag;

		$this->item_name = $item->item_name;
		$this->item_description = $item->item_description;

		$this->booking_enabled = ($item->booking_enabled == '1') ? True : False;
		$this->reason_booking_disabled = $item->reason_booking_disabled;

		$this->max_number_bookings_ahead = intval($item->max_number_bookings_ahead);
		$this->double_booking_on_weekend = ($item->double_booking_on_weekend == '1') ? True : False;

		$this->cancel_deadline = (intval($item->cancel_deadline) > 0) ? intval($item->cancel_deadline) : 0;
		$this->no_rules_apply_deadline = (intval($item->no_rules_apply_deadline) > 0) ? intval($item->no_rules_apply_deadline) : 0;

		$this->booking_price_youth = floatval($item->booking_price_youth);
		$this->booking_price_adult = floatval($item->booking_price_adult);

		$this->img_url = $item->img_url;
	}

	public function update()
	{
		global $wpdb;
		$wpdb->update(
			BOOKING_ITEMS_TABLE,
			array(
				"item_name" => $this->item_name,
				"item_description" => $this->item_description,
				"booking_enabled" => $this->booking_enabled,
				"reason_booking_disabled" => $this->reason_booking_disabled,

				"max_number_bookings_ahead" => $this->max_number_bookings_ahead,
				"double_booking_on_weekend" => $this->double_booking_on_weekend,

				"cancel_deadline" => $this->cancel_deadline,
				"no_rules_apply_deadline" => $this->no_rules_apply_deadline,

				"booking_price_youth" => $this->booking_price_youth,
				"booking_price_adult" => $this->booking_price_adult,

				"img_url" => $this->img_url
			),
			array(
				"id" => $this->id
			)
		);
		return;
	}

	public static function get_all_booking_items()
	{
		$booking_item_tags = self::get_all_booking_item_tags();
		$booking_items = [];
		foreach ($booking_item_tags as $tag) {
			$item = new static();
			$item->load_by_item_tag($tag);
			array_push($booking_items, $item);
		}
		return $booking_items;
	}

	public static function get_all_booking_item_tags()
	{
		global $wpdb;
		$tags_db_query =  $wpdb->get_results("SELECT `item_tag` FROM `" . BOOKING_ITEMS_TABLE . "`");
		$tags = [];

		foreach ($tags_db_query as $tag) {
			array_push($tags, $tag->item_tag);
		}
		return $tags;
	}
}
