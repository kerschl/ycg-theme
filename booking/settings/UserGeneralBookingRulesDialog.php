<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(__DIR__ . "/UserMetaDataDialog.php");
require_once(__DIR__ . "/../user/BookingUser.php");

class UserGeneralBookingRulesDialog extends UserMetaDataDialog
{
	public function __construct()
	{
		$this->booking_user_role_select_options = array(
			"primary_option_key" => BOOKING_USER_ROLE_NORMAL,
			"primary_option_desc" => "Normal",
			"secondary_option_key" => BOOKING_USER_ROLE_TRAINER,
			"secondary_option_desc" => "Trainer"
		);
		$this->booking_rate_select_options = array(
			"primary_option_key" => BOOKING_RATE_ADULT,
			"primary_option_desc" => "Erwachsene",
			"secondary_option_key" => BOOKING_RATE_YOUTH,
			"secondary_option_desc" => "Jugend"
		);

		add_action('show_user_profile', array(&$this, 'show_fields'));
		add_action('edit_user_profile', array(&$this, 'show_fields'));

		add_action('personal_options_update', array(&$this, 'save_fields'));
		add_action('edit_user_profile_update', array(&$this, 'save_fields'));
	}

	public function show_fields($user_id)
	{
		parent::__construct("Buchungsverwaltung", $user_id); // A bit fishy but it works; User id gets only available after hook was fired
		$this->start_dialog_table();
		$this->select_dialog("Benutzerrolle", BOOKING_USER_ROLE_META_KEY, $this->booking_user_role_select_options, "Für Trainer gelten keine Buchungsbeschränkungen");
		$this->select_dialog("Buchungstarif", BOOKING_RATE_META_KEY, $this->booking_rate_select_options, "Buchungstarif auswählen");
		$this->text_dialog("Telefonnummer", BOOKING_USER_PHONE_NUMBER_META_KEY, "Bitte eine Handynummer angeben");
		$this->end_dialog_table();
	}

	public function save_fields($user_id)
	{
		if (!current_user_can('edit_user', $user_id))
			return false;

		$user = new BookingUser($user_id);
		$user->set_booking_rate($_POST[BOOKING_RATE_META_KEY]);
		$user->set_user_role($_POST[BOOKING_USER_ROLE_META_KEY]);
		$user->set_phone_number($_POST[BOOKING_USER_PHONE_NUMBER_META_KEY]);
	}
}
