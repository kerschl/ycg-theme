<?php

/**
 * Menu to create the DB and the Pages required for the booking system
 *
 * @author Christoph
 */
require_once(__DIR__ . "/BookingItemModel.php");

class BookingItemSettingsMenu extends BookingItemModel
{

	/**
	 * Autoload method
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		add_action('admin_menu', array(&$this, 'add_booking_rules_submenu'));
	}

	/**
	 * Register submenu
	 * @return void
	 */
	public function add_booking_rules_submenu()
	{
		add_submenu_page(
			'buchungen', // parent page slug
			'Buchungsregeln verwalten',
			'Buchungsregeln',
			'manage_options',
			'buchungsregeln',
			array(&$this, 'show_settings_page'),
			1 // menu position
		);
	}

	/**
	 * Render settings menu
	 * @return void
	 */
	public function show_settings_page()
	{
		$updated = $this->update_booking_item();

		if (!$updated) {
			$item_tags = self::get_all_booking_item_tags();

			if (in_array($_GET["item"], $item_tags)) {
				$item_tag = $_GET["item"];
			} else {
				$item_tag = $item_tags[0];
			}
			$this->load_by_item_tag($item_tag);
		}

		$this->show_setting_form();

		if ($updated) {
			do_action('admin_notices', $this->item_name);
		}
	}

	/**
	 * Update booking_item
	 */
	private function update_booking_item()
	{
		if (self::all_POST_arg_are_set()) {
			$this->load_from_POST();
			$this->update();
			add_action('admin_notices', array(&$this, 'show_update_confirmation_admin_notice'));
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Show update confirmation
	 */
	public function show_update_confirmation_admin_notice($item_name)
	{
		echo '<div class="notice notice-success is-dismissible">' .
			'<p>Einstellungen für <b>' . $item_name . '</b> wurden aktualisiert</p>' .
			'</div>';
	}

	private function get_item_uri(string $item)
	{
		$query = $_GET;
		// replace parameter(s)
		$query['item'] = $item;
		// rebuild url
		$query_result = http_build_query($query);
		// new link
		return $_SERVER['PHP_SELF'] . '?' . $query_result;
	}

	private function get_item_tabs(string $active)
	{
		echo '<h2 class="nav-tab-wrapper">';

		foreach (self::get_all_booking_items() as $item) {
			$active_class = ($active == $item->item_tag) ? "nav-tab-active" : "";
			echo '<a href="' . $this->get_item_uri($item->item_tag) . '" class="nav-tab ' . $active_class . '">' . $item->item_name . '</a>';
		}

		echo '</h2>';
	}

	private function show_setting_form()
	{
?>
		<div class="wrap">
			<h1>Buchungsregeln</h1>
			<?php $this->get_item_tabs($this->item_tag); ?>
			<form id="<?= $this->item_tag ?>" method="post" action="<?= esc_attr($_SERVER['REQUEST_URI']); ?>">
				<input type="hidden" id="id" name="id" value="<?= $this->id ?>">
				<input type="hidden" id="item_tag" name="item_tag" value="<?= $this->item_tag ?>">
				<input type="hidden" value="<?= esc_attr($_SERVER['REQUEST_URI']); ?>" name="redirect_to">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="item_name">Name</label></th>
							<td><input name="item_name" type="text" class="regular-text" id="item_name" value="<?= $this->item_name ?>" maxlength="30"></td>
						</tr>
						<tr>
							<th scope="row"><label for="item_description">Beschreibung</label></th>
							<td><input name="item_description" type="text" class="regular-text" id="item_description" value="<?= $this->item_description ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_enabled">Buchung aktiv</label></th>
							<td><input name="booking_enabled" type="checkbox" id="booking_enabled" <?= (($this->booking_enabled == 1) ? 'checked' : '') ?>></td>
						</tr>
						<tr>
							<th scope="row"><label for="reason_booking_disabled">Grund warum Buchung deaktiviert ist</label></th>
							<td><input name="reason_booking_disabled" type="text" class="regular-text" id="reason_booking_disabled" value="<?= $this->reason_booking_disabled ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="max_number_bookings_ahead">Maximale Anzahl von Buchungen im voraus (0 für beliebige)</label></th>
							<td><input name="max_number_bookings_ahead" type="number" min="0" id="max_number_bookings_ahead" value="<?= $this->max_number_bookings_ahead ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="double_booking_on_weekend">Kann für ein komplettes Wochenende gebucht werden</label></th>
							<td><input name="double_booking_on_weekend" type="checkbox" id="double_booking_on_weekend" <?= (($this->double_booking_on_weekend == 1) ? 'checked' : '') ?>></td>
						</tr>
						<tr>
							<th scope="row"><label for="cancel_deadline">Wie viele Stunden vor der Buchung kann nicht mehr storniert werden</label></th>
							<td><input name="cancel_deadline" type="number" min="0" id="cancel_deadline" value="<?= $this->cancel_deadline ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="no_rules_apply_deadline">Wie viele Stunden vorher werden die Buchungsregeln ausgesetzt</label></th>
							<td><input name="no_rules_apply_deadline" type="number" min="0" id="no_rules_apply_deadline" value="<?= $this->no_rules_apply_deadline ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_price_youth">Preis Jugend</label></th>
							<td><input name="booking_price_youth" type="number" step="0.01" min="0" id="booking_price_youth" value="<?= $this->booking_price_youth ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="booking_price_adult">Preis Erwachsene</label></th>
							<td><input name="booking_price_adult" type="number" step="0.01" min="0" id="booking_price_adult" value="<?= $this->booking_price_adult ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="img_url">Link zu Thumbnail</label></th>
							<td><input name="img_url" type="text" class="regular-text" id="img_url" value="<?= $this->img_url ?>"></td>
						</tr>

					</tbody>
				</table><br>
				<input class="button button-primary" type="submit" value="Regeln speichern">
			</form>
		</div>
<?php
	}
}
