<?php
require_once(__DIR__ . "/BookingItemModel.php");

class BookingItemRulesAccordion extends BookingItemModel
{
	public function show_booking_rules()
	{
		$items = self::get_all_booking_items();
		$item_count = 1;

		if (count($items) > 0) {
			echo '<div class="accordion mb-3" id="RulesAccordion">';
			foreach ($items as $item) {
				$this->show_booking_rule($item, $item_count);
				$item_count = $item_count + 1;
			}
			echo '</div>';
		} else {
			echo '<p>Error: No items registered</p>';
		}
	}
	private function show_booking_rule($item, $item_count)
	{
?>
		<div class="accordion-item">
			<h2 class="accordion-header" id="item-rule-<?= $item_count ?>">
				<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse<?= $item_count ?>" aria-expanded="false" aria-controls="flush-collapse<?= $item_count ?>">
					Regeln für <?= $item->item_name ?>
				</button>
			</h2>
			<div id="flush-collapse<?= $item_count ?>" class="accordion-collapse collapse" aria-labelledby="item-rule-<?= $item_count ?>" data-bs-parent="#RulesAccordion">
				<div class="accordion-body">
					<ol>
						<?php
						if ($item->max_number_bookings_ahead > 0) {
							echo '<li>Es dürfen maximal <strong>' . $item->max_number_bookings_ahead . '</strong> Segeltage im Voraus gebucht werden. Wenn ein Tag abgesegelt ist, kann ein neuer gebucht werden. </li>';
						}
						if ($item->double_booking_on_weekend == False) {
							echo '<li>Es darf maximal eine Buchung an einem Wochenende getätigt werden.</li>';
						}
						echo '<li>An ausgewählten Terminen ist die Buchung der Vereinsboote und des Zubehörs für Regattabuchungen vorbehalten. Die genauen Termine sind dem jeweiligen Buchungskalender zu entnehmen.</li>' .
							'<li>Buchungen können bis <strong>' . $item->cancel_deadline . '</strong> Stunden vor Beginn des Buchungstages storniert werden.</li>' .
							'<li>Ab <strong>' . $item->no_rules_apply_deadline . '</strong> Stunden vor Beginn eines Buchungstages gelten die oben aufgeführten Regeln nicht mehr.</li>';
						?>
					</ol>
				</div>
			</div>
		</div>
<?php
	}
}
