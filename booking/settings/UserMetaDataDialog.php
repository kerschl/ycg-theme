<?php
require_once(__DIR__ . "/../../../../../wp-load.php");

class UserMetaDataDialog
{
	public function __construct($dialog_title, $user)
	{
		$this->dialog_title = $dialog_title;
		$this->user = $user;
	}

	public function start_dialog_table()
	{
		echo '<hr>' .
			'<h3>' . $this->dialog_title . '</h3>' .
			'<table class="form-table">';
	}

	public function end_dialog_table()
	{
		echo '</table>';
	}

	public function select_dialog($label, $meta_key, $select_options, $description = "")
	{
		echo '<tr>' .
			'<th><label for="' . $meta_key . '">' . $label . '</label></th>' .

			'<td><select name="' . $meta_key . '" id="' . $meta_key . '">' .
			'<option selected="selected" value="' . $this->get_primary_select_value($meta_key, $select_options) . '">' . $this->get_primary_select_desc($meta_key, $select_options) . '</option>' .
			'<option value="' . $this->get_secondary_select_value($meta_key, $select_options) . '">' . $this->get_secondary_select_desc($meta_key, $select_options) . '</option>' .
			'</select><br>' .

			'<span class="description">' . $description . '</span>' .
			'</td>' .
			'</tr>';
	}

	private function get_primary_select_value($meta_key, $select_options)
	{
		if (get_the_author_meta($meta_key, $this->user->ID) == $select_options["secondary_option_key"]) {
			return $select_options["secondary_option_key"];
		} else {
			return $select_options["primary_option_key"];
		}
	}

	private function get_primary_select_desc($meta_key, $select_options)
	{
		if (get_the_author_meta($meta_key, $this->user->ID) == $select_options["secondary_option_key"]) {
			return $select_options["secondary_option_desc"];
		} else {
			return $select_options["primary_option_desc"];
		}
	}

	private function get_secondary_select_value($meta_key, $select_options)
	{
		if (get_the_author_meta($meta_key, $this->user->ID) == $select_options["secondary_option_key"]) {
			return $select_options["primary_option_key"];
		} else {
			return $select_options["secondary_option_key"];
		}
	}

	private function get_secondary_select_desc($meta_key, $select_options)
	{
		if (get_the_author_meta($meta_key, $this->user->ID) == $select_options["secondary_option_key"]) {
			return $select_options["primary_option_desc"];
		} else {
			return $select_options["secondary_option_desc"];
		}
	}

	public function text_dialog($label, $meta_key, $description = "")
	{
		echo '<tr>' .
			'<th><label for="' . $meta_key . '">' . $label . '</label></th>' .
			'<td>' .
			'<input type="text" name="' . $meta_key . '" id="' . $meta_key . '" value="' . esc_attr(get_the_author_meta($meta_key, $this->user->ID)) . '" class="regular-text"><br>' .
			'<span class="description">' . $description . '</span>' .
			'</td>' .
			'</tr>';
	}
}
