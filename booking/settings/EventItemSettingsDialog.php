<?php
require_once(__DIR__ . "/../../../../../wp-load.php");
require_once(__DIR__ . "/BookingItemModel.php");

class EventItemSettingsDialog
{
	public function __construct()
	{
		add_action('edit_form_after_editor', array(&$this, 'show_fields'));
		add_action('save_post', array(&$this, 'save_fields'));
	}

	public function show_fields($post)
	{
		if ($post->post_type == 'event') {
			$items = BookingItemModel::get_all_booking_items();

			if (count($items) > 0) {
				$this->start_dialog_table();
				foreach ($items as $item) {
					$item_blocked = get_post_meta($post->ID, BOOKING_ITEM_EVENT_META_PREFIX . $item->item_tag)[0];
					$item_blocked = ($item_blocked == null) ? BOOKING_ITEM_IS_NOT_BLOCKED_FOR_EVENT : $item_blocked;
					$this->toggle_dialog($item->item_name, $item->item_tag, $item_blocked);
				}
				$this->end_dialog_table();
			}
		}
	}

	private function start_dialog_table()
	{
		echo '<hr>' .
			'<h3>Boote und Zubehör für Veranstaltung blockieren</h3>' .
			'<table class="widefat">';
	}

	private function end_dialog_table()
	{
		echo '</table>';
	}

	private function toggle_dialog($label, $meta_key, $meta_value)
	{
		static $alternate = FALSE;
		if ($alternate) {
			echo '<tr class="alternate" >';
			$alternate = FALSE;
		} else {
			echo '<tr>';
			$alternate = TRUE;
		}
		echo '<th class="row-title" scope="row"><label for="' . $meta_key . '">' . $label . '</label></th>' .
			'<td><input name="' . $meta_key . '" type="checkbox" id="' . $meta_key . '" ' . (($meta_value == BOOKING_ITEM_IS_BLOCKED_FOR_EVENT) ? 'checked' : '') . '></td>' .
			'</tr>';
	}

	public function save_fields($post_id)
	{
		$post = get_post($post_id);
		if ($post->post_type == 'event') {
			$items = BookingItemModel::get_all_booking_items();

			if (count($items) > 0) {
				foreach ($items as $item) {
					if (isset($_POST[$item->item_tag]) && $_POST[$item->item_tag] == 'on') {
						update_post_meta($post_id, BOOKING_ITEM_EVENT_META_PREFIX . $item->item_tag, BOOKING_ITEM_IS_BLOCKED_FOR_EVENT);
					} else {
						update_post_meta($post_id, BOOKING_ITEM_EVENT_META_PREFIX . $item->item_tag, BOOKING_ITEM_IS_NOT_BLOCKED_FOR_EVENT);
					}
				}
			}
		}
	}
}
