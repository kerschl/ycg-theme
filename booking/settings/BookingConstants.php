<?php
// Booking item types
define("BOOKING_ITEM_TYPE_BOAT", "boat");
define("BOOKING_ITEM_TYPE_GEAR", "gear");

// User role constants
define("BOOKING_USER_ROLE_META_KEY", "booking_user_role");
define("BOOKING_USER_ROLE_NORMAL", "normal");
define("BOOKING_USER_ROLE_TRAINER", "trainer");

// Booking rate constants
define("BOOKING_RATE_META_KEY", "booking_rate");
define("BOOKING_RATE_YOUTH", "youth");
define("BOOKING_RATE_ADULT", "adult");

// Booking phone number constants
define("BOOKING_USER_PHONE_NUMBER_META_KEY", "phone_number");

// Booking item access constants
define("BOOKING_ITEM_ACCESS_TRUE", "true");
define("BOOKING_ITEM_ACCESS_FALSE", "false");

// Booking item access constants
define("BOOKING_ITEM_EVENT_META_PREFIX", "event_reservate_");
define("BOOKING_ITEM_IS_BLOCKED_FOR_EVENT", "true");
define("BOOKING_ITEM_IS_NOT_BLOCKED_FOR_EVENT", "false");

// Database table names
global $wpdb;
define("BOOKINGS_TABLE", $wpdb->prefix . "boat_booking");
define("BOOKING_ITEMS_TABLE", $wpdb->prefix . "booking_items");
define("MAINTENANCE_NOTES_TABLE", $wpdb->prefix . "maintenance_notes");

// Page slugs
define("BOOKING_CALENDAR_URL", "buchung-kalender");
define("CREATE_BOOKING_URL", "buchung-anlegen");
define("BOOKING_HOME_URL", "buchung");
define("LOGBOOK_URL", "logbuch");
define("LOGBOOK_ENTRY_URL", "logbucheintrag");
define("CANCEL_BOOKING_URL", "buchung-stornieren");
define("MAINTENANCE_NOTE_URL", "wartungsnotiz");
define("USER_ACCOUNT_URL", "benutzerkonto");
define("BILLING_URL", "abrechnung");
