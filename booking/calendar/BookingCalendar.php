<?php
require_once(__DIR__ . "/../booking/Booking.php");
require_once(__DIR__ . "/../booking/EventCalendar.php");
require_once(__DIR__ . "/../settings/BookingItemModel.php");
require_once(__DIR__ . "/../booking/BookingExceptions.php");

class BookingCalendar
{
	public function __construct(BookingItemModel $booking_item)
	{
		$this->booking_item = $booking_item;

		$this->now = current_datetime();

		$year  = null;
		$month = null;

		if (isset($_GET['jahr'])) {
			$year = $_GET['jahr'];
		} else if (null == $year) {
			$year = date("Y", time());
		}

		if (isset($_GET['monat'])) {
			$month = $_GET['monat'];
		} else if (null == $month) {
			$month = date("m", time());
		}

		if ($year == 0 or $month == 0) {
			throw new StandardBookingException("Ungültiges Datum");
		}

		// Set the corresponding private class vars
		$this->currentYear = intval($year);
		$this->currentMonth = intval($month);
		$this->daysInMonth = $this->days_in_month($month, $year);
		$this->day_of_the_week_of_the_first_day_in_the_month = $this->day_of_the_week_of_the_first_day_in_the_month();
	}

	/********************* PROPERTY ********************/
	private $monthLabels = array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
	private $currentYear = 0;
	private $currentMonth = 0;
	private $daysInMonth = 0;
	private $now = null;
	private $booking_item = null;
	private $day_of_the_week_of_the_first_day_in_the_month = 0;

	/********************* PUBLIC **********************/
	public function show()
	{
		$this->calendar_start();
		$this->navigation_menu();
		$this->show_days();
		$this->calendar_end();
		$this->calendar_CSS();
	}

	private function calendar_start()
	{
		echo '<div class="col-12 col-lg-8 mx-auto">' .
			'<div class="d-block position-static p-2 shadow rounded-3">' .
			'<div class="d-grid gap-1">' .
			'<div class="cal">';
	}

	private function calendar_end()
	{
		echo '</div></div></div></div>';
	}

	private function left_arrow()
	{
		echo '
		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
			<path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
		</svg>';
	}

	private function right_arrow()
	{
		echo '
		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
			<path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
		</svg>';
	}

	private function show_days()
	{
		echo '<div class="cal-days">';

		for ($i = 1; $i < $this->day_of_the_week_of_the_first_day_in_the_month; $i++) {
			echo '<button class="btn cal-btn" disabled="" type="button"></button>';
		}

		for ($i = 1; $i <= $this->daysInMonth; $i++) {
			//echo '<button class="btn cal-btn" type="button">' . $i . '</button>';
			$this->show_day($i);
		}

		echo '</div>';
	}

	private function show_day(int $day)
	{
		$date = new DateTime($this->currentYear . "-" . $this->currentMonth . "-" . $day . " 23:59:59", wp_timezone());
		$booking_url = '/' . CREATE_BOOKING_URL . '?jahr=' . $this->currentYear . '&monat=' . $this->currentMonth . '&tag=' . $day . '&item=' . $this->booking_item->item_tag;

		if ($this->now > $date) {
			echo '<button class="btn cal-btn" type="button" disabled>' . $day . '</button>';
		} else {
			$booking = Booking::get_booking_by_tag_and_date($this->booking_item->item_tag, $date);
			if (count($booking) > 0) {
				echo '<a class="btn cal-btn booked" href="' . $booking_url . '">' . $day . '</a>';
				return;
			}

			if ($this->date_is_reserved_for_regatta_bookings($date)) {
				echo '<a class="btn cal-btn regatta" href="' . $booking_url . '">' . $day . '</a>';
				return;
			}

			echo '<a class="btn cal-btn" href="' . $booking_url . '">' . $day . '</a>';
		}
	}

	private function date_is_reserved_for_regatta_bookings(DateTime $date)
	{
		$event = EventCalendar::get_event_for_date($date);
		$no_rules_apply_time = clone $date;
		$no_rules_apply_time->setTime(0, 0);
		$no_rules_apply_time = $no_rules_apply_time->sub(new DateInterval("PT" . $this->booking_item->no_rules_apply_deadline . "H"));
		if ($event != null) {
			if ($event->is_reserved_for_regatta_bookings($this->booking_item->item_tag) and $this->now < $no_rules_apply_time) {
				return true;
			}
		}
		return false;
	}

	private function navigation_menu()
	{
		$nextMonth = $this->currentMonth == 12 ? 1 : $this->currentMonth + 1;
		$nextYear = $this->currentMonth == 12 ? $this->currentYear + 1 : $this->currentYear;
		$preMonth = $this->currentMonth == 1 ? 12 : $this->currentMonth - 1;
		$preYear = $this->currentMonth == 1 ? $this->currentYear - 1 : $this->currentYear;

		$prev_month_url = '?monat=' . sprintf('%02d', $preMonth) . '&jahr=' . $preYear . '&item=' . $this->booking_item->item_tag;
		$next_month_url = '?monat=' . sprintf("%02d", $nextMonth) . '&jahr=' . $nextYear . '&item=' . $this->booking_item->item_tag;

?>
		<div class="cal-month">
			<a class="btn cal-nav" href="<?= $prev_month_url; ?>">
				<?= $this->left_arrow(); ?>
			</a>
			<strong class="cal-month-name"><?= $this->monthLabels[$this->currentMonth - 1] . " " . $this->currentYear; ?></strong>
			<a class="btn cal-nav" href="<?= $next_month_url; ?>">
				<?= $this->right_arrow(); ?>
			</a>
		</div>
		<div class="cal-weekdays text-muted">
			<div class="cal-weekday">Mo</div>
			<div class="cal-weekday">Di</div>
			<div class="cal-weekday">Mi</div>
			<div class="cal-weekday">Do</div>
			<div class="cal-weekday">Fr</div>
			<div class="cal-weekday">Sa</div>
			<div class="cal-weekday">So</div>
		</div>
<?php
	}

	private function calendar_CSS()
	{
		echo '<style>';
		include(__DIR__ . '/booking_calendar.css');
		echo '</style>';
	}

	private function days_in_month($month = null, $year = null)
	{
		if (null == ($year))
			$year =  date("Y", time());

		if (null == ($month))
			$month = date("m", time());

		return date('t', strtotime($year . '-' . $month . '-01'));
	}

	private function day_of_the_week_of_the_first_day_in_the_month()
	{
		$first_day_of_the_month = new DateTime($this->currentYear . "-" . $this->currentMonth . "-01", wp_timezone());
		return intval($first_day_of_the_month->format('N'));
	}
}
