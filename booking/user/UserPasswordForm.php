<?php
require_once(__DIR__ . "/BookingUser.php");

/**
 * @property string	$password_regex
 */

class UserPasswordForm extends BookingUser
{
	public function __construct($id)
	{
		parent::__construct($id);
		$this->password_regex = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/";
	}

	public function password_is_valid(string $password)
	{
		return preg_match($this->password_regex, $password);
	}

	public function set_password(string $password)
	{
		wp_set_password($password, $this->ID);
	}

	public function show_input_form()
	{
?>
		<div class="card col-12 col-lg-6 mb-3 mx-auto">
			<div class="mx-3 my-3">
				<form name="password_form" id="password_form" action="<?= site_url('/' . USER_ACCOUNT_URL); ?>" method="post">
					<div class="mb-3">
						<label for="new_password" class="form-label">Neues Passwort: </label>
						<input id="new_password" name="new_password" type="password" value="" oninput="updateNewPasswordCheck(); submitButtonUpdate();">
						<div class="invalid-feedback">
							Das Passwort muss mindestens 8 Zeichen, einen Großbuchstaben, einen Kleinbuchstaben, eine Ziffer und ein Sonderzeichen enthalten.
						</div>
					</div>
					<div class="mb-3">
						<label for="confirm_password" class="form-label">Passwort wiederholen: </label>
						<input id="confirm_password" name="confirm_password" type="password" value="" oninput="updateConfirmPasswordCheck(); submitButtonUpdate();">
						<div class="invalid-feedback">
							Die Passwörter stimmen nicht überein.
						</div>
					</div>
					<button id="new_password_submit_btn" class="btn btn-primary" type="submit">Speichern</button>
				</form>
			</div>
		</div>
		<script>
			var new_password = document.getElementById("new_password");
			var confirm_password = document.getElementById("confirm_password");
			var new_password_submit_btn = document.getElementById("new_password_submit_btn");


			function passwordIsValid() {
				var password_regex = <?= $this->password_regex ?>;
				return password_regex.test(new_password.value);
			}

			function confirmPasswordIsSame() {
				return new_password.value == confirm_password.value;
			}

			function updateNewPasswordCheck() {
				if (passwordIsValid()) {
					new_password.classList.remove("is-invalid");
					new_password.classList.add("is-valid")
				} else {
					new_password.classList.remove("is-valid");
					new_password.classList.add("is-invalid");
				}
			}

			function updateConfirmPasswordCheck() {
				if (confirmPasswordIsSame()) {
					confirm_password.classList.remove("is-invalid");
					confirm_password.classList.add("is-valid")
				} else {
					confirm_password.classList.remove("is-valid");
					confirm_password.classList.add("is-invalid");
				}
			}

			function submitButtonUpdate() {
				if (passwordIsValid() && confirmPasswordIsSame()) {
					new_password_submit_btn.disabled = false;
				} else {
					new_password_submit_btn.disabled = true;
				}
			}

			submitButtonUpdate();
		</script>
	<?php
	}

	public function show_update_confirmation()
	{
	?>
		<div class="text-center" id="booking">
			<h1>Passwort aktualisiert</h1>
			<p>Das Passwort wurde erfolgreich geändert. Du musst dich neu einloggen.</p>
			<a class="btn btn-primary" href="/<?= BOOKING_HOME_URL ?>">Neu einloggen</a>
		</div>';
<?php
	}
}
