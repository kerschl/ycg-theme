<?php
require_once(__DIR__ . "/../../../../../wp-includes/class-wp-user.php");

class BookingUser extends WP_User
{
	public function __construct($id)
	{
		parent::__construct($id);
	}

	public function set_user_role(string $role)
	{
		$role = ($role == BOOKING_USER_ROLE_TRAINER) ? BOOKING_USER_ROLE_TRAINER : BOOKING_USER_ROLE_NORMAL;
		update_user_meta($this->ID, BOOKING_USER_ROLE_META_KEY, $role);
	}

	public function get_user_role()
	{
		return get_the_author_meta(BOOKING_USER_ROLE_META_KEY, $this->ID);
	}

	public function set_booking_rate(string $booking_rate)
	{
		$booking_rate = ($booking_rate == BOOKING_RATE_YOUTH) ? BOOKING_RATE_YOUTH : BOOKING_RATE_ADULT;
		update_user_meta($this->ID, BOOKING_RATE_META_KEY, $booking_rate);
	}

	public function get_booking_rate()
	{
		return get_the_author_meta(BOOKING_RATE_META_KEY, $this->ID);
	}

	public function set_phone_number(string $phone_number)
	{
		update_user_meta($this->ID, BOOKING_USER_PHONE_NUMBER_META_KEY, $phone_number);
	}

	public function get_phone_number()
	{
		return get_the_author_meta(BOOKING_USER_PHONE_NUMBER_META_KEY, $this->ID);
	}

	public function set_item_access($item, String $access)
	{
		$access = ($access == BOOKING_ITEM_ACCESS_TRUE) ? BOOKING_ITEM_ACCESS_TRUE : BOOKING_ITEM_ACCESS_FALSE;
		update_user_meta($this->ID, $item, $access);
	}

	public function has_access_to_item($item)
	{
		return get_the_author_meta($item, $this->ID) == BOOKING_ITEM_ACCESS_TRUE;
	}
}
