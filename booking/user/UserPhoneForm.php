<?php
require_once(__DIR__ . "/BookingUser.php");

/**
 * @property string	$phone_regex
 */

class UserPhoneForm extends BookingUser
{
	public function __construct($id)
	{
		parent::__construct($id);
		$this->phone_regex = "/^[\+]?[0-9]{1,4}?[-\s\.]?[0-9]{3,4}?[-\s\.]?[0-9]{4,9}$/";
	}

	public function phone_number_is_valid(string $phone_number)
	{
		return preg_match($this->phone_regex, $phone_number);
	}

	public function show_input_form()
	{
?>
		<div class="card col-12 col-lg-6 mb-3 mx-auto">
			<div class="mx-3 my-3">
				<form name="phone_form" id="phone_form" action="<?= site_url('/' . USER_ACCOUNT_URL); ?>" method="post" class="needs-validation">
					<div class="mb-3">
						<label for="InputUsername" class="form-label">Telefonnummer: </label>
						<input class="" id="phone_number" name="phone_number" type="text" value="<?= $this->get_phone_number(); ?>" oninput="updatePhoneNumberCheck()">
						<div class="invalid-feedback">
							Telefonnummer bitte im Format +49 170 123456 angeben.
						</div>
					</div>
					<button id="phone_number_submit_btn" class="btn btn-primary" type="submit">Speichern</button>
				</form>
			</div>
		</div>
		<script>
			var phone_number = document.getElementById("phone_number");
			var phone_number_submit_btn = document.getElementById("phone_number_submit_btn");


			function phoneNumberIsValid() {
				var phone_regex = <?= $this->phone_regex ?>;
				return phone_regex.test(phone_number.value);
			}

			function updatePhoneNumberCheck() {
				if (phoneNumberIsValid()) {
					phone_number.classList.remove("is-invalid");
					phone_number.classList.add("is-valid")
					phone_number_submit_btn.disabled = false;
				} else {
					phone_number.classList.remove("is-valid");
					phone_number.classList.add("is-invalid");
					phone_number_submit_btn.disabled = true;
				}
			}
		</script>
	<?php
	}

	public function show_update_confirmation()
	{
	?>
		<div class="text-center" id="booking">
			<h1>Telefonnummer aktualisiert</h1>
			<p>Telefonnummer <?= $this->get_phone_number(); ?> wurde gespeichert</p>
			<a class="btn btn-primary" href="/<?= BOOKING_HOME_URL ?>">Zurück zum Buchungsportal</a>
		</div>
<?php
	}
}
