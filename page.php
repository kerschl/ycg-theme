<?php
/* Template for displaying pages */
get_header();
?>
<article>
	<?php
	while (have_posts()) : the_post();
		echo '<h1>';
		the_title();
		echo '</h1>';
		the_content();
	endwhile;
	?>
</article>
<?php get_footer();
