<?php
/* Template for displaying posts archive */
get_header();
?>
<article>
	<h1>Aktuelles</h1>
	<p>Aktuelle Informationen und Berichte über den Club und seine Aktivitäten.</p>
</article>
<div id="archive">
	<?php
	if (have_posts()) {
		echo '<div class="row">';
		while (have_posts()) {
			the_post();
			get_template_part('template-parts/posts/post-item');
		}
		echo '</div>';
	?>
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<?php

				global $wp_query;

				// Get the index of the current page
				$currentPage = max(1, get_query_var('paged', 1));

				// Get maximum number of pages
				$maxPageNum = $wp_query->max_num_pages;

				// Get array with all page index
				$pages = range(1, max(1, $wp_query->max_num_pages));

				// How much should the nav bar show into each direction
				$navBarOffset = 2;

				$navBarOffsetMin = $currentPage - $navBarOffset;
				$navBarOffsetMax = $currentPage + $navBarOffset;

				// Check for negative nav bar offset
				if ($navBarOffsetMin < 1) {
					$navBarOffsetMax = min(2 * $navBarOffset + 1, $maxPageNum);
					$navBarOffsetMin = 1;
				} elseif ($navBarOffsetMax > $maxPageNum) {
					$navBarOffsetMax = $maxPageNum;
					$navBarOffsetMin = max($maxPageNum - (2 * $navBarOffset), 1);
				}
				?>
				<li class="page-item <?= ($currentPage > 1) ? '' : 'disabled'; ?>">
					<a class="page-link" href="<?= ($currentPage > 1) ? esc_attr_e(get_pagenum_link($currentPage - 1)) : '#'; ?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
				</li>
				<?php
				if ($navBarOffsetMin > 1) {
					echo '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
				}

				foreach (range($navBarOffsetMin, $navBarOffsetMax) as $navItem) {
				?>
					<li class="page-item <?php if ($navItem == $currentPage) echo "active"; ?>"><a class="page-link" href="<?php esc_attr_e(get_pagenum_link($navItem)) ?>"><?= $navItem; ?></a></li>
				<?php
				}

				if ($navBarOffsetMax < $maxPageNum) {
					echo '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
				}
				?>
				<li class="page-item <?= ($currentPage < $maxPageNum) ? '' : 'disabled'; ?>">
					<a class="page-link" href="<?= ($currentPage < $maxPageNum) ? esc_attr_e(get_pagenum_link($currentPage + 1)) : '#'; ?>" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a>
				</li>
			</ul>
		</nav>
	<?php
	} else {
		get_template_part('template-parts/posts/post-no-posts');
	} ?>
</div>
<?php get_footer();
