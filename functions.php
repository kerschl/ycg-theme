<?php
/* Theme functions and definitions */
require_once(__DIR__ . "/booking/settings/BookingItemModel.php");
require_once(__DIR__ . "/booking/settings/UserMetaDataDialog.php");
require_once(__DIR__ . "/booking/settings/BookingConstants.php");
require_once(__DIR__ . "/booking/settings/UserBookingItemRestrictionsDialog.php");
require_once(__DIR__ . "/booking/settings/UserGeneralBookingRulesDialog.php");
require_once(__DIR__ . "/booking/setup/BookingSystemSetupSubMenu.php");
require_once(__DIR__ . "/booking/settings/BookingItemSettingsMenu.php");
require_once(__DIR__ . "/booking/settings/EventItemSettingsDialog.php");
require_once(__DIR__ . "/booking/admin-menu/CustomBookingCreatorMenu.php");
require_once(__DIR__ . "/booking/admin-menu/BookingsAdministrationMenu.php");
require_once(__DIR__ . "/booking/admin-menu/MaintenanceNotesAdministrationMenu.php");
require_once(__DIR__ . "/booking/admin-menu/BookingStatsAdminMenu.php");

// Hide adminbar to all exept user has manage_optionsBookingsMenu
if (!current_user_can('manage_options')) {
	add_filter('show_admin_bar', '__return_false');
}

// Menu walkers
function register_my_menus()
{
	class ycg_main_menu_walker extends Walker_Nav_menu
	{
		function start_lvl(&$output, $depth = 0, $args = null)
		{
			$output .= "\n<ul class=\"dropdown-menu dropdown-menu-end depth_$depth\" >\n";
		}
		function start_el(&$output, $item, $depth = 0, $args = null, $id = 0)
		{
			$classes = empty($item->classes) ? array() : (array) $item->classes;
			$classes[] = ($args->walker->has_children) ? 'dropdown' : '';
			$classes[] = 'nav-item';
			if ($depth && $args->walker->has_children) {
				$classes[] = 'dropdown-menu dropdown-menu-end';
			}

			$class_names =  join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
			$class_names = ' class="' . esc_attr($class_names) . '"';

			$id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
			$id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

			$output .= '<li ' . $id . $class_names .  '>';

			$active_class = ($item->current || $item->current_item_ancestor || in_array("current_page_parent", $item->classes, true) || in_array("current-post-ancestor", $item->classes, true)) ? 'active' : '';
			$nav_link_class = ($depth > 0) ? 'dropdown-item ' : 'nav-link ';
			$attributes = ($args->walker->has_children) ? ' class="' . $nav_link_class . $active_class . ' dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"' : ' class="' . $nav_link_class . $active_class . '"';

			$item_output = $args->before;
			$item_output .= '<a href="' . esc_attr($item->url) . '"' . $attributes . '>';
			$item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after . '</a>';
			$item_output .= $args->after;

			$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
		}
	}
	// register main menu
	register_nav_menus(array(
		'main-menu' => 'Main Menu',
	));
	class ycg_footer_menu_walker extends Walker_Nav_menu
	{
		function start_el(&$output, $item, $depth = 0, $args = null, $id = 0)
		{
			$title = $item->title;
			if ($item->url) {
				$permalink = $item->url;
			};
			$output .= "<li class='nav-item'><a class='nav-link px-2 py-0 text-white' href='" . $permalink . "'>" . $title . '</a>';
		}
	}
	// register footer menu
	register_nav_menus(array(
		'footer-menu' => 'Footer Menu',
	));
}
add_action('init', 'register_my_menus');

// Adds classes to the <body> tag
function custom_body_classes($classes)
{
	$classes[] = 'd-flex flex-column min-vh-100';
	return $classes;
}
add_filter('body_class', 'custom_body_classes');

// Post thumbnails
function ycg_theme_features()
{
	add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'ycg_theme_features');
add_image_size('post-thumb', 300, 180, true);

// Custom post types
function ycg_post_types()
{
	register_post_type('event', array(
		'rewrite' => array('slug' => 'veranstaltungen'),
		'has_archive' => true,
		'public' => true,
		'show_in_rest' => true,
		'labels' => array(
			'name' => 'Veranstaltungen',
			'add_new_item' => 'Neue Veranstaltung erstellen',
			'edit_item' => 'Veranstaltung bearbeiten',
			'singular_event' => 'Veranstaltung'
		),
		'menu_icon' => 'dashicons-calendar',
	));
	register_post_type('gallery', array(
		'supports' => array('title', 'editor'),
		'rewrite' => array('slug' => 'galerie'),
		'has_archive' => true,
		'public' => true,
		'show_in_rest' => true,
		'labels' => array(
			'name' => 'Galerie',
			'add_new_item' => 'Neue Galerie erstellen',
			'edit_item' => 'Galerie bearbeiten',
			'singular_event' => 'Galerie'
		),
		'menu_icon' => 'dashicons-format-gallery',
	));
	register_post_type('slide', array(
		'rewrite' => array('slug' => 'slide'),
		'has_archive' => false,
		'public' => false,
		'show_in_rest' => false,
		'show_ui' => true,
		'labels' => array(
			'name' => 'Slides',
			'add_new_item' => 'Neuen Slide erstellen',
			'edit_item' => 'Slide bearbeiten',
			'singular_event' => 'Slide'
		),
		'menu_icon' => 'dashicons-slides',
	));
	register_post_type('result', array(
		'rewrite' => array('slug' => 'regatta-ergebnisse'),
		'has_archive' => true,
		'public' => true,
		'show_in_rest' => true,
		'labels' => array(
			'name' => 'Regatta Ergebnisse',
			'add_new_item' => 'Neues Regatta Ergebnis erstellen',
			'edit_item' => 'Regatta Ergebnis bearbeiten',
			'singular_event' => 'Regatta Ergebnis'
		),
		'menu_icon' => 'dashicons-media-spreadsheet',
	));
}
add_action('init', 'ycg_post_types');

// Ical event
function create_ical_event($post_id)
{
	// If not event, exit
	if (get_post_type($post_id) != 'event') {
		return;
	}
	// Get event post
	$event = get_post($post_id);
	// Get event category, date and extern status
	$ev_type_obj = get_field('veranstaltungskategorie', $post_id);
	$ev_date_field = get_field('veranstaltungsdatum', $post_id);
	$ev_is_external = get_field('veranstaltungextern', $post_id);
	if (!(isset($ev_type_obj) && isset($ev_date_field) && isset($ev_is_external))) {
		error_log("Event ID: " . $post_id);
		error_log("ev_type_obj: " . json_encode($ev_type_obj));
		error_log("ev_date_field: " . $ev_date_field);
		error_log("ev_is_external: " . var_export($ev_is_external, true));
		return;
	}

	$ev_type_value = $ev_type_obj['value'];
	//error_log("ev_type_value: " . $ev_type_value);
	$ev_type_label = $ev_type_obj['label'];
	//error_log("ev_type_label: " . $ev_type_label);
	$ev_date = new DateTime($ev_date_field);
	$evstr_start = $ev_date->format('Ymd');
	//error_log("evstr_start: " . $evstr_start);
	$ev_date->modify('+1 day');
	$evstr_end = $ev_date->format('Ymd');
	//error_log("evstr_end: " . $evstr_end);
	$evstr_title = html_entity_decode($event->post_title, ENT_COMPAT, 'UTF-8');
	//error_log("evstr_title: " . $evstr_title);
	$evstr_description = html_entity_decode(esc_html($ev_type_label), ENT_COMPAT, 'UTF-8');
	//error_log("evstr_description: " . $evstr_description);
	$evstr_url = get_permalink($post_id);
	//error_log("evstr_url: " . $evstr_url);
	$ev_folder_path = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/events/';
	$ev_folder_path_ics = $ev_folder_path . "ics/";
	if (!file_exists($ev_folder_path_ics)) {
		mkdir($ev_folder_path_ics, 0770, true);
	}
	$ev_file_path = $ev_folder_path_ics . $post_id . '.ics';
	//error_log("ev_file_path: " . $ev_file_path);
	$ev_uid = preg_replace('/[^a-zA-Z0-9]/', '-', $evstr_title) . "-" . $evstr_start . "-" . $evstr_end;
	//error_log("ev_uid: " . $ev_uid);
	if ($ev_is_external) {
		$ev_color = "khaki"; // nextcloud
	} else {
		$ev_color = "steelblue"; // nextcloud
	}
	//error_log("ev_color: " . $ev_color);

	$ical = fopen($ev_file_path, 'w') or die('Datei ' . $ev_file_path . ' kann nicht gespeichert werden!');
	$ics_content =
		"BEGIN:VEVENT\r\n" .
		"SEQUENCE:3\r\n" .
		"DTSTART;VALUE=DATE:" . $evstr_start . "\r\n" .
		"DTEND;VALUE=DATE:" . $evstr_end . "\r\n" .
		"STATUS:CONFIRMED\r\n" .
		"SUMMARY:" . $evstr_title . "\r\n" .
		"URL;VALUE=URI:" . $evstr_url . "\r\n" .
		"DESCRIPTION:" . $evstr_title . " - " . $evstr_description . "\\nVeranstaltungs-Kalender Yachtclub-Gollenshausen e.V.\\n" . $evstr_url . "\r\n" .
		"CATEGORIES:Yachtclub-Gollenshausen e.V.\r\n" .
		"UID:" . $ev_uid . "\r\n" .
		"COLOR:" . $ev_color . "\r\n" .
		"END:VEVENT\r\n";
	fwrite($ical, $ics_content);
	fclose($ical);
	compile_ical_event();
}
function compile_ical_event()
{
	// Compile events into ics file
	$calendar = fopen($_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/events/calendar.ics', 'w');
	$ical_begin = "BEGIN:VCALENDAR\r\n" .
		"VERSION:2.0\r\n" .
		"PRODID:-//Yachtclub Gollenshausen e.V.//yachtclub-gollenshausen.de//DE\r\n" .
		"CALSCALE:GREGORIAN\r\n";
	$ical_end = "END:VCALENDAR";
	fwrite($calendar, $ical_begin);
	if ($folder = opendir($_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/events/ics/')) {
		while (false !== ($file = readdir($folder))) {
			if ($file != "." && $file != "..") { // Skip directories
				$filePath = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/events/ics/' . $file;
				if (is_file($filePath)) {
					$content = file_get_contents($filePath);
					fwrite($calendar, $content);
				}
			}
		}
		closedir($folder);
	}
	fwrite($calendar, $ical_end);
	fclose($calendar);
}
add_action('acf/save_post', 'create_ical_event', 10, 3);
function delete_ical_event($post_id)
{
	$ics_file = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/events/ics/" . $post_id . ".ics";
	if (is_file($ics_file)) {
		unlink($ics_file);
	}
	compile_ical_event();
}
add_action('wp_trash_post', 'delete_ical_event');

// Removes Gutenberg blocks stylesheet
function wpassist_remove_block_library_css()
{
	wp_dequeue_style('wp-block-library');
}
add_action('wp_enqueue_scripts', 'wpassist_remove_block_library_css');

// Removes wp-emoji js
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Force enable sitemap (/wp-sitemap.xml)
add_filter('wp_sitemaps_enabled', '__return_true');
// Sitemap disable users and taxonomies
add_filter('wp_sitemaps_add_provider', 'kama_remove_sitemap_provider', 10, 2);
function kama_remove_sitemap_provider($provider, $name)
{
	$remove_providers = ['users', 'taxonomies'];
	// disabling users archives
	if (in_array($name, $remove_providers)) {
		return false;
	}
	return $provider;
}

// Menus for administration of bookings of individual users
new UserGeneralBookingRulesDialog();
new UserBookingItemRestrictionsDialog();

// Admin menus for booking system administration
new BookingsAdministrationMenu();
new MaintenanceNotesAdministrationMenu();
new BookingItemSettingsMenu();
new CustomBookingCreatorMenu();
new BookingSystemSetupSubMenu();
new BookingStatsAdminMenu();

// Menu for blocking booking items for events
new EventItemSettingsDialog();
