<?php
/* Template for displaying galleries archive */
get_header();
?>
<article>
<?php
		$gallery_page = get_page_by_path( "galerie");
		$post = get_post($gallery_page->ID);
		echo "<h1>" . apply_filters('the_title', $post->post_title) . "</h1>";
		echo apply_filters('the_content', $post->post_content);
		?>
</article>
<div id="archive">
	<div class="accordion" id="accordion">
		<?php
		$oldest_year = 2010;
		$current_year = strval(date("Y"));
		$accordion_counter = 0;
		while ($current_year >= $oldest_year) {
			$query_args = array(
				'posts_per_page' => -1,
				'date_query' => array('year'  => $current_year),
				'post_type' => 'gallery',
				'order' => 'DESC'
			);
			$gallery = new WP_Query($query_args);
			if ($gallery->found_posts > 0) {
				$accordion_counter += 1;
				get_template_part(
					'template-parts/accordion/accordion-start',
					null,
					array(
						'accordion_counter' => $accordion_counter,
						'current_year' => $current_year,
						'accord_is_open' => (1 === $accordion_counter)
					)
				);
				while ($gallery->have_posts()) {
					$gallery->the_post();
					get_template_part('template-parts/gallery/gallery-item');
				}
				get_template_part('template-parts/accordion/accordion-end');
			}
			$current_year -= 1;
			wp_reset_query();
		}
		if (0 === $accordion_counter) {
			get_template_part('template-parts/gallery/gallery-no-galleries');
		}
		?>
	</div>
</div>
<?php get_footer();
