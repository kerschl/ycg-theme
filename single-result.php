<?php
/* Template for displaying single results */
get_header();
while (have_posts()) : the_post();
	get_template_part('template-parts/results/results-single');
endwhile;
get_footer();
