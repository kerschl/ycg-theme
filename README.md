# YCG Wordpress Theme

Das Wordpress Theme das auf der aktuellen Website des Yachtclub Gollenshausen verwendet wird.

## Plugins:

-   [Advanced Custom Fields](https://wordpress.org/plugins/advanced-custom-fields/)
-   [Admin Columns for ACF Fields](https://wordpress.org/plugins/admin-columns-for-acf-fields/)
-   [Classic Editor](https://wordpress.org/plugins/classic-editor/)
-	[Admin Menu Editor](https://wordpress.org/plugins/admin-menu-editor/)
-	[Post Duplicator](https://de.wordpress.org/plugins/post-duplicator/)

## Settings:

-   Einstellungen > Medien > Vorschaubilder > [false] Das Vorschaubild auf die exakte Größe beschneiden
-   Einstellungen > Permalinks > Monat und Name
-   Einstellungen > Lesen > Blogseiten zeigen maximal > 9 (oder andere durch drei teilbare natürliche Zahl)

## Booking System:

Das Buchungssystem kann mithilfe des Automaten im Backend aufgesetzt werden. Zu finden unter: `Buchungen > Setup` 

## Used in Project:

-   Bootstrap
-   Photoswipe
-   Imagesloaded
-   Masonry Layout
