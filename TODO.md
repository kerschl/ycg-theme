# YCG Website ToDo's

Startseite

-   [x] Liste mit neuesten Einträgen in Galerie
-   [x] Slider im backend sichtbar, aber nicht in sitemap

Aktuelles

-   [x] Post-Bild für Card und Single Post
-   [x] Schönere Angabe von Datum und Autor
-   [x] Einstellung für angezeigte Posts in Readme hinzufügen

Veranstaltungen

-   [x] Standardmäßig nur bevorstehende Events anzeigen
-   [x] Meldung wenn kein Event vorhanden
-   [x] Durch zusätzlichen Text auf externes Event hinweisen
-   [x] Bilder für Kategorien in Assets verschieben
-   [x] Single Event aufhübschen
-   [x] Event-Bilder sollen sich über volle Breite erstrecken
-   [x] Errorhandling für ACF Felder
-   [x] Datumsangaben auf Deutsch
-   [x] Script zum Sortieren der Events wird nur für Event Archiv geladen
-   [x] Breite anpassen, sodass alle Event Items gleich aussehen + Testen (Windows,Mac)
-   [ ] Unterstützung für Mehrtägige Veranstaltungen
-   [ ] Unterteilung nach Monaten

Regatta

-   [x] Content Seite pflegen
-   [ ] Integration von mangage2sail
-	[ ] YCG Regatten auf Anmeldeseite aufführen
-   [x] Neuer Posttype Ergebnis
-   [x] Ergebnis-Posts können mit Aktuellen Posts verlinkt werden
-   [x] Ergebnisse unterteilen nach Jahren
-   [x] Ergebnisse importieren aus CSV Dateien aus Velum

Galerie

-   [x] Ordnerstruktur für Bilder
-   [x] Auflistung im Archiv unterteilt nach Jahren (Acordeons)
-   [x] Script zum konvertieren, umbenennen und entfernen von Metadaten von Bildern
-   [x] Galerie unterteilen nach Jahren
-	[x] Suckless umsetzung ohne langsame Datenbank abfragen

Jugend

-	[x] Content Seite pflegen
-	[x] Neuer Content

Mitglieder

-   [x] Mitglied werden > Content Seite pflegen

Buchungssystem

-   [x] Eigenes Buchungssystem mit WP Integration
-   [x] Online Logbuch
-	[X] Termine an denen Regatten stattfinden im Kalender farblich kennzeichnen (Option für YCG Regatten und extra für externe Regatten)
-	[X] Für reservierte Termine Buchungsanmerkungen anzeigen
-	[ ] Abrechnung als PDF ausgeben
-	[X] Buchungsbeschränkungen implementieren
-	[X] Gruppierung von Logbuch und Kalender nach Booten
-	[X] Menü um Passwort zu ändern
-	[X] Menü um Telefonnummer zu speichern
-	[X] Beschränkung: ohne hinterlegte Telefonnummer keine Buchung
-	[X] Beschränkung: mit nicht ausgefüllten Logbucheinträgen keine neue Buchung
-	[ ] E-Mail Bestätigung
-	[X] Wartungsnotizen in Logbüchern
-	[ ] In Buchungstabelle booking_start_time und booking_end_time entfernen
-	[ ] andere Preise fürs Wochenende
- 	[ ] Bilder zu Logbuch und Wartungsnotizen hinzufügen
- 	[ ] Bessere Buchungsabrechnung

Über Uns

-   [ ] Unsere Boote > Neuer Content
-   [x] Chronik > Content Seite pflegen
-   [x] Vorstand > Content Seite pflegen
-   [ ] Vorstand > Extra Post type für Vorstandsmitglieder
-   [x] Links > Content Seite pflegen
-   [x] Kontakt > Form
-   [x] Anfahrt > Map mit Marker 

Datenschutz/Impressum

-   [x] Impressum > Content Seite pflegen
-   [x] Datenschutz > Content Seite pflegen
-   [ ] Neue Texte?
