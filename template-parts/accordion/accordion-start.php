<?php
/* Accordion start markup */
?>
<div class="accordion-item">
	<h2 class="accordion-header" id="heading<?= $args['accordion_counter'] ?>">
		<?php
		if ($args['accord_is_open']) {
			echo '<button class="accordion-button fs-4" type="button" data-bs-toggle="collapse" data-bs-target="#collapse' . $args['accordion_counter'] . '" aria-expanded="true" aria-controls="collapse' . $args['accordion_counter'] . '">';
		} else {
			echo '<button class="accordion-button collapsed fs-4" type="button" data-bs-toggle="collapse" data-bs-target="#collapse' . $args['accordion_counter'] . '" aria-expanded="true" aria-controls="collapse' . $args['accordion_counter'] . '">';
		}
		echo '' . $args['current_year'] . '</button>';
		?>
	</h2>
	<?php
	if ($args['accord_is_open']) {
		echo '<div id="collapse' . $args['accordion_counter'] . '" class="accordion-collapse collapse show" aria-labelledby="heading' . $args['accordion_counter'] . '" data-bs-parent="#accordion">';
	} else {
		echo '<div id="collapse' . $args['accordion_counter'] . '" class="accordion-collapse collapse" aria-labelledby="heading' . $args['accordion_counter'] . '" data-bs-parent="#accordion">';
	}
	?>
	<div class="accordion-body">
		<div class="list-group list-group-flush">
