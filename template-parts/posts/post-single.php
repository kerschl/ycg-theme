<?php
/* Template for displaying a single post */
?>
<div class="container-md px-0 post-img">
	<img class="img-fluid w-100" src="<?= the_post_thumbnail_url(); ?>" alt="post img">
</div>
<article id="post-<?php the_ID(); ?>">
	<h1><?php the_title(); ?></h1>
	<div class="d-flex align-items-start">
		<span class="blog-post-meta text-muted me-auto">
			Veröffentlicht von <?php the_author(); ?> am <?php the_time('j. F Y'); ?>
		</span>
		<?php get_template_part('template-parts/buttons/share-button') ?>
	</div>
	<hr>
	<?php the_content() ?>
	<hr>
	<?php
	if ($relatedResult = get_field('result')) {
		echo '<p class="blog-post-meta text-muted">Regatta Ergebnis: ';
		foreach ($relatedResult as $result) {
			echo '<br class="d-sm-none"><a href="' . get_the_permalink($result) . '">' . get_the_title($result) . '</a> <small>&#128279;</small> ';
		}
		echo '</p>';
	}

	if ($relatedGallery = get_field('galerie')) {
		echo '<p class="blog-post-meta text-muted">Bildergalerie: ';
		foreach ($relatedGallery as $gallery) {
			echo '<br class="d-sm-none"><a href="' . get_the_permalink($gallery) . '">' . get_the_title($gallery) . '</a> <small>&#128279;</small> ';
		}
		echo '</p>';
	}

	get_template_part('template-parts/buttons/edit-button');
	?>
</article>
