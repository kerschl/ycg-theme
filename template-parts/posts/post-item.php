<?php
/* Single post-item markup */
?>
<div class="col-12 col-lg-4">
	<div class="card mb-3">
		<img class="card-img-top w-100" alt="post img" src="<?php
															echo the_post_thumbnail_url($size = 'post-thumb');
															?>">
		<div class="card-body">
			<h5 class="card-title"><?php the_title(); ?></h5>
			<p class="mb-1 small text-muted">Von <?php the_author(); ?> am <?php the_time('j. F Y'); ?></p>
			<p class="card-text"><?= wp_trim_words(get_the_content(), 16, " ..."); ?></p>
		</div>
		<div class="card-footer bg-white p-3 pt-0 border-top-0">
			<a class="btn btn-primary" href="<?php the_permalink(); ?>" role="button">Weiterlesen</a>
		</div>
	</div>
</div>
