<?php
/*
Taken from: https://wordpress.stackexchange.com/questions/82407/custom-login-form
*/
?>

<article>
	<h1>Bootsbuchung</h1>
	<p>Wenn Sie noch kein Login für unser Buchungssystem haben, kontaktieren Sie uns unter <a href="mailto:buchung@yachtclub-gollenshausen.de">buchung@yachtclub-gollenshausen.de</a>.</p>
</article>
<div id="booking">
	<div class="card col-12 col-lg-4 mb-3 mx-auto">
		<div class="mx-3 my-3">
			<form name="loginform" id="loginform" action="<?= site_url('/wp-login.php'); ?>" method="post">
				<div class="mb-3">
					<label for="InputUsername" class="form-label">Benutzername: </label>
					<input id="user_login" type="text" size="20" value="" name="log" class="form-control">
				</div>
				<div class="mb-3">
					<label for="InputPassword" class="form-label">Passwort</label>
					<input id="user_pass" type="password" size="20" value="" name="pwd">
				</div>
				<div class="mb-3 form-check">
					<input id="rememberme" class="form-check-input" type="checkbox" value="forever" name="rememberme">
					<label class="form-check-label" for="rememberme">Eingeloggt bleiben</label>
				</div>
				<p><input id="wp-submit" type="submit" value="Login" name="wp-submit"></p>
				<input type="hidden" value="<?= esc_attr($_SERVER['REQUEST_URI']); ?>" name="redirect_to">
			</form>
		</div>
	</div>
</div>
