<?php
/* Single carousel-item markup */
$url = get_field("slider_bild");
?>
<div class="carousel-item <?php if ($args['slide_count'] == 0) {
								echo "active";
							} ?>">
	<img class="d-block w-100" src="<?= $url . '" alt="' . $url ?>">
	<div class="container">
		<div class="carousel-caption text-start">
			<h2 class="mt-3 mb-0"><?php the_title(); ?></h2>
			<?php the_content(); ?>
			<p class="my-3">
				<a class="btn btn-primary" href="<?php the_field("button_link"); ?>"><?php the_field("button_text"); ?></a>
			</p>
		</div>
	</div>
</div>
