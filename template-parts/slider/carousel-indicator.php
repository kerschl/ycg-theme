<?php
/* Single carousel-indicator markup */
echo '<button type="button" data-bs-target="#FrontPageCarousel" data-bs-slide-to="' .
	$args['slide_count'] .
	'" class="' .
	(($args['slide_count'] == 0) ? 'active' : '') .
	'" aria-label="Slide ' .
	($args['slide_count'] + 1) .
	'"></button>';
