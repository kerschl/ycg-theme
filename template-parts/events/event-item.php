<?php
/* Single event-item markup */
$event_date = new DateTime(get_field("veranstaltungsdatum"));
$event_date_str = strtotime(get_field("veranstaltungsdatum"));
$event_start = new DateTime(get_field("veranstaltungsuhrzeit"));
$event_type_obj = get_field('veranstaltungskategorie');
$today = new DateTime('NOW');
$today->sub(new DateInterval('PT21H59M59S'));
?>
<div class="event-item <?= the_field('veranstaltungextern') . ' ' . $event_type_obj['value'] . (($event_date <= $today) ? ' past-event d-none' : '') ?>">
	<a class="d-flex text-decoration-none border rounded overflow-hidden mb-3" data-bs-toggle="collapse" href="#event-<?php the_ID() ?>" aria-expanded="false" aria-controls="event-<?php the_ID() ?>">
		<div class="<?= $event_date <= $today ? 'bg-secondary' : (get_field('veranstaltungextern') ? 'bg-warning' : 'bg-primary'); ?> position-relative">
			<div class="h-100 d-flex align-items-center justify-content-center flex-column">
				<div class="fs-1 fw-light lh-1 text-white">
					<?= $event_date->format("d"); ?>
				</div>
				<div class="fs-5 fw-normal lh-1 text-white text-uppercase">
					<?= date_i18n("M", $event_date_str); ?>
				</div>
			</div>
			<h6 class="h-0">——————</h6>
		</div>
		<div class="p-3 text-black d-flex align-items-start flex-column">
			<h4 class="mb-2"><?php the_title(); ?></h4>
			<p class="mb-0">
				<?= $event_type_obj['label'] . (get_field('veranstaltungextern') ? ' - Extern' : ''); ?>
			</p>
		</div>
	</a>
	<?php
	$eventdetails = apply_filters('the_content', get_the_content());
	if (get_field('veranstaltungextern')) {
		$eventdetails = '<p><strong>Achtung:</strong> Diese Veranstaltung wird nicht vom Yachtclub Gollenshausen e.V. organisiert. Sie ist jedoch in unserem Veranstaltungskalender aufgeführt, um eine bessere Übersicht über Veranstaltungen am Chiemsee zu ermöglichen.</p>' . $eventdetails;
	}
	if (get_field("ausschreibung") || get_field("anmeldung")) {
		$eventdetails = $eventdetails . '<p>';
		if (get_field("ausschreibung")) {
			$eventdetails = $eventdetails . '<a class="btn border text-decoration-none mt-1 me-2" href="' .
			get_field("ausschreibung") . '" target="_blank" title="Ausschreibung herunterladen">' .
			file_get_contents(get_template_directory() . "/assets/icons/clip.svg") .
			' Ausschreibung herunterladen</a>';
		}
		if (get_field("anmeldung")) {
			$eventdetails = $eventdetails . '<a class="btn border text-decoration-none mt-1 me-2" href="' .
			get_field("anmeldung") . '" target="_blank" title="Anmeldung herunterladen">' .
			file_get_contents(get_template_directory() . "/assets/icons/clip.svg") .
			' Anmeldung herunterladen</a>';
		}
		$eventdetails = $eventdetails . '</p>';
	}
	if ($eventdetails != "") {
	?>
		<div class="event-details collapse" id="event-<?php the_ID() ?>">
			<div class="card card-body mb-3">
				<?php
				echo $eventdetails;
				get_template_part('template-parts/buttons/edit-button');
				?>
			</div>
		</div>
	<?php
	}
	?>
</div>
