<?php
/* Template part for displaying a single event */
$event_date = new DateTime(get_field("veranstaltungsdatum"));
$event_date_str = strtotime(get_field("veranstaltungsdatum"));

$event_type_obj = get_field('veranstaltungskategorie');
$event_img = 'src="' . get_bloginfo('template_directory') . '/assets/events/default_images/category_' . $event_type_obj['value'] . '.jpg"';
$event_img_alt = 'alt=category_' . $event_type_obj['value'] . '_image"';
//$event_ics_href = 'href="/wp-content/uploads/events/ics/' . $event_date->format("Y") . '/' . basename(get_permalink()) . '-' . $event_date->format("Ymd") . '.ics"';
$ev_start = new DateTime(get_field("veranstaltungsuhrzeit"));
?>

<div class="container-md px-0 post-img">
	<img class="img-fluid w-100" <?= $event_img . ' ' . $event_img_alt; ?>>
</div>
<article id="event-<?php the_ID(); ?>">
	<h1><?php the_title() ?></h1>
	<div class="d-flex align-items-start">
		<span class="blog-post-meta text-muted me-auto">
			<?= date_i18n("j. F Y", $event_date_str) . ' ab ' . $ev_start->format('H') . ' Uhr - ' . $event_type_obj['label'] ?>
		</span>
		<?php get_template_part('template-parts/buttons/share-button') ?>
	</div>
	<hr>
	<?php
	if (get_field('Veranstaltungextern')) {
		echo '<p><strong>Achtung:</strong> Diese Veranstaltung wird nicht vom Yachtclub Gollenshausen e.V. ausgerichtet. Sie ist lediglich im Veranstaltungskalender aufgeführt um unseren Mitgliedern eine bessere Übersicht über die Veranstaltungen am Chiemsee zu bieten.</p>';
	}
	?>
	<?php the_content();
	/**<a class="btn btn-primary me-2 mb-3" <?= $event_ics_href ?> title="Termin speichern">
		Termin in Kalender speichern
	</a>*/
	?>
	<?php if (get_field("ausschreibung")) { ?>
		<a class="btn btn-primary me-2 mb-3" href="<?php the_field("ausschreibung") ?>" target="_blank" title="Ausschreibung herunterladen">Ausschreibung herunterladen</a>
	<?php }
	if (get_field("anmeldung")) { ?>
		<a class="btn btn-primary mb-3" href="<?php the_field("anmeldung") ?>" target="_blank" title="Anmeldung herunterladen">Anmeldung herunterladen</a>
	<?php }
	if (get_field("manage2sail")) { ?>
		<a class="btn btn-warning mb-3" href="<?php the_field("manage2sail") ?>" target="_blank" title="Zu Manage2Sail">Zur Manage2Sail Anmeldung</a>
	<?php } ?>
	<?php get_template_part('template-parts/buttons/edit-button'); ?>
</article>
