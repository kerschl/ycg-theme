<?php
/* Template for displaying a single result */
?>
<article id="result-<?php the_ID() ?>">
	<h1>Ergebnisse - <?php the_title() ?></h1>
	<?php
	the_content();

	$csv_path = get_attached_file(get_field('csv_upload'));
	if (file_exists($csv_path)) {
		$handle = fopen($csv_path, "r") or die('CSV Datei konnte nicht geöffnet werden');

		if ($handle !== FALSE) {

			// If Split up into two groups is wanted
			if (get_field('yardstick_grenze_fur_gruppeneinteilung') != 0) {
				// Gruppe 1
				echo '<h4 class="">Gruppe 1</h4>';
				get_template_part('template-parts/results/results-table-head');

				$row = 1;
				$platz_counter = 1;

				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

					// Check if the first entry is a number and yardstick is below threshold
					if (is_numeric($data[0]) && intval($data[6]) <=  get_field('yardstick_grenze_fur_gruppeneinteilung')) {
						get_template_part('template-parts/results/results-table-row', null, array('platz_counter' => $platz_counter, 'data' => $data));
						$platz_counter++;
					}
					$row++;
				}
				get_template_part('template-parts/results/results-table-end');

				// Gruppe 2
				echo '<h4 class="">Gruppe 2</h4>';
				get_template_part('template-parts/results/results-table-head');

				rewind($handle);
				$row = 1;
				$platz_counter = 1;

				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

					// Check if the first entry is a number nd yardstick is above threshold
					if (is_numeric($data[0]) && intval($data[6]) >  get_field('yardstick_grenze_fur_gruppeneinteilung')) {
						get_template_part('template-parts/results/results-table-row', null, array('platz_counter' => $platz_counter, 'data' => $data));
						$platz_counter++;
					}
					$row++;
				}
				get_template_part('template-parts/results/results-table-end');
			}

			// Check if a total ranking should be displayed
			if (get_field('gesamtwertung')) {
				// Gesamtwertung
				echo '<h3 class="">Gesamtwertung</h3>';
				get_template_part('template-parts/results/results-table-head');

				rewind($handle);
				$row = 1;
				$platz_counter = 1;

				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

					// Check if the first entry is a number
					if (is_numeric($data[0]) && intval($data[6])) {
						get_template_part('template-parts/results/results-table-row', null, array('platz_counter' => $platz_counter, 'data' => $data));
						$platz_counter++;
					}
					$row++;
				}
				get_template_part('template-parts/results/results-table-end');
			}
			fclose($handle);
		}
	} else {
		echo '<p class="text-center">Eine Webansicht dieser Regattaergebnisse ist leider nicht möglich</p>';
	}

	get_template_part('template-parts/buttons/edit-button');
	?>
</article>
