<?php
// Start new row entry
// This fixes UTF-8 encoding if default_charset returns iso-8859-x (Strato?)
// This file has to be renamed
echo '<tr>' .
	'<th>' . utf8_encode($args['platz_counter']) . "</th>\n" . // Platzierung
	'<td>' . utf8_encode($args['data'][2]) . "</td>\n" . // Skipper
	'<td>' . utf8_encode($args['data'][1]) . "</td>\n" . // Segelnummer
	'<td>' . utf8_encode($args['data'][3]) . "</td>\n" . // Crew
	'<td>' . utf8_encode($args['data'][4]) . "</td>\n" . // Bootsklasse
	'<td>' . utf8_encode($args['data'][5]) . "</td>\n" . // Club
	'<td>' . utf8_encode($args['data'][6]) . "</td>\n" . // Yardstick
	'<td>' . utf8_encode($args['data'][7]) . "</td>\n" . // Gesegelte Zeit
	'<td>' . utf8_encode($args['data'][8]) . "</td>\n" . // Berechnete Zeit
	'</tr>';// End row entry
