<?php
/* Single gallery-item markup */
?>
<li class="list-group-item list-group-item-action d-inline-flex">
	<h5 class="m-0 me-auto"><?php the_title(); ?></h5>
	<?php if (get_field('csv_upload')) { ?>
		<div><a class="btn btn-primary btn-sm mx-2" href="<?php the_permalink() ?>">WEB</a></div>
	<?php } ?>
	<div>
		<a class="btn btn-primary btn-sm ms-2" href="<?php the_field('pdf_upload') ?>" target="_blank">PDF</a>
	</div>
</li>
