<?php
// Start new row entry
echo '<tr>' .
	'<th>' . $args['platz_counter'] . "</th>\n" . // Platzierung
	'<td>' . $args['data'][2] . "</td>\n" . // Skipper
	'<td>' . $args['data'][1] . "</td>\n" . // Segelnummer
	'<td>' . $args['data'][3] . "</td>\n" . // Crew
	'<td>' . $args['data'][4] . "</td>\n" . // Bootsklasse
	'<td>' . $args['data'][5] . "</td>\n" . // Club
	'<td>' . $args['data'][6] . "</td>\n" . // Yardstick
	'<td>' . $args['data'][7] . "</td>\n" . // Gesegelte Zeit
	'<td>' . $args['data'][8] . "</td>\n" . // Berechnete Zeit
	'</tr>';// End row entry
