<?php
/* Template for displaying the logout button */
if (is_user_logged_in()) {
	echo '<a id="logout" class="btn btn-outline-warning btn-sm my-1 ms-1" href="' . wp_logout_url($redirect = '/buchung') . '">Ausloggen</a>';
}
