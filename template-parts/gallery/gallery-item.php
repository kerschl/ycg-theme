<?php
/* Single gallery-item markup */
?>
<a class="list-group-item list-group-item-action d-inline-flex" href="<?php the_permalink() ?>">
	<h5 class="m-0 me-auto"><?php the_title(); ?></h5>
	<?php the_time('j. F Y'); ?>
</a>
