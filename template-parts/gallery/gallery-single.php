<?php
/* Template part for displaying a single post */
?>
<article>
	<h1 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<div class="row row-cols-3 row-cols-md-4 row-cols-lg-6 pswp-gallery pswp-gallery--single-column" id="gallery">
		<?php
		$webpath = '/wp-content/uploads/galerie/' . get_field("gallery_path") . "/";
		$absolutepath = $_SERVER['DOCUMENT_ROOT'] . $webpath;
		class Image
		{
			public $file;
			public $width;
			public $height;
		}
		$images = [];
		if (is_dir($absolutepath)) {
			$handle = opendir($absolutepath);
			while (($file = readdir($handle)) !== false) {
				list($width, $height) = getimagesize($absolutepath . $file);
				if (($file == '.') || ($file == '..') || ($file == 'thumbnails') || ($file == 'originals') || ($file == 'videos')) {
					continue;
				}
				list($width, $height) = getimagesize($absolutepath . $file);
				$obj = new Image();
				$obj->file = $file;
				$obj->width = $width;
				$obj->height = $height;
				array_push($images, $obj);
			}
			closedir($handle);
		}
		asort($images); //sort images
		foreach ($images as $image) {
			echo '
			<div class="col px-2 pb-3">
				<a class="card overflow-hidden" href="' . $webpath . $image->file . '" data-pswp-width="' . $image->width . '" data-pswp-height="' . $image->height . '" data-cropped="true" target="_blank">
					<img class="w-100" src="' . $webpath . "thumbnails/" . $image->file . '" alt="' . $image->file . '" title="' . $image->file . '" />
				</a>
			</div>';
		}
		?>
		<div class="sizer"></div>
	</div>
	<?php get_template_part('template-parts/buttons/edit-button'); ?>
</article>
<script src="<?php bloginfo('template_directory'); ?>/node_modules/masonry-layout/dist/masonry.pkgd.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/node_modules/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script type="module">
	// init Masonry
	var grid = document.querySelector('.row');
	var msnry = new Masonry(grid, {
		itemSelector: '.col',
		columnWidth: '.sizer',
		percentPosition: true
	});
	imagesLoaded(grid).on('progress', function() {
		// layout Masonry after each image loads
		msnry.layout();
	});

	import PhotoSwipeLightbox from '<?php bloginfo('template_directory'); ?>/node_modules/photoswipe/dist/photoswipe-lightbox.esm.js';
	const lightbox = new PhotoSwipeLightbox({
		gallery: '#gallery',
		children: 'a',
		//showHideAnimationType: 'none',
		closeTitle: 'Bild schließen',
		zoomTitle: 'Vergrößern',
		arrowPrevTitle: 'Vorheriges Bild',
		arrowNextTitle: 'Nächstes Bild',
		errorMsg: 'Entschuldigung, das Bild kann nicht geladen werden',
		indexIndicatorSep: ' von ',
		pswpModule: () => import('<?php bloginfo('template_directory'); ?>/node_modules/photoswipe/dist/photoswipe.esm.js')
	});
	lightbox.init();
</script>
