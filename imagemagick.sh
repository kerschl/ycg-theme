#/bin/sh
if [ -d "$1" ] && [ -n "$2" ] && [ -n "$3" ]; then

	mkdir $1originals $1thumbnails; 
	
	ls $1$3 | cat -n | while read number file;
	do

		#vars
		new=$2-`printf "%03d.jpg" $number`

		magick $file -auto-orient -resize "1920>x1920>" $1$new;
		magick $file -auto-orient -resize "150>" $1/thumbnails/$new;

		mv $file $1originals;
		jpegoptim -m 92 $1$new;
		mogrify -strip $1$new;
		mogrify -strip $1/thumbnails/$new;
	done
	
else
	echo 'Error: Please enter parameters
Usage:   ./imagemagick.sh PATH NAME SELECTION
Example: ./imagemagick.sh ~/p/ test "*.jpg"
Note: Make sure that there is a slash at the end of the PATH
	  and that the SELECTION ist put in quotation marks.'
fi

