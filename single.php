<?php
/* Template for displaying single posts */
get_header();
while (have_posts()) : the_post();
	get_template_part('template-parts/posts/post-single');
endwhile;
get_footer();
