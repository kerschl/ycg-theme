<?php /* The footer added to each view */ ?></main>
<footer id="footer" class="py-4 bg-primary mt-auto">
	<div class="container">
		<?php if (has_nav_menu('footer-menu')) {
			wp_nav_menu([
				'theme_location'  => 'footer-menu',
				'menu'            => 'footer-menu',
				// div
				'container'       => false,
				// ul
				'menu_class'      => 'nav justify-content-center border-bottom pb-4 mb-4',
				'menu_id'         => 'footer-menu',
				// li
				'walker' => new ycg_footer_menu_walker()
				// declared in functions.php
				// https://developer.wordpress.org/reference/functions/wp_nav_menu/
			]);
		} ?>
		<div class="d-flex justify-content-center align-items-center bd-highlight">
			<div class="copyright text-white text-end me-3"><small>&copy; <?= date('Y'); ?> <?php bloginfo('name'); ?><br />All Rights Reserved</small></div>
			<img alt="Yachtclub-Gollenshausen" height="40" src="<?php bloginfo('template_directory'); ?>/assets/logo/logo.svg">
		</div>
	</div>
</footer>

<script src="<?php bloginfo('template_directory'); ?>/node_modules/@popperjs/core/dist/umd/popper.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/node_modules/bootstrap/dist/js/bootstrap.js"></script>
<?php wp_footer(); ?>
</body>

</html>
