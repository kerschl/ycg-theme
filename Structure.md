# Website Structure

```
header-menu
├── Startseite / frontpage
├── Aktuelles / archive posts
├── Veranstaltugen / archive events
├── Regatten / content page, archive results
├── Galerie / archive gallery
├── Jugend / content page
├── Mitglieder / menufolder
│	├── Mitglied werden / content page
│	└── Bootsbuchung / booking
└── Über Uns  / menufolder
	├── Unsere Boote / content page
	├── Hafen / content page
	├── Revier / content page
	├── Chronik / content page
	├── Vorstandschaft / content page
	├── Kontakt/Anfahrt / content page
	└── Links / content page

footer-menu
├── Startseite / frontpage
├── Impressum / content page
├── Datenschutz / content page
├── Kontakt / content page
└── Mitglied werden / content page
```
